var marca;

//no javascri temos o switch é como o if mas em vez de ver uma a uma vai logo para a linha verdadeira //
//Nota: é preciso colocar break em cada caso (ver melhor o switch)//
function quadrado(x) {
    return x*x
}
quadrado (3)
console.log(quadrado (3)) //é como se fosse um print no python, para ver o resultado abrimos o site e depois //
//botão direito, inspeccionar, console//
//window.onload ao carregar a página executa o que estiver aseguir ao igual//

//Ciclos
var count = 0
while (count<10) {
    console.log(count);
    count ++; //aqui soma +1 ao count//
}

function dowhilefun(end) {
var count = 0
do {
    console.log(count);
    count ++; //aqui soma +1 ao count//
} while (count <10);
}
window.onload = dowhilefun(0)

function fori(end) {
    for (var i = 0; i < end; i +=2) //cria uma variavel para incrementar, a expressão que vai analisar, e a função se a cond for verd
console.log(i)
}
window.onload = fori(10)

//objectos
var carro = {marca:"Volvo", modelo:"XC60"} // parecido com o dicionário do python
var m =carro.marca


function ponto (xx,yy ) {
    this.x = xx; //é como fosse o init do python
    this.y =yy;
}
var p1 = new ponto (4,5) // aparece "ponto {x: 4, y: 5}" por que é o nome da função
var p2 = new ponto (10,34)
console.log (p1)

// arrays ex
var vazio = []; //array vazio
var pares = [2,4,6]; //array do mesmo tipo
var mix = [1.1, true, "a"]; //array de dif tipos
//array so tem a operação remover e adicionar
// para ordenar um array de numeros temos que usar a função
//function myFunction() {
    //points.sort(function(a, b){return a-b});} o points é o nume do array

