# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 15:31:27 2020

@author: Nuno Santos
"""

# def testrange(valor):
#     if valor in range(3,9):
#         print("value in the range")
#     else:
#         print("Not in range")

# testrange(12)

# def listaunica(lista):
#     l = []
#     for i in lista:
#         if i not in l:
#             l.append(i)
#         else:
#             continue
#     print(l)
    
# listaunica([1,2,3,3,3,3,4,5])

def fundentro(a):
        def add(b):
                nonlocal a
                a += 1
                return a+b
        return add
    
func= fundentro(2)
print(func(2))