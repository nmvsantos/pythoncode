# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 15:36:40 2020

@author: Nuno Santos
"""
import pandas as pd
import numpy as np
# Sample_data = {'X':[78,85,96,80,86], 'Y':[84,94,89,83,86],'Z':[86,97,96,72,83]}
# df= pd.DataFrame(Sample_data)
# print(df)


exam_data = {'name': ['Anastasia', 'Dima', 'Katherine', 'James', 'Emily', 'Michael', 'Matthew', 'Laura', 'Kevin', 'Jonas'],
'score': [12.5, 9, 16.5, np.nan, 9, 20, 14.5, np.nan, 8, 19],
'attempts': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
'qualify': ['yes', 'no', 'yes', 'no', 'no', 'yes', 'yes', 'no', 'no', 'yes']}
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

df = pd.DataFrame(exam_data, index = labels)
df = df[["attempts", "name", "qualify", "score"]]
# print(df)
# print(df.info())
# print(df.iloc[:3])
# print(df.head(3))
# print(df[['name', 'score']])
#print(df.iloc[[1, 3, 5, 6], [1, 3]]) #Escolhe as linhas 1,3,5 e 6 e colunas 1 e 3
                                #Dando os valores presentes 
# print(df[df['attempts'] >= 2])    # Para saber os valores com condicionais                            
# print(df.describe())                                
#print(df[df['score'].between(15, 20)]) #Dá os valores entre 15 e 20 no "score"
#df2 =df[(df['attempts'] < 2) & (df['score'] > 15)] 
""" Dá o valor maior de 15 no score e menor de 2 no attempts"""
# print(df2)                 
# df.loc['d', 'score'] = 11.5 #Altera o valor para 11.5
# print(df)
# print(df["attempts"].sum())

# df['qualify'] = df['qualify'].map({'yes': True, 'no': False})
# ## Para alterar o valores usamos o map mas só para um numero restrito de valo
# print(df)

# df['name'] = df['name'].replace('James', 'Suresh')
# ## Modificar o nome ou valor usamos replace.
# print(df)

# color = ['Red','Blue','Orange','Red','White','White','Blue','Green','Green','Red']
# df['color'] = color # Para adicionar uma coluna nova. NOTA temos que criar os
## valores
# print(df)



"""
https://www.w3resource.com/python-exercises/pandas/index-dataframe.php
"""


d = {'col1': [1, 4, 3, 4, 5], 'col2': [4, 5, 6, 7, 8], 'col3': [7, 8, 9, 0, 1]}
df11 = pd.DataFrame(data=d)
# print("Original DataFrame")
# print(df11)
# print('After add one row:')
df22 = {'col1': 10, 'col2': 11, 'col3': 12}
df11 = df11.append(df22, ignore_index=True)
# print(df11)
"""
Se quiser acrescentar uma linha é fazer append
"""
# print(df11)
# print()
# # df11 = df11.drop(df11.index[[2,4]])
# df12 = df11.drop(df11.index[2:4])
# print(df11)
# print(df12)
# df12 = df12.reset_index() #Para reordenar o novo index
# print(df12)
# df12 = df12.drop(columns=["index"]) #para remover a coluna com o old index
# print(df12)
"""
Se quisermos remover linhas utilizamos o drop, assim index[[2,4]] removemos
a linha nº2 e nº4, assim index[2:4] removemos a linha nº 2 e 3
"""

# d = {'col2': [4, 5, 6, 9, 5], 'col3': [7, 8, 12, 1, 11]}
# df = pd.DataFrame(data=d)
# print("Original DataFrame")
# print(df)
# new_col = [1, 2, 3, 4, 7]  
# # insert the said column at the beginning in the DataFrame
# idx = 0
# df.insert(loc=idx, column='col1', value=new_col) # para inserir uma coluna nova
# print("\nNew DataFrame")
# print(df)


##Escolher tdas as colunas excepto
# d = {'col1': [1, 2, 3, 4, 7], 'col2': [4, 5, 6, 9, 5], 'col3': [7, 8, 12, 1, 11]}
# df = pd.DataFrame(data=d)
# print("Original DataFrame")
# print(df)
# print("\nAll columns except 'col3':")
# df = df.loc[:, df.columns != 'col3'] # Usa-se o .loc
# print(df)
# df1 = df.nlargest(3, 'col1') # Para escolher o top 3 da coluna 1 .nlargest
# print(df1)


## Criar uma coluna nova apartir de dados de um coluna
df = pd.DataFrame({
    'name': ['Alberto Franco','Gino Mcneill','Ryan Parkes', 'Eesha Hinton', 'Syed Wharton', 'Kierra Gentry'],
      'age': [18, 22, 85, 50, 80, 5]
})
print("Original DataFrame:")
print(df)
print('\nAge group:')
df["age_groups"] = pd.cut(df["age"], bins = [0, 18, 65, 99], 
                          labels = ["kids", "adult", "elderly"])
"""
Kids -> 0 a 18 adult -> 18 a 65 elderly ->65 a 99
"""
print(df["age_groups"])
print(df)
















