# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 20:25:25 2020

@author: Nuno Santos
"""

import statistics
from scipy import stats
import numpy as np

a = [4,36,45,50,75]
b = [1,2,2,3,4,7,9]
c = [6,3,9,6,6,5,9,9,3,1]
## Média
# print(statistics.mean(a))
# print(np.mean(b))
# print(np.mean(c))
# ## Moda
# print(statistics.mode(a))
# print(stats.mode(b))
# print(stats.mode(c)) #No stats além de dizer o valor diz quantas vezes
# ## Mediana
# print(statistics.median(a))
# print(statistics.median(b))
# print(np.median(c))

##Slide 25
a=[9,2,5,4,12,7,8,11,9,3,7,4,12,5,4,10,9,6,9,4] #Entire popula
b=[9,2,5,4,12,7] #Amostra

##Para entire popula usa-se
print(np.std(a)) # para o standard desviation
print(statistics.pstdev(a)) # para o standard desviation
print(np.var(a)) #Para a variancia
print(statistics.pvariance(a)) #Para a variancia

## Para uma amostra
print(statistics.stdev(b)) #Para uma amostra
print(statistics.variance(b)) #Para uma amostra
"""
NOTA: para calcular a variacia e o standard desviation utilizar sempre o 
statistics. Ter cuidado com a função pois para entire popula tem um p antes
"""


