# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 08:47:44 2020

@author: Nuno Santos
"""

#### Aula 21

##Statistic and data visualization

## Mediana é o valor que se encontra no meio de um conjunto de numeros já
## previamente ordenados

"""
Para calcular mean, median and mode temos formulas do Python
Podemos usar o numpy, o statistics ou scipy

np.mean, np.median são  funções do numpy
stats.mode do scipy é a melhor função para calcular o mode.
"""
# c = [6,3,9,6,6,5,9,9,3,1]
# import numpy as np
# import scipy as stats

# ##Primeiro ordenar o c
# c = sorted(c)
# print(c)
# print(len(c))
# print("Média" , np.mean(c))
# print("Mediana" , np.median(c))
# #print(stats.mode(c))

"""
Standard desviation e variança
Ver com muita atenção o slide 25
"""

"""
Quartile ou quarter
é preciso ordenar os valores
"""

### Data visualization / Matplotlib
import matplotlib.pyplot as plt
import numpy as np

# x = [1,2,3,4,5]
# y = [2,4,6,8,10]
# plt.plot(x,y, "go--") "" O go-- g de green, o de ponto, tracejado 


# x = [1,2,3,4,5]
# y = [2,4,6,8,10]
# x1=[1,2,3,4,5]
# y1=[1,4,9,16,25]
# plt.plot (x,y, "r--P", label = "y=2x") #Para ver um grafico só é preciso #
# plt.plot (x1,y1 , "b:D", label="y=x^2")
# plt.legend()
# plt.title("Test")
# plt.xlabel("x")
# plt.ylabel("y")

##Slide 34
# x = [1,2,3,4,5]
# y = [2,4,6,8,10]
# x1=[1,2,3,4,5]
# y1=[1,4,9,16,25]
# plt.subplot(211) #Subplot dividir o meu ecrã 2 linha e 1 coluna e 1ºlugar
# plt.plot(x,y, "r--P", label = "y=2x")

# plt.subplot(212)
# plt.plot(x,y, "b:D", label = "y=x^2")
# plt.legent()
##pltsafig("C:\Users\Nuno Santos\Desktop\Python course\Aula 21/plot.png")

##Slide35
# x = np.arange(14)
# y = np.sin(x/2)
# plt.step(x,y+2, label="pre (default)") #faz a parte do azul
# plt.plot(x,y+2, "o--", color = "grey", alpha = 0.3)

##SLide 36
##subplots(2,1) divide o ecrã em 2 linas e 1 coluna
##               atenção que este tem o s no plots

## Slide 39 Boxplot
"""
É um gráfico importante que dá os valores do minimo, maximo, mediana,Q1 e !3
"""
# data = np.array([-10,-5,-2,-1,0,1,2,3,4])
# q1 = np.quantile(data,0.25)
# print(q1)
# q2 = np.quantile(data,0.5)
# print(q2)
# q3 = np.quantile(data,0.75)
# print(q3)

# iqr = q3-q1
# lv = q1-1.5*iqr #Isto é o boundarie mais baixo
# hv = q3 + 1.5*iqr #Isto é o boundarie mais alto
# print(lv)
# print(hv)

# plt.boxplot(data)

### Grafico de barras
# names = ["A", "B", "C"]
# values = [5,10,8]
# plt.bar(names, values, color="green")
# plt.xlabel("Names")
# plt.ylabel("no. student")


### Grafico de histograma/frequencia (quantos numero dentro de um intervalo)
# data = np.array([3,3,5,6,7,7,8,9,9])
# plt.hist(data, bins=4, edgecolor = "black") # Bins dá o numero de rectangulos
# plt.xlabel("Values")
# plt.ylabel("nFrequency")

### Grafico circular
# l = ["Python", "Java", "c++"]
# s = [200,150,100]
# c = ["green", "red", "blue"]
# plt.pie(s, labels = l, colors = c, autopct="%1.1f%%") # Para colocar a %


##### Pandas e plots
"""df.plot.box o position dá a distancia entre os grafico"""

##Slide 53 é muito importante
## Slide 55 
""" colocando só df.plot(Kind=) com o kind podemos escolher qual o tipo de 
gráfico que queremos, barras, circular, etc
"""

### Graficos 3d
from mpl_toolkits.mplot3d import Axes3D
x = np.array([1,2,3,4])
y = np.array([7,8])
a,b = np.meshgrid(x,y) # é para criar a 3 variavel para o 3d
print(a)
print(b)
print(a.shape)

fig = plt.figure(figsize = (5,5))
ax = fig.gca(projection="3d")
"""fazer em casa"""





