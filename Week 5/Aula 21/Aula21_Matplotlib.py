# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 20:41:23 2020

@author: Nuno Santos
"""
import numpy as np
import matplotlib.pyplot as plt
# x = [0,0.5,2]
# y = [0,1,4]
# plt.plot(x,y, "go--") 
"""
 O "go--" g é referente a green, o como mostra os pontos, -- como mostra a linha
"""

# x=[1,2,3,4,5]
# y=[2,4,6,8,10]

# plt.plot(x,y,"r--P", label="y=2x")
# plt.legend()
# plt.title("Test")
# plt.xlabel("x")
# plt.ylaber("y")

# x1=[1,2,3,4,5]
# y1=[1,4,9,16,25]
# plt.plot(x1,y1,"b:D", label="y=x^2")
# plt.legend()

# plt.plot(x1,y1,"b:D", label="y=x^2")
# plt.plot(x,y,"r--P", label="y=2x")

### Slide 34 Dividir a area de apresentação em 2
# x=[1,2,3,4,5]
# y=[2,4,6,8,10]

# x1=[1,2,3,4,5]
# y1=[1,4,9,16,25]

# plt.subplot(211) #2 de dividir na horizontal e fica em cima
# plt.plot(x,y, "r--P", label ="y=2x")
# plt.subplot(212) #2 de dividir na horizontal e fica em baixo
# plt.plot(x1,y1, "b:D", label="y=x^2")
# plt.legend()
"""
Para colocar 2 grafico independente é preciso escrever sempre 2 vezes
plt.subplot um com 211 e outro com 212
NOTA o primeiro digito refere às linhas, o segundo às colunas e o terceiro à
posição do gráfico EX. 223 o 3 é o terceiro gráfico na tela
"""
# plt.savefig("C:\\Users\\Nuno Santos\\Desktop\\Python course\\Aula 21") 
## O plt.savefig serve para guarda os grafico num ficheiro


##Slide 35
# x = np.arange(14)
# y = x
# plt.step(x,y+2, label="Pre") # mostra a linha azul que une os pontos
# plt.plot(x,y+2,"o--", color="red", alpha = 0.3)


##Slide 36 SUBPLOTS
# x1 = np.linspace(0.0, 5.0)
# x2 = np.linspace(0.0, 2.0)
# y1 = np.cos(2*np.pi*x1)*np.exp(-x1)
# y2 = np.cos(2*np.pi*x2)

# fig, (ax1, ax2) = plt.subplots(2,1) #ATENÇÃO é subplots tem que ter s
# fig.suptitle("A tale od 2 subplots")

# ax1.plot(x1,y1,"o-")
# ax1.set_ylabel("Damped oscillation")

# ax2.plot(x2,y2, ".-")
# ax2.set_xlabel("Time(s)")
# ax2.set_ylabel("Undamped")

# plt.show()


### Slide 38 SUBPLOTS
# x = np.arange(0.0,2.0,0.01)
# y = 1 + np.sin(2*np.pi*x)

# fig, ax = plt.subplots()
# ax.plot(x,y,label="sin($\phi$)")
# ax.set(xlabel="time(s)", ylabel="sin(2\u03c0s)",title = "Python Course")
# ax.grid() #é para colocar as linhas a cinza
# ax.legend()

# plt.show()

###Slide 42 boxplots
# data = np.array([-10,-5,-2,-1,0,1,2,3,4])

# q1 = np.quantile(data,0.25) #Para calcular o primeiro quartil colocamos 0.25
# q2 = np.quantile(data,0.5) #Para calcular o 2º ou do meio quartil colocamos 0.25
# q3 = np.quantile(data,0.75) #Para calcular o primeiro quartil colocamos 0.75
# iqr = q3-q1
# lv = q1-1.5*iqr
# hv = q3+1.5*iqr

"""
Para desenhar o gráfico não precisamos de fazer os calculos acima
"""
# plt.boxplot(data)
# plt.grid() # Para uma melhor leitura

### Slide43
# class1 = np.array([60,70,80,83,85,87,88,89,90,92,94,95,97,100,110])
# class2 = np.array([130,143,150,158,160,170,175,182,185,188,190,200,210,280,300])
# plt.boxplot([class1,class2], patch_artist = True)
# plt.grid()                  #patch_artist = True pinta o quadrado de azul
# plt.xlabel("classes")


### SLide 44 Gráfico de barras
# names = ["A", "B", "C", "D", "E"]
# values = [5,10,8,3,2]

# plt.bar(names, values, color="green")
# plt.xlabel("Names")
# plt.ylabel("Nº of student")

# #Colocar 2 gráficos
# fig, (ax1, ax2) = plt.subplots(2,1)
# ax1.bar(names, values, color="green") #Pinta as barras de verde
# plt.xlabel("Names")
# plt.ylabel("Nº of student")

# ax2.barh(names, values, color="green") #Pinta as barras de verde
# plt.xlabel("Nº of student")
# plt.ylabel("Names")


###Slide 45 Gráfico de pontos
# x = np.array([-3,-2,-1,0,1,2,3])
# y = np.array([9,4,1,0,1,4,9])
# plt.scatter(x,y, c="r")

###SLide 46 HISTOGRAMA
"""
O Histograma mostra num gráfico o numero de ocorrencia de um determinado numero
"""
# data = np.array([3,3,5,6,7,7,8,9,9,10,10,10,11,12,12,14,15,16,17,18,19,19,20])
# plt.hist(data, bins=5, edgecolor="black") 
# plt.xlabel("value")
# plt.ylabel("frequency")
"""
O bins indica quantas barras pretendo.
O edgecolor é a cor dos tracinhos a dividir as barras
"""

### Slide 47
# from scipy.stats import norm
# x = 100+15*np.random.randn(1000)
# n,bins, _ = plt.hist(x, bins=20, edgecolor = "black", density = 1)
# """O density é como se fosse uma escala para mostrar valores"""
# y = norm.pdf(bins,100,15)
# plt.plot(bins,y,"r") 


### Slide48 gráfico circular
# l = ["Python", "java", "C++"]
# s = [200,150,100]
# c = ["green", "red", "yellow"]
# plt.pie(s, labels = l , colors = c, autopct="%.2f")
"""Para escrever que dados precisamos
em 1º pomos os valores, depois associamos o labels ao titulo corres dos valores
e associamos as cores ao colors 
Para colocar percentagem  autopct = "%.2f" 2 é o nº casa decimais e f o format
"""

########## Gráfico com o recurso ao Pandas
### Slide 51
import pandas as pd
# df = pd.DataFrame(np.random.rand(10,5), columns=["A", "B", "C", "D", "E"])
# print(df)
#df.plot.box()
#df.boxplot()

### Slide 53 Dtaframe com gráficos de barras
# df = pd.DataFrame(np.random.rand(10,4),columns=["A", "B", "C", "D"])
# print(df)
#df.plot.bar()
"""
Cria para cada coluna uma barras referente ao index
"""
#df.plot.bar(stacked=True)
"""
Com o stacked junta as 4 barras numa só
"""
#
"""
Se quisermos barras horizontais usamos barh
"""

###Slide 54
# df = pd.DataFrame(np.random.rand(50,4),columns=["A", "B", "C", "D"])
# print(df.head())
#df.plot.scatter (x="A" , y = "B", c = "r", label = "Group1")
#df.plot.scatter (x="C" , y = "D", c = "b", label = "Group2")

##Para juntar os 2 gráficos
# k = df.plot.scatter(x="A" , y = "B", c = "r", label = "Group1")
# df.plot.scatter(x="C" , y = "D", c = "b", label = "Group2", ax = k)
# """
# Para juntar os 2 gráficos:
#     -colocar um dos gráficos como variável
#     -De seguida escrever a função de desenhar gráfico do outro mas acrescentando
# no final ax = k
# """


###Slide 55
# df = pd.DataFrame(np.random.rand(50,3),columns=["A", "B", "C"])
# df["C"]*=200 # Para que os valores possam ir de um intervalo 0 a 200
# df.sort_values(by = "A", inplace=True) #O inplace é para aplicar para sempre
# print(df.head())

# df.plot(kind="line", x="A", y="B", c="g")
"""
Podemos também desenhar gráficos escrevendo .plot mas depois dentro do ()
temos que fazer "kind=(Tipo de gráfico)"
"""

### Slide 56  gráfico de áreas
# df = pd.DataFrame(np.random.rand(50,3),columns=["A", "B", "C"])
# print(df.head())
# df.plot(kind="area")

### Slide 58 Panda Series e grafico circular
# s = pd.Series(3*np.random.rand(3), index=["A", "B", "C"], name = "series")
# print(s)
# #s.plot.pie() #cria o gráfico de uma forma simples mas com pouca edição
# s.plot.pie(labels = ["AA", "BB", "CC"], colors = ["b","r","g"], fontsize = 10)

"""
Se quiser editar a legenda tem que usar labels com s
Para mudar a cor tem que ser colors com s
Fontsize altera o tamanho da letra da labels
"""

### Slide 59
# df = pd.DataFrame(3*np.random.rand(4,2),index=["A", "B", "C", "D"],
#                   columns=["x", "y"])
# print(df)
# df.plot.pie(subplots = True, figsize = (11,8))
"""
Com o subplots com s cria os gráficos separado conforme o numero de colunas
o figsize redimensiona o gráfico
"""

# df = pd.DataFrame(3*np.random.rand(4,3),index=["A", "B", "C", "D"],
#                   columns=["x", "y", "z"])
# print(df)
# df.plot.pie(subplots = True, figsize = (15,8))






















