# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 15:01:57 2020

@author: Nuno Santos
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv("Titanic.csv")
#print(df.info())  # Dá a informação e o tipo de in fo presente

#print(df.describe())

# print(df.isnull().sum()) # dá-nos quantos valores NaN ou em branco temos

# print(df.head(7))

df.Name.apply(lambda x: x.split(',')[1].split('.')[0].strip()).unique()
#Name é onde queremos colocar a função, primeiro separa pela"," 
#e eu quero a segunda parte. Depois divide pelo "." e só quero a
#a primeira parte e depois remove o resto, unique remove os duplicados

def f(name):
    if '.' in name:
        return name.split(',')[1].split('.')[0].strip()
    else:
        return 'Unknown'

def g(tt):
    if tt in ['Mr']:
        return 1
    elif tt in ['Master']:
        return 3
    elif tt in ['Ms', 'Mlle', 'Miss']:
        return 4
    elif tt in ['Mrs','Mme']:
        return 5
    else:
        return 2

df['title'] = df['Name'].apply(f).apply(g) #Para chamar as funções acima 
#fazemos apply e nome da função print(df.head(5)) # Criou uma nova coluna para
# colocar os valores atribuidos na função g

df['title2'] = df['Name'].transform(lambda x: x.split(',')[1].split('.')[0].strip()) #podemos usar também o transform

#print(pd.crosstab(df['Sex'], df['title2'])) # Crosstab comparar dados de 2 
##colunas cruzando a informação

# t = pd.crosstab(df['title2'], df['Survived'])
# tt2=(pd.crosstab(df['Sex'], df['Survived'] ))
# print(tt2)
# t_pct = t.div(t.sum(1).astype(float), axis=0) # 
# t_pct * 100


# t_pct.plot(kind="bar", stacked = True, title='Survival Rate by title')
# plt.xlabel('title')
# plt.ylabel('Survival Rate')

""" para remover colunas desnecessárias"""
df = df.drop(['PassengerId','Name','Ticket'], axis=1) # o axis é para indicar
                            ## onde estão os valores a remover(axis0 ou axis1)
# print(df.head())

# print(df.Embarked)
edt  = pd.get_dummies(df['Embarked']) #get_dummies converte a coluna Embarked 
                            #em numero sendo que o 1 é sim e o 0 não
# print(edt)
# print(edt.sum(axis = 1).sum()) #soma coluna a coluna e depois soma tudo, 
                    #concluimos que existe 2 NaN porque 891(passageiros)-889(sum)
edt.drop(['S'], axis=1, inplace=True)
# print(edt)
df = df.join(edt) # Para juntar os 2 dataframes usamos o join e colocamos 
                    #as colunas C e Q
# print(df)

df.drop(['Embarked'], axis=1,inplace=True) #Aqui removemos embarked porque 
                                            #já temos as colunas C e Q
# print(df.head(3))

# print(df['Age'].isna().sum()) # Aqui indica quantos NaN temos na coluna Age

df['Age'] = df.groupby(['Pclass'])['Age'].transform(lambda x: x.fillna(x.mean())) 
# Aqui é para agrupar por Pclass e depois na coluna Age aplicamos a formula
                            # para preencher a idade de acordo com a média
# print(df)

df.drop("Cabin",axis=1,inplace=True) #Devido à falta de valores removemos

s = sorted(df['Sex'].unique()) # ordena e depois remove os duplicados
print(s)

















