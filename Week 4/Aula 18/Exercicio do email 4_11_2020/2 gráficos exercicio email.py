# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 14:27:29 2020

@author: Nuno Santos
"""

###### Exercicio dos gráficos 2 exercicios email 8/11/2020
# Exer grafico PRT medias
import csv
x = []
y = []
a = []
b = []
with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:         
        if row[0]== "PRT" and row[2] == "BOY":            
            print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
        elif row[0]== "PRT" and row[2] == "GIRL":
            print(', '.join(row))
            a.append((row[5]))
            b.append(round(float(row[6])))
    x = sorted(set(x)) 
    z = list(map(lambda x,y: (x+y)/2, y, b)) 
    print(x)
    print(y) 
    print(a)
    print(b)
    print(z)       
            
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
blue_patch = mpatches.Patch(color='blue', label='PRT')

plt.rcParams['figure.figsize'] = (8,5) 
plt.legend(handles=[blue_patch]) 


plt.subplot(211) 
plt.legend(handles=[blue_patch])
plt.title("PRT médias") 
plt.plot(x,z)
plt.grid() 
plt.ylabel("médias")

plt.subplot(212) 
plt.bar(x,z, ec = "black")
plt.axis(ymin = 460 , ymax = 500) 
plt.xlabel("Ano")
plt.ylabel("médias")
plt.grid(axis = "y")

plt.show()



# ##### Exer grafico 5 paises medias
import csv
x = []
y = []
a = []
b = []

AUS_boy = []
AUS_girl =[]
BEL_boy=[]
BEL_girl=[]
CAN_boy=[]
CAN_girl=[]
CZE_boy=[]
CZE_girl=[]
DNK_boy=[]
DNK_girl=[]

with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:         
        if row[0]== "PRT" and row[2] == "BOY":            
            #print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
        elif row[0]== "PRT" and row[2] == "GIRL":
            #print(', '.join(row))
            a.append((row[5]))
            b.append(round(float(row[6])))            
        elif row[0]== "AUS" and row[2] == "BOY":            
            #print(', '.join(row))            
            AUS_boy.append(round(float(row[6])))
        elif row[0]== "AUS" and row[2] == "GIRL":
            #print(', '.join(row))            
            AUS_girl.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "BOY":            
            #print(', '.join(row))            
            BEL_boy.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "GIRL":
            #print(', '.join(row))            
            BEL_girl.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "BOY":            
            #print(', '.join(row))            
            CAN_boy.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "GIRL":
            #print(', '.join(row))            
            CAN_girl.append(round(float(row[6]))) 
        elif row[0]== "CZE" and row[2] == "BOY":            
            #print(', '.join(row))            
            CZE_boy.append(round(float(row[6])))
        elif row[0]== "CZE" and row[2] == "GIRL":
            #print(', '.join(row))            
            CZE_girl.append(round(float(row[6]))) 
        elif row[0]== "DNK" and row[2] == "BOY":            
            #print(', '.join(row))            
            DNK_boy.append(round(float(row[6])))
        elif row[0]== "DNK" and row[2] == "GIRL":
            #print(', '.join(row))            
            DNK_girl.append(round(float(row[6])))                        

    x = sorted(set(x)) 
    PRT_media = list(map(lambda x,y: (x+y)/2, y, b)) 
    AUS_media = list(map(lambda x,y: (x+y)/2, AUS_boy, AUS_girl))
    BEL_media = list(map(lambda x,y: (x+y)/2, BEL_boy, BEL_girl))
    CAN_media = list(map(lambda x,y: (x+y)/2, CAN_boy, CAN_girl))
    CZE_media = list(map(lambda x,y: (x+y)/2, CZE_boy, CZE_girl))
    DNK_media = list(map(lambda x,y: (x+y)/2, DNK_boy, DNK_girl))
    print(x)
    print(y) 
    print(AUS_boy)
    print(AUS_girl)
    print(AUS_media)  
     
           
            
import matplotlib.pyplot as plt


import matplotlib.patches as mpatches
blue_patch = mpatches.Patch(color='blue', label='PRT')
red_patch = mpatches.Patch(color='red', label='AUS')
orange_patch = mpatches.Patch(color='orange', label='BEL')
green_patch = mpatches.Patch(color='green', label='CAN')
olive_patch = mpatches.Patch(color='olive', label='CZE')
purple_patch = mpatches.Patch(color='purple', label='DNK')


plt.rcParams['figure.figsize'] = (10,6)

plt.legend(handles=[blue_patch, red_patch, orange_patch, green_patch, olive_patch,
                    purple_patch], loc = "upper right")
plt.title("Comp 5 paises com PT")
plt.grid()
plt.xlabel("Ano")
plt.ylabel("médias")
plt.plot(x,PRT_media)
plt.plot(PRT_media, color= "blue", marker = "+")
plt.plot(AUS_media, color ="red", marker = "*")
plt.plot(BEL_media, color ="orange", marker = "*")
plt.plot(CAN_media, color ="green", marker = "*")
plt.plot(CZE_media, color ="olive", marker = "*")
plt.plot(DNK_media, color ="purple", marker = "*")
plt.show()
