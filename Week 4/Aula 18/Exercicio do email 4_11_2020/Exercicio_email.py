# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 14:47:51 2020

@author: Nuno Santos
"""

### Exercicios do email:
## Exercicio do remove

###### Exercicio dos gráficos
## Exer grafico PRT medias
# import csv
# x = []
# y = []
# a = []
# b = []
# with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
#     csv_reader = csv.reader(csvfile, delimiter = ",")
#     for row in csv_reader:         
#         if row[0]== "PRT" and row[2] == "BOY":            
#             print(', '.join(row))
#             x.append(row[5])
#             y.append(round(float(row[6])))
#         elif row[0]== "PRT" and row[2] == "GIRL":
#             print(', '.join(row))
#             a.append((row[5]))
#             b.append(round(float(row[6])))
#     x = sorted(set(x)) # para obter so um ano de cada em vez repetidos
#     z = list(map(lambda x,y: (x+y)/2, y, b)) #para somar boy and girl
#     print(x)
#     print(y) 
#     print(a)
#     print(b)
#     print(z)       
            
# import matplotlib.pyplot as plt
# import matplotlib.patches as mpatches
# blue_patch = mpatches.Patch(color='blue', label='PRT')

# plt.legend(handles=[blue_patch])
# plt.title("PT médias")
# plt.xlabel("Ano")
# plt.ylabel("médias")
# plt.plot(x,z)
# plt.plot(z, color= "blue", marker = "+")
# plt.show()



# ##### Exer grafico 5 paises medias
import csv
x = []
y = []
a = []
b = []

AUS_boy = []
AUS_girl =[]
BEL_boy=[]
BEL_girl=[]
CAN_boy=[]
CAN_girl=[]
CZE_boy=[]
CZE_girl=[]
DNK_boy=[]
DNK_girl=[]

with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:         
        if row[0]== "PRT" and row[2] == "BOY":            
            #print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
        elif row[0]== "PRT" and row[2] == "GIRL":
            #print(', '.join(row))
            a.append((row[5]))
            b.append(round(float(row[6])))            
        elif row[0]== "AUS" and row[2] == "BOY":            
            #print(', '.join(row))            
            AUS_boy.append(round(float(row[6])))
        elif row[0]== "AUS" and row[2] == "GIRL":
            #print(', '.join(row))            
            AUS_girl.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "BOY":            
            #print(', '.join(row))            
            BEL_boy.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "GIRL":
            #print(', '.join(row))            
            BEL_girl.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "BOY":            
            #print(', '.join(row))            
            CAN_boy.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "GIRL":
            #print(', '.join(row))            
            CAN_girl.append(round(float(row[6]))) 
        elif row[0]== "CZE" and row[2] == "BOY":            
            #print(', '.join(row))            
            CZE_boy.append(round(float(row[6])))
        elif row[0]== "CZE" and row[2] == "GIRL":
            #print(', '.join(row))            
            CZE_girl.append(round(float(row[6]))) 
        elif row[0]== "DNK" and row[2] == "BOY":            
            #print(', '.join(row))            
            DNK_boy.append(round(float(row[6])))
        elif row[0]== "DNK" and row[2] == "GIRL":
            #print(', '.join(row))            
            DNK_girl.append(round(float(row[6])))                        

    x = sorted(set(x)) # para obter so um ano de cada em vez repetidos
    PRT_media = list(map(lambda x,y: (x+y)/2, y, b)) #para somar boy and girl
    AUS_media = list(map(lambda x,y: (x+y)/2, AUS_boy, AUS_girl))
    BEL_media = list(map(lambda x,y: (x+y)/2, BEL_boy, BEL_girl))
    CAN_media = list(map(lambda x,y: (x+y)/2, CAN_boy, CAN_girl))
    CZE_media = list(map(lambda x,y: (x+y)/2, CZE_boy, CZE_girl))
    DNK_media = list(map(lambda x,y: (x+y)/2, DNK_boy, DNK_girl))
    print(x)
    print(y) 
    print(AUS_boy)
    print(AUS_girl)
    print(AUS_media)  
     
           
            
import matplotlib.pyplot as plt


import matplotlib.patches as mpatches
blue_patch = mpatches.Patch(color='blue', label='PRT')
red_patch = mpatches.Patch(color='red', label='AUS')
orange_patch = mpatches.Patch(color='orange', label='BEL')
green_patch = mpatches.Patch(color='green', label='CAN')
olive_patch = mpatches.Patch(color='olive', label='CZE')
purple_patch = mpatches.Patch(color='purple', label='DNK')


plt.rcParams['figure.figsize'] = (10,6)

plt.legend(handles=[blue_patch, red_patch, orange_patch, green_patch, olive_patch,
                    purple_patch], loc = "upper right")
plt.title("Comp 5 paises com PT")
plt.xlabel("Ano")
plt.ylabel("médias")
plt.plot(x,PRT_media)
plt.plot(PRT_media, color= "blue", marker = "+")
plt.plot(AUS_media, color ="red", marker = "*")
plt.plot(BEL_media, color ="orange", marker = "*")
plt.plot(CAN_media, color ="green", marker = "*")
plt.plot(CZE_media, color ="olive", marker = "*")
plt.plot(DNK_media, color ="purple", marker = "*")
plt.show()


## Exercicio desempenho meninas e meninos
# import csv
# x = []
# y = []
# b = []
# b2006= []
# g2006= []
# b2009= []
# g2009= []
# b2012= []
# g2012= []
# b2015= []
# g2015= []
# b2018= []
# g2018= []
# bo = []
# gi = []

# with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
#     csv_reader = csv.reader(csvfile, delimiter = ",")
#     for row in csv_reader:     
#         if row[2] == "BOY" and row[5] =="2003":            
#             #print(', '.join(row))
#             x.append(row[5])
#             y.append(round(float(row[6])))
#             y1 = sum(y)  
#         elif row[2] == "GIRL" and row[5] =="2003":
#             #print(', '.join(row))            
#             b.append(round(float(row[6])))
#             b1 = sum(b)
#         elif row[2] == "BOY" and row[5] =="2006":            
#             #print(', '.join(row))
#             x.append(row[5])
#             b2006.append(round(float(row[6])))
#             b06=sum(b2006)
#         elif row[2] == "GIRL" and row[5] =="2006":
#             #print(', '.join(row))            
#             g2006.append(round(float(row[6]))) 
#             g06=sum(g2006)
#         elif row[2] == "BOY" and row[5] =="2009":            
#             #print(', '.join(row))
#             x.append(row[5])
#             b2009.append(round(float(row[6])))
#             b09=sum(b2009)
#         elif row[2] == "GIRL" and row[5] =="2009":
#             #print(', '.join(row))            
#             g2009.append(round(float(row[6]))) 
#             g09=sum(g2009)
#         elif row[2] == "BOY" and row[5] =="2012":            
#             #print(', '.join(row))
#             x.append(row[5])
#             b2012.append(round(float(row[6])))
#             b12=sum(b2012)
#         elif row[2] == "GIRL" and row[5] =="2012":
#             #print(', '.join(row))            
#             g2012.append(round(float(row[6]))) 
#             g12=sum(g2012)
#         elif row[2] == "BOY" and row[5] =="2015":            
#             #print(', '.join(row))
#             x.append(row[5])
#             b2015.append(round(float(row[6])))
#             b15=sum(b2015)
#         elif row[2] == "GIRL" and row[5] =="2015":
#             #print(', '.join(row))            
#             g2015.append(round(float(row[6]))) 
#             g15=sum(g2015)
#         elif row[2] == "BOY" and row[5] =="2018":            
#             #print(', '.join(row))
#             x.append(row[5])
#             b2018.append(round(float(row[6])))
#             b18=sum(b2018)
#         elif row[2] == "GIRL" and row[5] =="2018":
#             #print(', '.join(row))            
#             g2018.append(round(float(row[6]))) 
#             g18=sum(g2018)
            
#     bo.append(y1)     
#     bo.append(b06)  
#     bo.append(b09)  
#     bo.append(b12)  
#     bo.append(b15)  
#     bo.append(b18)  
   
#     gi.append(b1)     
#     gi.append(g06)  
#     gi.append(g09)  
#     gi.append(g12)  
#     gi.append(g15)  
#     gi.append(g18)     
   
# x = sorted(set(x))
# print(x)            
# y1 = sum(y)            
# b1 = sum(b)
# b06=sum(b2006)
# g06=sum(g2006)
# b09=sum(b2009)
# g09=sum(g2009)
# b12=sum(b2012)
# g12=sum(g2012)
# b15=sum(b2015)
# g15=sum(g2015)
# b18=sum(b2018)
# g18=sum(g2018)

# # gi = b+g06+g09+g12+g15+g18
# print(bo)
# print(gi)

# import matplotlib.pyplot as plt
# import numpy as np
# barWidth = 0.20

# r1 = np.arange(len(x))
# r2 = [i + barWidth for i in r1]
# r3 = [i + barWidth for i in r2]



# plt.title("Comp boys and Girls")
# plt.xlabel("Ano")
# plt.ylabel("Total")
# plt.legend()
# plt.bar(r1,bo, color="blue")
# plt.bar(r2,gi, color = "pink")
# plt.xticks([r for r in range(len(x))],x)
# plt.show



## EXTRA Exercicio desempenho meninas e meninos só para ano 2003

# import csv
# import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt

# x = []
# y = []
# b = []
# b1 = []

# with open("DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
#     csv_reader = csv.reader(csvfile, delimiter = ",")
#     for row in csv_reader:     
#         if row[2] == "BOY" and row[5] =="2003":            
#             #print(', '.join(row))
#             x.append(row[5])
#             y.append(round(float(row[6])))
#             y1 = sum(y)  
#         elif row[2] == "GIRL" and row[5] =="2003":
#             #print(', '.join(row))            
#             b.append(round(float(row[6])))
#             b1 = sum(b)
   
# tot=[]
# ge = []
# x = sorted(set(x))
# x = x+x
# for i in range(1):
#     tot.append(y1)
#     tot.append(b1)
# for i in range(1):
#     ge.append("boy")
#     ge.append("girl")
    
# print(x)
# print(ge)
# print(tot)
# print(y1)
# print(b1)
# print(ge)

# ar = pd.DataFrame([y1,b1])
# print(ar)

# barWidth = 1.20
# r1 = np.arange(len(x))
# r2 = [i + barWidth for i in r1]

# plt.title("Comp boys and Girls")
# plt.xlabel("Ano 2003")
# plt.ylabel("Total")

# plt.bar(r1,tot)


# plt.xticks([r for r in range(len(ge))],ge,)

# plt.show





            