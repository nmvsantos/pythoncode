# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 16:22:38 2020

@author: Nuno Santos
"""

###Gráficos da OCDE
## Gráfico de barras para Portugal
# import pandas as pd
# import matplotlib.pyplot as plt

# df = pd.read_csv("DP_LIVE_04112020161547500.csv")
# print(df)

# df.drop(["Flag Codes"], axis=1, inplace=True) # Para eliminar a coluna com NaN

# df2 = df.loc[df["LOCATION"]=="PRT"] #Escolher só as linhas de PRT
# print(df2)

# df2.drop(["LOCATION","INDICATOR","SUBJECT","MEASURE", "FREQUENCY"], axis = 1, inplace=True)
# print(df2) #Eliminar o que não nos interessa

# df3 = df2.groupby(['TIME']).mean() # Para agrupar os anos e calcular média

# df3.plot(kind="bar", title = "Portugal", ylim = (460,500))
# plt.ylabel('média')
# plt.grid(axis = "y")

# # Gráfico de linhas
# df3.plot(kind="line", title = "Portugal", ylim = (460,500))
# plt.ylabel('média')
# plt.grid()
# plt.legend(["PRT Values"])

##Gráfico para calcular 
"""
barras com os dados em relação ao desempenho de meninos e as meninas usando 
o matplotlib
"""
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("DP_LIVE_04112020161547500.csv")

df.drop(["Flag Codes","INDICATOR","MEASURE", "FREQUENCY"], axis = 1, inplace=True)
#print(df)

df2 = df.loc[df["LOCATION"]=="PRT"] #Escolher só as linhas de PRT
print(df2)

# df3 = df2.groupby(['TIME',"SUBJECT"]).mean()
# print(df3)
colours={"BOY":"green", "GIRL":"pink"}

df2.plot(kind = "barh", xlim = (450,500), color =df2["SUBJECT"].replace(colours),
         x = "TIME", y = "Value")
plt.xlabel('média')
plt.ylabel("Boy / Girl by years")
plt.grid(axis = "x")
plt.title("2003-2018 Comparison Boys and Girls Portugal")
plt.legend(colours)
plt.show()











