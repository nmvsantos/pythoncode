# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 16:20:03 2020

@author: Nuno Santos
"""

Input =  [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]
d = {}

def frequencies (v):
    for i in Input:
        if i in d:
            d[i] +=1
        else:
            d[i] = 1
    #print(d)  
    
    key_max = max(d.keys(), key=(lambda k: d[k]))
    #key_min = min(d.keys(), key=(lambda k: d[k]))    
    
    x = sorted(((v,k) for k,v in d.items()))
    #print(x)
    key_min = (x[0][1], x[1][1])

    # print("O key maior é" ,key_max)
    # print("O key menor é", key_min)
    
    print("O resultado final é:",(key_min, key_max))


frequencies(Input)

"""
Para entender o funcionamento do lambda
https://www.w3resource.com/python-exercises/dictionary/python-data-type-dictionary-exercise-15.php
"""