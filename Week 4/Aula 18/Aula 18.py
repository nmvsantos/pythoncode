# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 08:30:20 2020

@author: Nuno Santos
"""

####***** Aula 18
### Continuação do review
##Try and catch

# import json
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\JJson.json"

# try 
#     with open file_path as file json_file:
        
### Lambda, maps, filter
### Lambda
# def power_tree(x):
#     return(x**3)
# print(power_tree(10))

# #Com o lambda
# a = lambda x : x**3
# print(a(10))

# def larger_num(n1,n2):
#     if n1 >n2:
#         return n1
#     else:
#         return n2
# print(larger_num(5,7))

#com lambda
larger_num = lambda n1,n2: n1 if n1>n2 else n2
print(larger_num(5,7))


### Maps


### Classes
# class nuno:
#     a = [2,4,6]
#     b = [7,8]
#     c = [10,11,21]
    
#     def __init__ (self, val):
#         self.val = val
#         print(val)
#     def increase(self):
#         self.val +=1
#     def show(self):
#         print(self.val)

# x = nuno(3)
# c = x.increase()
# d = x.show()
# print(type(x.a))
# print(x.a.__add__(x.b)) #junta a lista b ao a
# print(x.a.__eq__(x.b)) #isto compara se a lista a é igual lista b
# print(x.a.__ne__(x.c))

####################************** Numpy

### numpy está formatado para fazer as contas mais depressa
import numpy as np

#print(np.__version__) #a versão
#print(np.version) # a localização
# np.array([1,2,3])
# array([1,2,3])

students = np.array([[[1,3,5],[1,1,1]],[[4.5,4,5],[4.3,4.4,4.6]]], dtype=int)
### o dtype é para escolher o tipo de dados que queremos
#print(students)

# print(students.shape, "\n",  # 2 blocks, 2 lines, 3 elements
#       students.nbytes, "\n",  #ocupa x bytes
#       students.ndim, "\n", # 3 dmensões
#       students.dtype, "\n", #o tipo
#       students.size, "\n", 
#       students.data, "\n",
#       students.itemsize, "\n") 


# x1 = np.array([[-1,3]])
# x2 = np.array([[1,2]])
# x3 = x1+x2
# x4 = x1*x2
# x5 = x1-x2
# print(x1)
# print(x2)
# print(x3)
# print(x4)
# print(x5)

# x3 = np.power(10,4, dtype=np.int8) # faz a conta 10**4
# print(x3)  # com o int8 dá um valor errado
# x4 = np.power(10,4, dtype=np.int32)
# print(x4) # O o int32 dá o valor mais correcto
# """ ver o slide 8 com os intervalos de int"""


### Reshape vs resizw
# print(np.arange(10)) # imprime de 0 a 9
# print(np.arange(10).reshape(2,5)) # cria um 2 array com 5 elementos

#print(np.arange(10).reshape(2,7)) # não dá por é impossivel dividir o array
#em 2 arrays com 7 elementos

# print((np.resize(np.arange(10),(2,5))))
# print((np.resize(np.arange(10),(2,7)))) # Com o resize ele completa com outros
# ## elementos que não estão na lista

### newaxis e copy
# d = np.arange(2,5)
# print(np.arange(2,5)) #imprime o array de 2 a 4
# print(d.shape) 
# print(d[:,np.newaxis]) # aqui vira o array a coluna
# print(d[:,np.newaxis].shape) # são 3 elemento e 1 linha

# x = np.array([1,4,5])
# y = x
# z = np.copy(x)
# x[1] = 2 # para mudar o segundo elemento
# print(x)
# print(y)
# print(z)


### Sorte
# dtype = [("name",'S10'), ("grade", float), ("age", int)]
# values = [("joseane",5,30),("Hamed",5,32), ("Stefan",5,30) ]
# sorted_data = np.array(values,dtype = dtype)
# print(np.sort(sorted_data, order="age")) # Ordena por age
""" Ver porque aparece o b no resultado e explicar porque no nome aparece 
aparece "s10" """

### Criar 0
# b = np.zeros((2,2,4), dtype = int)
# print(b)
# print(b.ndim, b.dtype, b.size)

# a = np.zeros((10,10))
# print("% a bytes" % (a.size * a.itemsize))

# c = np.zeros(10)
# c[8] = 1 # Altera o valor na posição 8
# print(c)

### Slice
# b = np.arange(50)
# b = b[::-1] #inverte
# b = b.reshape(5,10)
# print(b)
# print()
# print(b[4,1]) # dá o valor da 4º linha e o valor na 2 posição "8"
# print()
# print(b[3,:]) # mostra a 4 linha
# print()
# print(b[0,2:]) # na primeira linha retira os 2 primeiro elementos
# print()
# print(b[:1,1:]) # Na primeira linha retirar o primeiro elemento

### Concatenar
#print(np.ones((2,1)))
# b = np.arange(50)
# b = b.reshape(5,10)
# c = np.ones((2,10))
# print(b)
# print()
# print(c)
# print()
# print(np.concatenate((b,c))) # junta os arrays NOTA tem que ter a mesma dimensão

# d = np.hstack(c) # O hstack coloco o array numa unica linha
# print(d)
# print(np.vstack(d)) # O vstack coloco o array numa unica coluna


### zeros_like   eye
# d = np.ones((1,3))
# x = np.zeros_like((d))
# x1 = np.ones_like((x))
# print(d)
# print(x)
# print(x1)

# b = np.eye(3)
# print(b)

# print(np.full((2,1),np.inf)) # Assim cria array com valor infinito/maior poss
# a = np.full((2,1),np.inf)
# print(a)
# print(a.dtype, (a.size*a.itemsize)) #Dá o tipo e o tamnho

print(np.empty((1,2))) # Cria variaveis com o menor possivel


###astype 
# b =  np.arange(50)
# c = b.astype(np.int16)

### Random
# b=np.random.random((10,10))
# bmin, bmax = b.min(), b.max()
# print(bmin, bmax)

###Usefull funci
# yesterday = np.datetime64("today") - np.timedelta64(1) #Timedelta é data de hje 
# today = np.datetime64("today")
# tomorrow = np.datetime64("today") + np.timedelta64(1)
# print(yesterday,today,tomorrow)

# b = np.arange("2019", "2021", dtype="datetime64")
# print(b.size) # Dá 2 que são 2 anos


#### Matplotlib / grafico

# import matplotlib.pyplot as plt
# values = np.linspace(-(2*np.pi), 2*np.pi, 20) # 20 dá o num de valores k kero
# ## no intervalo de -(2*np.pi) a 2*np.pi, já linspace junta todos os pontos
# print(values)
# cos_values = np.cos(values)
# sin_values = np.sin(values)

# plt.plot(cos_values, color="blue", marker= "*")
# plt.plot(sin_values, color="red", marker = "+")
# plt.show()


# ### 
# import matplotlib.pyplot as plt
# from PIL import Image

# my_path = "C:\\Users\\Nuno Santos\\Pictures\\Desem"
# im = Image.open(my_path)
# plt.imshow(im)

















