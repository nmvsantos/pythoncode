# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 14:09:29 2020

@author: Nuno Santos
"""

from math import pi

def opera_circulos(raio):
    print("Perimetro do circulo", 2*pi*raio)
    print("Area do circulo",pi*raio**2)
    print("Volume da esfera", (4/3)*pi*(raio**3),)

def opera_quadrado(lado):
    print("\nPerimetro do quadrado", lado + lado + lado + lado)
    print("Area do quadrado" , lado * lado)
    
opera_circulos(int(input("Indique o valor do raio ")))
opera_quadrado(int(input("Indique o valor do lado ")))