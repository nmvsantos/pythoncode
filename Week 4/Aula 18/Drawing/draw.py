# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 16:17:28 2020

@author: Nuno Santos
"""

import sys
sys.path.append("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 18\\Area")

"""
Temos que ir buscar as formulas à pasta onde guardámos o ficheiro .py
NOTA: só a pasta sem o nome do ficheiro
"""
import GeometryCalculations
GeometryCalculations.squareArea(5)
GeometryCalculations.rectangulArea(5, 5)