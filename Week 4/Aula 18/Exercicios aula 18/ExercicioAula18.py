# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 15:11:11 2020

@author: Nuno Santos
"""

### numpy está formatado para fazer as contas mais depressa
import numpy as np

#print(np.__version__) #dá a versão do numpy
#print(np.version) # a localização

# students = np.array([[[1,3,5],[1,1,1]],[[4.5,4,5],[4.3,4.4,4.6]]], dtype=int)
# ### o dtype é para escolher o tipo de dados que queremos
# print(students.ndim, students.dtype)
# print(students.shape, "\n",  # 2 blocks, 2 lines, 3 elements
#       students.nbytes, "\n",  #ocupa x bytes
#       students.ndim, "\n", # 3 dmensões
#       students.dtype, "\n", #o tipo
#       students.size, "\n", 
#       students.data, "\n",
#       students.itemsize, "\n") 

### Slide 7
# x1 = np.array([[-1,3]])
# x2 = np.array([[1,2]])
# x3 = x1+x2 # Dá para somar arrays
# x4 = x1*x2 # 
# x5 = x1-x2
# print(x1)
# print(x2)
# print(x3)
# print(x4)
# print(x5)

### Slide 8
# x3 = np.power(10,4, dtype=np.int8) # faz a conta 10**4
# print(x3)  # com o int8 dá um valor errado
# x4 = np.power(10,4, dtype=np.int32)
# print(x4) # O o int32 dá o valor mais correcto
# """ ver o slide 8 com os intervalos de int"""

### Slide 10 Resize reshape
# print(np.arange(10)) # imprime de 0 a 9
# print(np.arange(10).reshape(2,5)) # cria um 2 array com 5 elementos

# #print(np.arange(10).reshape(2,7)) # não dá porque é impossivel dividir o array
#                                 ###em 2 arrays com 7 elementos

# print((np.resize(np.arange(10),(2,5))))
# print((np.resize(np.arange(10),(2,7)))) # Com o resize ele completa com outros
#                         ## elementos que não estão na lista


### Slide 11  Newaxis e copy
# d = np.arange(2,5)
# print(np.arange(2,5)) #imprime o array de 2 a 4
# print(d.shape) 
# print(d[:,np.newaxis]) # aqui vira o array a coluna
# print(d[:,np.newaxis].shape) # são 3 elemento e 1 linha

# x = np.array([1,4,5])
# y = x
# z = np.copy(x)
# x[1] = 2 # para mudar o segundo elemento
# print(x)
# print(y)
# print(z)


### Slide 12 Sorting
# dtype = [("name",'S10'), ("grade", float), ("age", int)]
# values = [("joseane",5,30),("Hamed",5,32), ("Stefan",5,25) ]
# sorted_data = np.array(values,dtype = dtype)
# print(np.sort(sorted_data, order="age")) # Ordena por age
# """ Ver porque aparece o b no resultado e explicar porque no nome aparece 
# aparece "s10" """

### Slide 13 Criar arrays de 0 com o  numpy
# b = np.zeros((2,2,4), dtype = int)
# print(b)
# print(b.ndim, b.dtype, b.size)

# a = np.zeros((10,10))
# print("% a bytes" % (a.size * a.itemsize))

# c = np.zeros(10)
# c[8] = 1 # Altera o valor na posição 8
# print(c)

### Slide 14
# b = np.arange(50)
# b = b[::-1] #inverte a sequencia
# b = b.reshape(5,10) # Coloca em 5 linhas 10 elementos
# print(b)
# print()
# print(b[4,1]) # dá o valor da 5º linha e o valor na 2 posição "8"
# print()
# print(b[3,:]) # mostra a 4º linha
# print()
# print(b[0,2:]) # na primeira linha retira os 2 primeiro elementos
# print()
# print(b[:1,1:]) # Na primeira linha retirar o primeiro elemento


### Slide 15 Concantenar
# a = np.arange(2,6)
# b = np.array([[3,5],[4,6]])
# print(a)
# print(b)
# print(np.vstack(a)) # O vstack coloco o array numa unica coluna
# print(np.hstack(b)) #O hstack coloco o array numa unica linha

# b = np.arange(50)
# b = b.reshape(5,10)
# c = np.ones((2,10))
# print(b)
# print()
# print(c)
# print(c[1][1]) # Tem indexação
# print()
# print(np.concatenate((b,c))) # junta os arrays NOTA tem que ter a mesma dimensão
                            ## em numero de elementos, neste caso eram de 10

### Slide 16
#print(np.ones((2,1)))
# b = np.arange(50)
# b = b.reshape(5,10)
# c = np.ones((2,10))
# print(b)
# print()
# print(c)
# print()
# print(np.concatenate((b,c))) # junta os arrays NOTA tem que ter a mesma dimensão

# d = np.hstack(c) # O hstack coloco o array numa unica linha
# print(d)
# print(np.vstack(d)) # O vstack coloco o array numa unica coluna


### Slide 17 zeros_like   eye   empty   full
# d = np.ones((1,3))  ##Criada um array de 1's
# x = np.zeros_like((d)) ## Com o like ele cria o array de acordo com o tipo d
# x1 = np.ones_like((x))
# print(d)
# print(x)
# print(x1)

###Eye
# b = np.eye(3)
# print(b)

###Empty
# print(np.empty((1,2))) # Cria variaveis com o menor possivel

###Full
# print(np.full((2,1),np.inf)) # Assim cria array com valor infinito/maior poss
# a = np.full((2,1),np.inf)
# print(a.dtype, (a.size*a.itemsize)) #Dá o tipo e o tamnho


### Slide 18 astype 
# b =  np.arange(50)
# c = b.astype(np.int16)
# print(b.astype(np.int16))

### Slide 19 Random
# b=np.random.random((10,10)) #cria randomly array com 10 linas e 10 colunas
# bmin, bmax = b.min(), b.max() #Aqui vai buscar os valores max e min
# print(bmin, bmax)

# c = np.random.random((3,3,3))
# print(c)


### Slide 20
# yesterday = np.datetime64("today") - np.timedelta64(1) #Timedelta é data de hje 
# today = np.datetime64("today")
# tomorrow = np.datetime64("today") + np.timedelta64(1)
# print(yesterday,today,tomorrow)

# b = np.arange("2019", "2021", dtype="datetime64")
# print(b.size) # Dá 2 que são 2 anos entre 2019 e 2021


#############*************Matplotlib
# import matplotlib.pyplot as plt
# values = np.linspace(-(2*np.pi), 2*np.pi, 20) # 20 dá o num de valores k kero
# ## no intervalo de -(2*np.pi) a 2*np.pi, já linspace junta todos os pontos
# print(values)
# cos_values = np.cos(values)
# sin_values = np.sin(values)

# plt.plot(cos_values, color="blue", marker= "*") #gera a linha no grafico
# plt.plot(sin_values, color="red", marker = "+") #gera a linha no grafico
# plt.show()







