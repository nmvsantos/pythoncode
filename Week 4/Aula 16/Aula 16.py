# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 08:31:03 2020

@author: Nuno Santos
"""

### Revisão/correcção exercicios da lista semana 1

# num1 = int(input("Qual o primeiro numero?"))
# num2 = int(input("Qual o segundo numero?"))

# validopera = False
# while not validopera: # aqui vai verificar se a condição é True or False
#     opera = input("Diz qual é a opera '+', '-', '*', '/'")
#     if opera == "+":
#         result = num1 + num2
#         validopera = True
#         break
#     elif opera == "-":
#         result = num1 - num2
#         validopera = True
#         break
#     elif opera == "*":
#         result = num1 + num2
#         validopera = True
#         break
#     elif opera == "/":
#         result = num1 + num2
#         validopera = True
#         break  
#     else:
#         print("A operação deve ser '+', '-', '*', '/'")
#         continue
# print("O resultado é", result)


## outra forma de resolver o exercicio
# n1 = input("\n Enter first number: ")
# n2 = input("\n Enter second number: ")
# c = input("\n Enter the operator: ")

# str = n1+c+n2
# print(f'\n{n1} {c} {n2}')

# res = eval(str)
# print("\nRES : ", res)

# ### Guess game  rever em casa
# day = "Saturday"
# tempe = 30
# raining = True # Aqui está o problema

# if day =="Saturday" and tempe > 20 and raining == False:
#     print("go out")
# else:
#     print("Better finishing python programming exercises")

# Onde está o "or not" tem que estar "and" e o raining tem que ser "==False" no if

### Semana 2

### Revisão das listas

## Maneiras de importar funções
# import sqlite3
# from sqlite3 import Error # Estamos a importar da base de dados sqlite3
# from numpy import *
# import numpy as np

# b =[4,6,7]
# #print(np.add(b,5))
# #print(dir(np)) #imprime as funções presentes no numpy
# L = [3,True,"ale",2.7,[5,8]]
# a = [3,[109,27],4,15]
# print(a[0])  # imprime o elemento na posição 1
# print(a[1])  # Imprime o elemento na segunda posição
# print(a[1:4])  # Imprime o elemento na posição 1 ate à ultima

# ## Se quiser começar uma lista utilizamos o for 
# a = []
# for i in range(4):
#     a.append(i)  # Aqui é para colocar o valor de i na lista vazia com nome a
# print(a)

### Revisão sobre tupple

# t = ("English", "History", "Math")
# print(t[1])
# print(t.index("English"))

# t = (1,9,3,9)
# print(t.count(9))
# print(max(t))
# print(t+t)

# print((1,2)==(2,1))

#         ## Para remover tuple conver para lista
# t = (1,9,3,9)
# x = list(t)
# x.remove(1)
# t = tuple (x)
# print(t)

# a = (1,2)
# b = (3,4)
# c = zip(a,b) # 
# x= list(c)
# print(x)

# ### Revisão dictio

# k = {"red" , "green"}
# v = {"#FF0000","#008000"}
# z = zip(k,v) # Junta os valores conforme a sequencia
# d = dict(z)
# print(d)

# num = {"ali" : [12,13,8], "Sara": [15,7,14]}
# d={k:sorted(v) for k,v in num.items()}
# print(d)
# print(list(num.keys())) # Print as keys do dict
# print(list(num.values())) # Print os values do dict

### Revisão set

# f = {"Apple", "orange", "banana"}
# f.add("Cherry")
# print(f)
# f.update(["Mango","grapes"])
# print(f)


# ### Importa files
# epopeia = open("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt", "r")
# """
# NOTA para abrir o ficheiro tem que ter sempre \\
# """
# for line in epopeia:
#     print(line)
#     if "tempestade" in line.lower():
#         print(line)
# epopeia.close()

## or
# with open("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt", "r") as epopeia:
#     for line in epopeia:
#         if "tempestade" in line.lower(): # Aqui lê as linhas onde está "tempestade"
#             print(line)
# print(line)  # O line fica com um tipo de str

### para colocar os acentos 
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     """
#     O encoding coloco os acentos e caracteres especiais
#     """
#     line = epopeia.readline()
#     while line:
#         print(line,end="") # Com o end="" retirar o espaço entre as linhas
#         line = epopeia.readline() # Isto é para fechar o loop

# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# ## É recomen colocar o path como variavel
# ## E colocar uma variável para o encoding para não escrever sempre (encod)
# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     poem = epopeia.readlines() # vai juntar todas as linhas numa unica e
#     print(type(poem))          # vamos obter uma lista, recomend readlines
# print(poem)

# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# with open(file_path,"r", encoding = encod) as epopeia:
#     text = epopeia.read() # Com o read coloca o texto como está no ficheiro
#     print(type(text))     # Lendo o ficheiro inteiro
# print(text) # Retorna um sting com todo o texto


# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# ## É recomen colocar o path como variavel
# ## E colocar uma variável para o encoding para não escrever sempre (encod)
# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     poem = epopeia.readlines() 
#     print(type(poem))        
# print(poem)
# for linea in poem[::-1]:
#     print(linea, end="")
# print(type(poem))
# for linea in text[::-1]:
#     print(linea, end="")
# print(type(text))


### Importar ficheiro Json Json é 
### No ficheiro json temos que gravar com .json
### É recomendavel quand

# import json
# file_json = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\JJson.json"
# with open(file_json) as json_file:
#     json_Object = json.load(json_file) # Para acessar aos dados do Json
#     name = json_Object["data"][0]["name "]
#     #adress = json_Object["data"][1]["Adress"]["city"]  #Não funciona por tem 1
#     adress = json_Object["data"][0]["Adress"]["city"]  # Tem que mudar o 1 pa 0
#     print(json_Object)
#     print(name)
#     print(adress)

## Exercicio para ter mais do que 1 valor 
# import json
# file_json = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\Json2.json"
# with open(file_json) as json_file:
#     json_Object = json.load(json_file) # Para acessar aos dados do Json
#     name = json_Object["data"][0]["name "]
#     #adress = json_Object["data"][1]["Adress"]["city"]  #Não funciona por tem 1
#     adress = json_Object["data"][1]["Adress"]["city"]  # Tem que mudar o 1 pa 0
#     print(json_Object)
#     print(name)
#     print(adress)

#     a = json.dumps(json_Object, indent=4)
#     print(a)


####** Importar ficheiros Excel recorrendo ao pandas(função do Python)

# import pandas as pd
# a = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\Employee_data.xlsx"
# all_data = pd.read_excel(a, index_col = 1)    
# email = all_data["email"].head() # Com o head() dá os 5 primeiro valores
# company_name = all_data["company_name"]
# xl = pd.ExcelFile(a)
# df = xl.parse("b") # "b" porque é o nome da folha no excel





    
    