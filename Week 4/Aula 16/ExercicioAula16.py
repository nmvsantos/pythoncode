# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 13:52:18 2020

@author: Nuno Santos
"""

### Correcção de alguns exercicios das listas e do slides

### Exercicio 3 da lista 2
# num1 = int(input("Qual o primeiro numero?"))
# num2 = int(input("Qual o segundo numero?"))

# validopera = False
# while not validopera: # aqui vai verificar se a condição é True or False
#     opera = input("Diz qual é a opera '+', '-', '*', '/'")
#     if opera == "+":
#         result = num1 + num2
#         validopera = True
#         break # Os breaks é para parar logo e não verificar cada condicional
#     elif opera == "-":
#         result = num1 - num2
#         validopera = True
#         break
#     elif opera == "*":
#         result = num1 + num2
#         validopera = True
#         break
#     elif opera == "/":
#         result = num1 + num2
#         validopera = True
#         break  
#     else:
#         print("A operação deve ser '+', '-', '*', '/'")
#         continue
# print("\nO resultado é", result)

### Resolução do exercicio acima com eval
# nu1 = input("\nFirst number: ")
# nu2 = input("\nSecond number: ")
# ope = input("\nOperator: ")

# str = nu1+ope+nu2
# print(f'\n{nu1} {ope} {nu2}')

# res = eval(str) # O eval é um metodo que executa uma expressão
# print("\nRES : ", res)


###Exercicio 3 da lista 3
# a1=0
# a2=0
# a3=0
# a4=0
# li=[2,61,-1,0,88,55,3,121,25,75]
# for i in li:
#     if i in range(0,26):
#         a1+=1
#     if i in range(26,51):
#         a2+=1
#     if i in range(51,76):
#         a3+=1
#     if i in range(76,101):
#         a4+=1
# print("1 [0,25]: ", a1)
# print("1 [26,50]: ", a2)
# print("1 [51,75]: ", a3)
# print("1 [75,10]: ", a4)

### Exercicio do guess number
# guess_number = 7
# user = True
# while user:
#     user = int(input("Escreve um numero: "))
#     if user < guess_number:
#         print("Please guess higher")
#     elif user == guess_number:
#         print("Congrats, you found the number")
#         break # Para fechar o ciclo senão o loop não pára
#     elif user ==9:
#         print("No, it is not 9")
#     else:
#         print("Please guess lower.")


### Exercicio do erro na condicional
# day = "Saturday"
# tempe = 30
# raining = True 

# if day =="Saturday" and tempe > 20 and raining == False:
#     print("go out")
# else:
#     print("Better finishing python programming exercises")
"""
Resposta:
Onde está o "or not" tem que estar "and" 
a condição do raining tem que ser "rainning==False"
"""

### Revisão sobre listas, set, dicionários e tuple

###***Listas

#import sqlite3
#from sqlite3 import Error # Importar da base de dados sqlite3 não definida
#from numpy import * # Nunca fazer isto porque importa tudo do numpy
# import numpy as np

# b =[4,6,7]
# print(np.add(b,5)) # Soma 5 a cada elemento da lista
#print(dir(np)) #imprime as funções presentes no numpy
# L = [3,True,"ale",2.7,[5,8]]
# a = [3,[109,27],4,15]
# print(a[0])  # imprime o elemento na posição 1
# print(a[1])  # Imprime o elemento na segunda posição
# print(a[1:4])  # Imprime o elemento na posição 1 ate à ultima


### Para começar uma lista do incio podemos usar o for
# li = []
# for i in range(6):
#     li.append(i)  # Aqui é para colocar o valor de i na lista vazia com nome a
# print(li)


###*** Tuple
# t = ("English", "History", "Math")
# print(t[1]) #Imprime o valor na posição 1 do tuple
# print(t.index("English")) # Indica a posição da palavra English

# t = (2,7,3,7)
# print(t.count(7)) # Indica quantas vezes se repete o valor 9 no tuple
# print(max(t))
# print(t+t) #NOTA não soma os valores, junta 2 vezes o tuple
# print(t*2)

#print((1,2)==(2,1)) # Como são orderes não são iguais, dá resultado False

#         ## Para remover tuple temos que converte para lista
# t = (1,9,3,9)
# x = list(t)
# x.remove(1)
# t = tuple (x)
# print(t)

# a = (1,2)
# b = (3,4)
# c = zip(a,b) # O zip serve para unir os elementos das 2 tuple conforme a ordenação
# x= list(c) # É preciso converter sempre para lista para poder imprimir
# print(x)
# print(x[0])

# z = ((1,3),(2,4))
# u = zip(*z) # zip com o * troca os valor mas não mexes nem destroi o tuple
# print(list(u))


###*** Dicionário
# k = {"red" , "green"}
# v = {"#FF0000","#008000"}
# z = zip(k,v) # Junta os valores conforme a sequencia
# d = dict(z)
# print(d)

# num = {"ali" : [12,13,8], "Sara": [15,7,14]}
# d={k:sorted(v) for k,v in num.items()}
# print(d)
# print(list(num.keys())) # Print as keys do dict
# print(list(num.values())) # Print os values do dict

# d = {"Nome":"Nuno" , "Apelido":"Santos", "idade":"32"}
# d["Nome"] = "João" # Altera o value da key "Nome" para "João
# print(d)

# x = d.get("Apelido")
# print(x)
# d.pop("idade") # Retira o key e value referente ao key "idade"
# print(d)

###*** Set

# f = {"Apple", "orange", "banana"}
# f.add("Cherry")
# print(f)
# f.update(["Mango","grapes"])
# print(f)
# """
# Tanto o add como o update tem a mesma função, colocar valores no set
# """
# f.remove("banana")
# print(f)

# x = {1,2,3}
# y = {2,3,4}
# print(x.union(y)) # Une o set y ao x, o valores que forem repetidos, ele não os coloca
# print(x|y)

# print(x.intersection(y)) # Só imprime os valores que estão nas duas listas
# print(x&y)

# x = {1,2,3}
# y = {1,2,3,4,5}
# print(x.issubset(y)) # Se True quer dizer que o set x está dentro do set y
# print(y.issubset(x)) # Não está dentro do set


### Revisão sobre abrir/importar ficheiros txt, excel, csv

# epopeia = open("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt", "r")
# """
# NOTA para abrir o ficheiro tem que ter sempre \\
# """
# for line in epopeia:
#     #print(line)
#     if "salvamento" in line.lower(): #imprime as linhas onde está a palavra
#         print(line)
# epopeia.close()
"""
Se abrirmos o ficheiro através do for é preciso SEMPRE .close()
"""
### Outra forma de abrir é com o with
# with open("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt", "r") as epopeia:
#     for line in epopeia:
#         if "tempestade" in line.lower(): 
#             print(line)
# print(line)  # Line fica com um tipo de str


"""
Para eliminar o erro dos caracteres especiais como acentos temos que usar o
encoding = "utf-8"
"""
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     """
#     O encoding coloco os acentos e caracteres especiais
#     """
#     line = epopeia.readline()
#     while line:
#         print(line,end="") # Com o end="" retirar o espaço entre as linhas
#         line = epopeia.readline() 

### Diferença do readlines
# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# ## É recomen colocar o path como variavel
# ## E colocar uma variável para o encoding para não escrever sempre (encod)
# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     poem = epopeia.readlines() # vai juntar todas as linhas numa unica e
#     print(type(poem))          # vamos obter uma lista, recomend readlines
# print(poem)

### Com o read
# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"
# ## É recomen colocar o path como variavel
# ## E colocar uma variável para o encoding para não escrever sempre (encod)
# with open(file_path,"r", encoding = encod) as epopeia:
#     text = epopeia.read() # Com o read coloca o texto como está no ficheiro
#     print(type(text))     # Lendo o ficheiro inteiro
# print(text) # Retorna um sting com todo o texto


# encod = "utf-8"
# file_path = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\epopeia.txt"

# with open(file_path, "r", encoding="utf-8") as epopeia: 
#     poem = epopeia.readlines() 
#     text = epopeia.read()

# # for linea in poem[::-1]: # Imprime de baixo para cima
# #     print(linea, end="")
# # print(type(poem))
# for linea in text[::-1]: 
#     print(linea, end="") #não imprime porque é uma string e não uma list
# print(type(text))


### Abrir ficheiros Json, um tipo usado no java
"""
No ficheiro tipo json temos que gravar com .json
"""
# import json  # Existe uma função no python para ler ficheiro json
# file_json = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\JJson.json"
# with open(file_json) as json_file:
#     json_Object = json.load(json_file) # Para acessar aos dados do Json
#     name = json_Object["data"][0]["name "]
#     #adress = json_Object["data"][1]["Adress"]["city"]  #Não funciona por tem 1
#     adress = json_Object["data"][0]["Adress"]["city"]  # Tem que mudar o 1 pa 0
#     print(json_Object)
#     print(name)
#     print(adress)

### Mas se o json tiver mais do que um dado
import json
file_json = "C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 16\\Json2.json"
with open(file_json) as json_file:
    json_Object = json.load(json_file) # Para acessar aos dados do Json
    name = json_Object["data"][0]["name "]    
    adress = json_Object["data"][1]["Adress"]["city"]  
    print(json_Object)
    print(name)
    print(adress)
### para converter python em json
    a = json.dumps(json_Object, indent=4) # Convert Python para json
    print(a)

















