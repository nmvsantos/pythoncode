# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 08:40:20 2020

@author: Nuno Santos
"""

### aula 20
####################****** Review numpy*****#############
import numpy as np

"""
Num array está limitado o tipo de dados, ou seja, todos os elementos são do
mesmo, ou só inteiro, ou só float

Numpy.ndarray é um novo datatype 

.shape dá o numero de elementos

Para converter em numpy array, todos têm que ter o memso numero de elementos
senão vai dar erro

"""
# import numpy as np
# b=np.array([(1,2), (3,4)]) # Converte uma lista de tuples em matrix
# print(b)
"""
Temos um matrix 2by2
"""
# import numpy as np
# b=np.array([(1,2), [3,4]]) # Também podemos converter 1 tuple e 1 list em matrix
# print(b)

"""
b[1][1] é o mesmo que usar b[1,1] isto quando queremos imprimir o elemento 1 em 1
"""
# b=np.array([(1,2), [3,4]]) # Também podemos converter 1 tuple e 1 list em matrix
# print(b)
# print(b[0:2,1]) # Aqui vai analisar os primeiro intervalos e depois vai imprimir
#                 # o segundo elemento de cada intervalo
# print(b[1, :]) # Para imprimir a linha 1
# print(b[: , 0]) # Aqui imprime todos os elementos na posição 0 de cada intervalo

"""
Se fizermos b<3 dá uma matriz com verdadeiros e falsos
Mas se fizermos b<3.sum() assim já dá um valor de quantos trues
Se fizermos b<3.sum(axis=0) dá um matriz com o valor de Trues em cada coluna

x.T -> passa de linhas para colunas e viceversa

Só é possivel multiplicar matrizes quando têm a mesma tipologia

Com a função dot podemos multiplicar matrizes diferente mas o nº de colunas
tem que ser o mesmo numero de linhas do outro. 
"""

"""
Linspace divide um intervalo de numero em distancias iguais
"""
# a =np.linspace(1,10,num=5) #Começa no 1 e vai ate 10 dá 5 numeros dividos igual
# print(a)

"""
empty é bom para criar matriz vazia
full cria a matriz com os dados fornecidos
sort ordena do menor para o maior
"""


# np.random.seed(543563) 
# print(np.random.randint(4,size=(2,3))) #randint dá random de inteiros
""" 
Se cololocarmos np.random.seed(543563) ele dá sempre o mesmo random em qualquer
computador
"""

####################****PANDAS****###############
"""
Pandas é um novo tipo de dados como list
No pandas existe 2 tipos de datostructure Series and dataframe
Series é pegar numa lista ou array e colocar um index

s.value_counts conta quantos elementos iguais tens no Series
"""

"""
Dataframe
É uma tabela com várias colunas
df["c2"]["l2"] para acessar ao dado temos que colocar sempre coluna primeiro
e a linha depois

iloc acessar os membros através da localização
df.iloc[0:2,0:1] 0:2 é para as linhas e o 0:1 é para as colunas

.head(2) mostras as 2 primeiras linhas do dataframe
.trail(2) mostra as ultimas 2 linhas do dataframe

df["C2"]["L1"] = 2 ele mudao o valor para 2 naquela posição

.drop para remover ou um coluna ou linha mas não para sempre existe a opção
de colocar "inplace=True" na formula ele mantem as alterações para sempre.

sort_values(by = "C1") ordena por valores a coluna C1

.describe dá informação sobre o data frame como media, max, min, frequencia

df["C1"].mean() ou df.C1.mean()
 dá o valor da média de C1, podemos fazer para todos os dados
que estão no describe

df.index = ["first" , "Second"] com esta função podemos definir o index para o
dataframe

df.iloc[[0,2]] ele imprime só as linhas numero 0 e 2 












