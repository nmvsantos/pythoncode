# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 08:31:32 2020

@author: Nuno Santos
"""
###########************** O código de 05/11/2020 não e preciso no repositorio
####### para avaliação

# for i in range(2):
#     for j in range(2):
#         print([[i,j]])

######Hackerrank exercicio de Python
##Slide 1 da aula 18
# import numpy as np
# x = np.array([[-1,3],[4,2]])
# y = np.array([[1,2],[3,4]])
# z = np.dot(x,y) # Faz a soma dos dois array
# print(x)
# print(y)
# print("\n",z)

### Pandas usado para Tratamento de dados vamos focar em series e dataframe
##Versão do panda

import pandas
print(pandas.__version__)

import numpy as no
import pandas as pd

# data= pd.Series([0.25,0.5,0.75,1.0]) ## Series sempre com maiuscula
# print(data)
# print(data.values)
# print(data.index)
# print(data[0])
# print(data[1:3])

# data= pd.Series([0.25,0.5,0.75,1.0], ["a", "b", "c", "d"])
# print(data) # Atribui automaticamente a letra ao numero
# print(data["b"])

# data= pd.Series([0.25,0.5,0.75,1.0], [2,5,3,7])  # Atribui o index ao numero
# print(data)
# print(data[5])

# population_dict = {"california": 3833, "texas":2252, "NY": 1965}
# population = pd.Series(population_dict)

# print(population)
# print(population["california"])
# print(population["california":"texas"])

### Criar series
print(pd.Series([2,4,6]))

print(pd.Series(2 , index=[100,200,300]))

print(pd.Series({2:"a", 1:"b", 3:"c"}, index=[3,2]))









