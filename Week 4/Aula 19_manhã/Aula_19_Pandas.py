# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 16:44:25 2020

@author: Nuno Santos
"""

#### Aula sobre Pandas
import pandas as pd
# data = pd.Series([0.25, 0.5, 0.75, 1.0]) #Aqui cria tabela com os dados mencio
# print(data)
# print(data.values) #imprime os valores do data
# print(data.index) # TEM INDEX com este controlo consigo saber quantos valores tem
# print(data[1]) # dá o valor na posição 2


##Series com index
# data = pd.Series([0.25, 0.5, 0.75, 1.0], index=['a', 'b', 'c', 'd'])
# print(data) # vai juntar cada elemento do Series ao index
# print(data['b']) #para acessar ao valor d Series print o "b" porque é o novo Ser


### Series com dicionários
# population_dict = {'California': 38332521,
#                    'Texas': 26448193,
#                    'New York': 19651127,
#                    'Florida': 19552860,
#                    'Illinois': 12882135}
# population = pd.Series(population_dict)
# print(population)
# print(population['California']) # Dá o valor associado ao index "California"


### Criar series de raiz
# print(pd.Series([2, 4, 6])) #cria a série com os numeros dados
# print(pd.Series(5, index=[100, 200, 300])) #Para cada index coloca o valor 5

# se=pd.Series({2:'a', 1:'b', 3:'c'}, index=[3, 2])
# print(se) # Só cria o series para o index dado


### Panda Dataframe
# area_dict = {'California': 423967, 'Texas': 695662, 'New York': 141297,
#              'Florida': 170312, 'Illinois': 149995}
# area = pd.Series(area_dict)

# population_dict = {'California': 38332521,
#                    'Texas': 26448193,
#                    'New York': 19651127,
#                    'Florida': 19552860,
#                    'Illinois': 12882135}
# population = pd.Series(population_dict)

# print(population)
# print(area)

# states = pd.DataFrame({'population': population,
#                        'area': area})
# """
# Com o dataframe permite criar um dataframe juntando 2 dicionários
# """
# print(states)
# print(states.index) #Dá a decriçao do index
# print(states.columns) #Dá a descrição das colunas


#### Series data.items
# data = pd.Series([0.25, 0.5, 0.75, 1.0],
#                  index=['a', 'b', 'c', 'd'])
# data['e'] = 1.25 # aqui adiciona o dado "e" com 1.25 ao data
# print(data)
# print(data[(data > 0.3) & (data < 0.8)]) #Só imprime os valores dentro daquele
#                                         #intervalo


### loc, iloc e ix
# data = pd.Series(['a', 'b', 'c'], index=[1, 3, 5])
# print(data)
# print(data.loc[1]) #só imprime o valor onde está o index
# print(data.loc[1:3])

###Index
# area = pd.Series({'Alaska': 1723337, 'Texas': 695662,
#                   'California': 423967}, name='area')
# population = pd.Series({'California': 38332521, 'Texas': 26448193,
#                         'New York': 19651127}, name='population')
# print(area,"\n",population)
# print("\n",population / area) #Dá para fazer operações, população por m2


A = pd.Series([2, 4, 6], index=[0, 1, 2])
B = pd.Series([1, 3, 5], index=[1, 2, 3])
print(A + B) # soma só valores onde o index é igual












