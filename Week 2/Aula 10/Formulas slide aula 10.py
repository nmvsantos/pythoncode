# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 19:14:43 2020

@author: Nuno Santos
"""

### Aula10 Dictionary 
### Slide 15 - 17
# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}

# ## Para imprimir só o primeiro digito
# print(d["brand"])  

# ## Para colocar novo membro no dicionário
# d["month"] = "July" 
# print(d)

# ## Para alterar um value
# d["color"] = "grey" 
# print(d["color"])

###Slide 18 função get para ober um valor do dicionário
# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}
# a = d.get("model")
# print(a)
# ### Mas podemos usar a função para verificar se o valor está presente no dictio
# ### Se não tiver, apresenta o valor que identificarmos
# b = d.get("marca" , "n/a")
# print(b)

### Slide 19 para imprimir a lista completa de keys ou values
# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}
# print(d) ## Aqui imprime os keys e values como dicionário
# print(list(d.keys())) ##imprime só os keys atenção ao () depois do keys
# print(list(d.values())) ##imprime só os values atenção ao () depois do values
# a = list(d.items())
# print(list(d.items()))  #pint mas os dados são aprese numa lista

### Slide 20 e 21 função pop e popitem
# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}
# a = d.pop("color")  #retira o valor do dictio que quisermos e guarda-o
# print(a) # Aqui onde está guardado
# print(d) # O dictionário já sem o key e value

# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}
# f = d.popitem() # Retira o ultimo valor do dictionário e o () é obri estar vazio
# print(f)  # O valor guardado
# print(d)  # A lista sem o valor

##Slide23 A Exercise, 
# a = ["x", "y", "x" , "z", "y", "x"]
# d = {}
# for i in a:
#     if i in d:
#         d[i] += 1
#     else:
#         d[i] = 1
# print(d)
## Outra resolução mas com o get 
# a = ["x", "y", "x" , "z", "y", "x"]
# d = {}
# for i in a:
#     d[i] = d.get(i,0) +1
# print(d)

### Slide 27 Setdefault 
### Esta função é a mais indicada se queremos criar um dicionário de raiz
# d = {}
# for i in range(21):
#     d[i] = d.setdefault(i, str(i))
# print(d)

### Slide 28 Copiar dicionário
# d = {"brand" : "cherry", "model" : "arizo5" , "color": "wuite"}
# a = d   ## Aqui são dependente, ou seja, se mudar num sitio muda no outro
# b = d.copy()  ## Aqui são independente, ou seja, se um mudar o outro não muda

### Slide 42 Ordenar um dicionário por keys ou values
# d = {"a":4, "b":2, "f":1, "d":1, "c":1}
# import operator  #Usamos um função do modulo operator
# a = operator.itemgetter(1)  ### Ordenação por values para isso coloca~se "1"
# print(sorted(d.items(),key = a))

# b = operator.itemgetter(0)  ### Ordenação por keys para isso coloca~se "0"
# print(sorted(d.items(),key = b))

### Slide43
# num = {"Zé" : [20,113,5], "Manuel" :[22,10,25], "Rui" : [1,9,5]}
# d = {k: sorted(v) for k,v in num.items()}
# #### K:sorted(v) é como queremos o nosso dicionário e ordena a parte do values
# print(d)

### Slide 44-45 Juntar 2 dicionários
# d1 = {"x":3, "y":2 , "z":1}
# d2 = {"w":8, "t":7, "z":0}
# d1.update(d2)  ### Para atualizar usamos a função ?.update(!)
# print(d1) ## Nota o d1 no key "z" assume o valor do d2 eliminando registo do d1

### Podemos usar também o loop
# d1 = {"x":3, "y":2 , "z":1}
# d2 = {"w":8, "t":7, "z":0}
# d={}
# for i in (d1,d2):
#     d.update(i)
# print(d)

### Podemos usar também o {**d1,**d2}
# d1 = {"x":3, "y":2 , "z":1}
# d2 = {"w":8, "t":7, "z":0}
# d = {**d1, **d2}
# print(d)

### Slide 49 Zip function
### Uma das funções do zip é juntar 2 lista num diciona
# k = ["Nuno", "Santos"]
# v = ["Nome", "Apelido"]

# z = zip(k,v) # Aqui junta os primeiros elementos de cada lista
# d = dict(z)  # Converte a lista em dicionário
# print(d)

### Slide 61
# family = {"child1":{"name":"James", "age": 8}, 
#           "child2":{"name":"Emma", "age": 20}}
# print(family) # Assim imprime tudo
# print(family["child1"]) # Só imprime o primeiro elemento
# print(family["child1"]["name"]) # Imprime o primeiro valor do primeiro elemento

### Slide 61
# d = {"F":0 , "B" :0}

# import random
# for i in range(17):
#     d[random.choice(list(d.keys()))] += 1
# print(d)

###****************************Exercises****************************###

### Exercis 1
# a = "abfabdcaa"
# d={}
# for i in a:
#     d[i] = d.get(i,0) + 1
# print(d)

### Exercise 2
# a = "a dictionary is a datastructure."
# b = a.split() ### Divide o string em palavra conforme o espaço em branco
# d = {}
# for i in b:
#     d[i] = d.get(i,0) +1
# print(d)

### Exercise 3
# a = "a dictionary is a datastructure \na sit is also a datastructure."
# print(a)
# s1 = a.strip(".")
# print(s1)
# s2 = s1.split()
# print(s2)
# d= {}
# for i in s2:
#     d[i] = d.get(i,0) +1
# print(d)

### Exercise 4
# d = {"a":4, "b":2, "f":1, "d":1, "c":1}
# c = 0
# for i in d.values(): ### Nota importante a seguir ao d.values sempre "()"
#     c +=i
# print(c)
## Ou podemos fazer
# d = {"a":4, "b":2, "f":1, "d":1, "c":1}
# c = 0
# for i in d:
#     c += d[i]
# print(c)

###Exercise 5
# d1 = {"x":3, "y":2 , "z":1}
# d2 = {"w":8, "t":7, "z":5}

# for i,j in d2.items():
#     if i in d1:
#         d1[i] += d2[i]
#     else:
#         d1.update({i:j})
# print(d1)


### Exercise 6
# s = "Python Course"
# z = ["o", "r"]
# d = {}
# for i in s:
#     if i in z:
#         d[i] = d.get(i,0) + 1
#     else:
#         continue
# print(d)

### Exercise 7
# d = {"x" : 3, "y" : 2, "z" : 1, "y" : 4, "z" : 2}  
# print(d)
# for k,v in d.items:
#     if k not in r.keys():
#         r[k] = v
# print(r)
#### Answer Porque o dicionário não pode aceitar mais do que uma vez a mesma key
#### Ao fazer "print(d) imprime a soma dos values das keys repetidas

###Exercise 8
# student = [{"id": 123, "name" : "sophia", "s" : True}, 
#            {"id": 378, "name" : "William", "s" : False}, 
#            {"id": 934, "name" : "Sara", "s" : True}]

# ##a = student[0]["s"] ## Para mostrar o valor
# c = 0
# for i in student:
#     c += int(i["s"]) ## Fizermos o i["s"] acessamos ao valor True ou ao 3º valor
# print(c)

### Outra solução
# student = [{"id": 123, "name" : "sophia", "s" : True}, 
#             {"id": 378, "name" : "William", "s" : False}, 
#             {"id": 934, "name" : "Sara", "s" : True}]

# print(sum(i["s"]for i in student))  




### Homework
# person = {"phone" : {"home": "01-4455","mobile": "918-123456"}, "children": ["Olivia" , "Sophia"], "age" : "48", "name" : "Ahmed"}

# print(len(person))
# print(person["phone"]["home"])
# print(person["phone"]["mobile"])
# print(person["children"])
# print(person["children"][0])
# print(person.pop("age"))
































