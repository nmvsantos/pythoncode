# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 08:32:36 2020

@author: Nuno Santos
"""

#  Dictionary

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# print(d["n"])  #print o valor atribuido ao key
# d["p"] = "four"  # para adicionar ou alterar dados no dicionário
# print(d)
# z = d.get("n")  # podemos imprimir o valor referente ao key escolhido
# print(z)
# y = d.get("f" , -1) # vai procurar o valor "f" se não estiver print o -1
# print(y)

# x = "n" in d # Isto serve para avaliar se o valor "n" está no dict d como key
# print(x)

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# print(list(d.keys()))     #imprime todos os keys do dicionario
# print(list(d.values()))   # imprime todos os values do dicionario
# print(list(d.items()))    # imprime todos os valores do dictionario

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# for k,v in d.items():     # Loop para fazer o unpack do dictionário quando vem
#     print(k, ":", v)      # d.items
#     print("Key" , k , "is:" , k)
#     print("Value" , v , "is:" , v)
 
#d = {"n" : "one" , "m": "two" , "o" : "three"}
# c= 1
# for k,v in d.items():  
#     print("Key" , c , "is:" , k)
#     print("Value" , c , "is:" , v)
#     c +=1

#d = {"n" : "one" , "m": "two" , "o" : "three"}    
# d.pop("m")  #elimina o key e o value "m"
# print(d)

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# f= d.popitem() # Elimina o ultimo key and value do dictio e guarda o valor removido
# print(d)
# print(f)  # O valor removido fica com o formato tuple

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# d.clear()   #O clear limpa os dados do dicionário
# print(d)

#d = {"n" : "one" , "m": "two" , "o" : "three"}
# del d       #O delete elimina completamente o dicionário
# print(d)
     
#Slide 24   
# a = ["x", "y", "x", "z", "y", "x",]  
# d = {}  
# for i in a:
#     if i not in d:
#         d[i] = 1
#     else:
#         d[i] += 1
# print(d)

#Slide 25 Solution 2
# a = ["x", "y", "x", "z", "y", "x",]   
# d = {}  
# for i in a:
#     d[i] = d.get(i,0) +1  
# print(d)
    
#Slide 26 Solution 3
# a = ["x", "y", "x", "z", "y", "x",]   
# d = {}   
# for i in a:
#     d[i] = d.setdefault(i,0) +1  
# print(d)   
    
#o que pretendemos d3 = {1:"1" , 2 : "2", ... , 100,"100"}
# d3 = {}
# for i in range(1,101) :
#     d3.setdefault(i,str(i))
# print(d3)
    
# d = {"n" : "one" , "m": "two" , "o" : "three"}  
# b = d           # São dependente se um mudar o outro muda
# c = d.copy()    # São independente se um mudar o outro não muda
    
#Slide 30 Exercise 1
# a = "abfabdcaa"    
# d = {}
# for i in a:
#     if i not in d:
#         d[i] = 1
#     else:
#         d[i] += 1
# print(d)    
# #or
# d3 = {}
# for i in a:
#     d3[i] = d.get(i,0) +1  
# print(d)
    
#slide 32 Exercise 2
# line = "a dictionary is a datastructure."
# d = {}
# s = line.split() # para separar uma stringem lista e conforme o espaço
# print(s)
# for i in s:
#     d[i] = d.get(i,0) + 1
# print(d)

#slide 33 Exercise 3
# # My solution
# lines = 'a dictionary is a datastruture \n a set is also a datastruture.'
# d = {}
# s = lines.split() # para separar uma stringem lista e conforme o espaço
# print(s)
# for i in s:
#     if i == "datastruture.":
#         d["datastruture"] = d.get("datastruture",0) + 1
#     else:
#         d[i] = d.get(i,0) + 1
# print(d)

#Resol prof
lines = 'a dictionary is a datastruture\na set is also a datastruture.'
# print(lines)
# s = lines.split("\n")[0]       #Como temos 2 linhas podes colocar numa linha 
# s1 = lines.split("\n")[1]      #só dado referente ao primeiro membro
# s1 = s1.split(".")[0]          # Para retirar o . e ficar só com a primeira parte

#s = lines.split("\n")[0].split(".")[0]
#Podemos escrever as 2 ultimas linhas desta maneira
#s1 = lines.split("\n")[1].split(".")[0]

# print(s)
# print(s1)

# i = 0 , 1
# d = {}
# for i in range(2) :
#     s = lines.split("\n")[i].split(".")[0]
#     #s1 = lines.split("\n")[1].split(".")[0]    
#     s1 = s.split()    
#     for i in s1:
#         d[i] = d.get(i,0) + 1
# print(d)

#Slide 38 Exercise 4
# d = {"a": 4, "b" : 2, "f" : 1, "d" : 1, "c" : 1}
# #d = {"a": d["a"], "b" : d["v"], "f" : d["f"], "d" : d["d"], "c" : d["c"]}

# c= 0
# for k,v in d.items():
#     c +=v
# print(c)
# #ou
# c= 0
# for i in d:
#     c += d[i]
# print(c)
# #ou
# print(sum(d.values()))

#Slide41
# d = {"a": 4, "b" : 2, "f" : 1, "d" : 1, "c" : 1}
# import operator
# k = operator.itemgetter(1)  #Ordena por valores
# print(sorted(d.items(), key =k))
# z = operator.itemgetter(0)  #Ordena por keys
# print(sorted(d.items(), key =z))

# Slide 42 quando queremos dividir um dicionario com uma lista dentro usamos 
# usamos outra função

#slide 43 podemos atualizar 1 lista com dados de outra com .update()

#Slide 44
# d1 = {"x": 3 , "y" : 2 ,"z" : 1}
# d2 = {"w": 8 , "t" : 7 ,"z" : 5}

# d = {}
# for i in (d1,d2) :
#     print(i)
#     d.update(i)
# print(d)


# d1 = {"x": 3 , "y" : 2 ,"z" : 1}
# d2 = {"w": 8 , "t" : 7 ,"z" : 5}
# d = {**d1 , **d2}
# print(d)

#Slide 45 Exercise 5
d1 = {"x": 3 , "y" : 2 ,"z" : 1}
d2 = {"w": 8 , "t" : 7 ,"z" : 5}


for i,j in d2.items() :
    if i in d1:
        d1[i] += d2[i]
    else:
        d1.update({i : j})
print(d1)



























