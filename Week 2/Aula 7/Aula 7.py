# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:33:26 2020

@author: Nuno Santos
"""

### Aula 7
#***** Slide 20
# import math
# print(math.pi) 

# #or podemos definir um novo nome para a função

# import math as m
# m.pi
# print(m.pi)

#**** Slide 21
# from os import getcwd
# print(getcwd())

# #or

# from os import getcwd as gc
# print(gc())
# a = gc()


# from math import pi
# print(pi)  #só funciona com o pi o print(math.pi) não funciona porque só escolhemos
            #uma função de um modulo assim o programa torna mais rápido

#****** Slide 22 se quiser mais do que um função do mesmo modulo é separa
                # as funções com uma ","

# import numpy as np
# import matplotlib.pyplot as plt
# data = np.array([-20,-3,-2,-1,0,1,2,3,4])
# plt.boxplot(data)

#or

# import numpy as np
# from matplotlib import pyplot as plt
# data = np.array([-20,-3,-2,-1,0,1,2,3,4])
# print(plt.boxplot(data))

#**** Exer about conditions (Extra)
# Check the number more than 100, equal 100, less tha 100
# Check: id it odd, even

# a = 150

# if a > 100:
#     if a%2 ==0:
#         print("odd")
#     else:
#         print("even")
# elif a == 100 :
#     if a%2 ==0:
#         print("odd")
#     else:
#         print("even")
# else:
#     if a%2 ==0:
#         print("odd")
#     else:
#         print("even")


#**** Slide 27
# import random
# import math

# for i in range(5) :
#     print(random.randint(1, 25))
# print(math.pi)

#***** Slide28
# import math
# print(dir())
# print(dir(math))

# s = "a"
# print(dir(s))

#**** Slide 29  Para saber o que faz cada função
# a = help(len)
# print(a)

# print(len.__doc__)

#***** Extra sobre funções por nós definida para a função funcionar 
        #no fim temos que escrever o nome da função

# def my_fun() :
#     """
#     Extra sobre funções para a função funcionar no fim temos que escrever
#     #Esta informação só aparece se fizermos my_fun.__doc__ na consola

#     """
#     print("Python")

# my_fun()

#***** Slide 32-36 Exercic
# import math
# n = math.fmod(9,4)
# b = math.gcd(30,4)
# c = math.fabs(-4)
# print(n)
# print(b)
# print(b)

# import random
# d = random.randint(1,5)  # escolho um numero entre o 1 e o 5
# f = random.choice([1.5]) # só escolhe o numero 1 ou 5
# a = [1,2,3,4]
# random.shuffle (a) # reorganiza os numeros de maneira diferente
# print(d)
# print(f)
# print(a)
# print(f'{math.pi :.2f}') #se quisermos uma formatação só com 2 casas deci

# import sys
# print(sys.version)

# import datetime
# now = datetime.datetime.now()
# print(now)
# print(now.year)
# print(now.month)
# print(now.day)

# print(datetime.datetime.today())

#**** Exercice 3
# import datetime
# now = datetime.datetime.now()
# minu = now.minute
# print("minute" , now.minute)

# if minu%2 == 0 :
#     print("Not a odd minute")
# else:
#     print("Odd minute")
    
# #or usando o today
    
# import datetime
# tod = datetime.datetime.today()
# m = datetime.datetime.today().minute
# print("minute" , tod.minute)
# if m%2 == 0 :
#     print("Not a odd minute")
# else:
#     print("Odd minute")

# outra forma de calcular odd com o list de odds
# import datetime
# tod = datetime.datetime.today()
# m = datetime.datetime.today().minute
# odd_list = [i for i in range(1,60,2)]
# if m in odd_list :
#         print("odd minute")
       
# else:
#         print("Not an odd minute")
       
    
# Outra forma de calcular o Odd com o for
# import datetime
# m = datetime.datetime.now().minute
# print(m)
# check = False
# for i in range(1,60,2):
#     if m == i :
#         print("Odd")
#         break
#     else:
#         print("not an odd")
#         break

      


