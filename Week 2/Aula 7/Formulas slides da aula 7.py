# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 18:37:38 2020

@author: Nuno Santos
"""

### Slide 22 Para ver grafico através do separador Plots

# import numpy as np
# import matplotlib.pyplot as plt
# data = np.array ( [ 20 , 3 , 2 , 1 , 0 , 1 , 2 , 3 , 4] )
# plt.boxplot( data )

###Slide 23 Exercise 1
# import numpy as np
# import matplotlib.pyplot as plt
# data = np.array ( [ -20 , -3 , -2 , -1 , 0 , 1 , 2 , 3 , 4] )
# plt.boxplot( data )

###Solution of exercise 1
# import numpy as np
# from matplotlib import pyplot as plt
# data = np.array ( [ -20 , -3 , -2 , -1 , 0 , 1 , 2 , 3 , 4] )
# plt.boxplot( data )

### Slide 32 Exer 2 Formulas da aula
# import math
# print(math.fmod(9,4))
# print(math.gcd(30,4))
# print(math.fabs(-4))

# import random
# print(random.randint(1,5))
# print(random.choice([1,5]))

# a = [1,2,3,4]
# random.shuffle(a) # não é possivel fazer o print directo da função tem que ficar separa
# print(a)

### Slide 35
# import sys
# print(sys.version) 
# print(sys.platform) 

# import platform
# print(platform.release() )

### Slide 36 Importar função e utilizar alguma funções
# import datetime
# now = datetime.datetime.now()
# print(now ) 
# print(now.year) 
# print(now.month) 
# print(now.day) 
# print(datetime.datetime.today() ) 
# type(now) 

###Ex. 3 2 formas de resolver
# import datetime
# m = datetime.datetime.now().minute
# print(m)
# check = False
# for i in range(1,60,2):
#     if m == i :
#         print("Odd")
#         break
#     else:
#         print("not an odd")
#         break
    
# from datetime import datetime
# m = datetime.now().minute
# print(m)
# odds_list = [i for i in range(1,60,2)]
# if m in odds_list:
#     print("Odd minute")
# else:
#     print("Not an odd minute")
















