# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 15:37:48 2020

@author: Nuno Santos
"""

#****
#Difference between list, set, tupple and dictionary


# Python3 program to demonstrate  
# List  
   
# Creating a List 
# List = [] 
# print("Blank List: ") 
# print(List) 
   
# # Creating a List of numbers 
# List = [10, 20, 14] 
# print("\nList of numbers: ") 
# print(List) 
   
# # Creating a List of strings and accessing 
# # using index 
# List = ["Geeks", "For", "Geeks"] 
# print("\nList Items: ") 
# print(List[0])  
# print(List[1])


# #Python3 program to demonstrate  
# #Tuple

# #Creating an empty Tuple 
# Tuple1 = () 
# print("Initial empty Tuple: ") 
# print (Tuple1) 
   
# # Creating a Tuple with 
# # the use of list 
# list1 = [1, 2, 4, 5, 6] 
# print("\nTuple using List: ") 
# print(tuple(list1)) 
   
# #Creating a Tuple  
# #with the use of built-in function 
# Tuple1 = tuple('Geeks') 
# print("\nTuple with the use of function: ") 
# print(Tuple1)


# #Python3 program to demonstrate  
# #Set in Python 
   
# Creating a Set 
# set1 = set() 
# print("Intial blank Set: ") 
# print(set1) 
   
# # Creating a Set with 
# # the use of Constructor 
# # (Using object to Store String) 
# String = 'GeeksForGeeks'
# set1 = set(String) 
# print("\nSet with the use of an Object: " ) 
# print(set1) 
   
# # Creating a Set with 
# # the use of a List 
# set1 = set(["Geeks", "For", "Geeks"]) 
# print("\nSet with the use of List: ") 
# print(set1)


## Python3 program to demonstrate  
## Dictionary in Python 
# people = {"name": "joao","age": 39,"skylls": ['python', 'ruby', 'php']}
# print(people["name"])
# print(people["age"])
# print(people["skylls"])

# my_dict = { "1": "a", "2": "b", "3": "c" } 
# print(my_dict.items()) # dict_items([('1', 'a'), ('3', 'c'), ('2', 'b')]) 
# print(my_dict.keys()) # dict_keys(['1', '3', '2']) 
# print(my_dict.values()) # dict_values(['a', 'c', 'b'])


#*** para mostrar que a lista é editavel mas o tuple não
list_num = [1,2,3,4]
tup_num = (1,2,3,4)
print(list_num)
print(tup_num)
#Output:
[1,2,3,4]
(1,2,3,4)
list_num[2] = 5
print(list_num)
#tup_num[2] = 5

