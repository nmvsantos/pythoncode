# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 15:18:01 2020

@author: Nuno Santos
"""

#***** Slide 36
# import math
# n = -16
# if n < 0 :
#     n = abs(n)
    
# print(math.sqrt(n))

#**** Slide 38
# x = 3
# y=  2
# if x == 1 or y ==1 :
#     print("OK)")
# else :
#     print("NOK")

# names = ["Sara", "Taha", "Farshid"]
# if "ali" in names:
#     print("found")
# else:
#     print ("not found")

#**** Slide 40

# my_list = ["a", "e", "i", "o", "u"]
# if "o" in my_list:
#     s = "yes"
# else :
#     s = "no"
# print(s)

#*****41
# grade = 12
# if grade <10:
#     s = "fail"
# else:
#     s = "pass"
# print(s)

# #or
# grade = 12
# s ="fail" if grade < 10 else "pass"
# print(s)

#******Slide 43 IF/ELIF/ELSE
# score = 101
# if score >100:
#     l = "Error grade"
# elif score >= 90:
#     l = "A"
# elif score >= 80:
#     l = "B"
# elif score >= 70:
#     l = "C"
# else :
#     l ="D"
# print(l)

#**** Slide 45 and 46
# for j in range (5,10,2):
#     print(j , end = "", )

# for j in range(5,10):
#     print(j, end = "")

# # for j in range(4):
# #     print(j , end = "")

# s = "python"
# for ch in s:
#     print( ch)

#**** Slide 47
# word = "Python"
# c = 0
# for i in word:
#     c = c+1
# print (c)

#**** Slide 48 print quanto "a" tem na plavra
# wor = "alireza"
# c=0
# for i in wor:
#     if i == "a":
#         c = c+1
# print(c)

#***** Slide 49  conta as vogais numa palavra
# name = "farshid"
# v = "aeiou"
# c=0
# for ch in name:
#     if ch in v:
#         print(ch, end="")
#         c = c+1
# print()
# print(c)
    
#***** Slide 50
# for i in range (1,4) :
#     for j in range (2,4):
#         print (j , end = "")
# print()

#***** Slide 51 Função for com "break" ou "continue"
# for i in range(5):
#     if i ==3:
#         break    #Quando chega ao break, pára a função de vez
#     else:
#         print(i, end = "")
        
# for i in range(5):
#     if i ==3:
#         continue    # quando chega ao continue ele vai directamente para o for
#     else:
#         print(i, end = "")

#***** Slide 52
# i = 1
# while i <= 3:
#     print(i, end ="")
#     i = i+1
    
#***** Slide 53  
# s = "abcdef"
# i = 0
# while True:
#     if s[i] =="d":
#         break
#     print(s[i] , end="")
#     i = i+1

#**** Slide 54 Função while com "break" ou "continue"

# n = 8
# while n > 2 :
#     n = n-1
#     if n ==5:
#         break
#     print (n , end = "")
    
# n = 8
# while n >2:
#     n = n-1
#     if n==5:
#         continue
#     print (n , end="")

#***** Slide 57
import random
n = random.randrange(0,10)
f = "No"
print(n)
while f == "No":
    a = int(input("game:guess a number between 0 to 9:"))
    if a < n :
        print ("increase")
    elif a>n:
        print("decrease")
    else:
        print("Correct, you won")
        f = "yes"
print("Thank you")













