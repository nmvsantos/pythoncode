# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 08:39:09 2020

@author: Nuno Santos
"""

# quando fazemos a >> 1 quer dizer que dividimos o "a" por 2
#quando fazemos a << 1 quer dizer que vamos multiplicar por 2
# quando fazemos a << 2 quer dizer que vamos multiplicar 2 vezes por 2

#outra maneira de escrever o codigo do if mas só com 2 condições
# my_list = ["a", "b", "c", "n"]
# s = "yes" if ("o" in my_list) else "no"
# print(s)

# # Examples with range
# c=list(range(2 , 10 , 3)) # o 2 é o primeiro digito, o 10 é o ultimo que 
#                           # não está incluido e o 3 escolhemos de 3 em 3 numeros
# print(c) 

# d=list(range(10, 2 , -2)) 
# print (d)

# e = list(range(1, 10 )) #print desde o 1 até 9
# print(e)

# f = list(range(10 )) #print desde o 0 até 9
# print(f)

#for j in range(5,10,2) :
#    print(j, end = " ")  # o end é para colocar o resultado numa linha só
    
# for a in range(3) :   #print 3 vezes o hello
#     print("Hello", end = "")

# name = "farshid"
# v = "aeiou"
# c = 0
# for ch in name:
#     if ch in v:
#         print(ch, end = " ")
#         c += 1
# print(c)

#*** outra maneira de fazer o ex de cima mas tudo numa linha
# name = "farshid"
# v = "aeiou"
# a = [ch for ch in name if ch in v]
# print(a)

#*****

# m = []
# name = "farshid joe sara"
# v = "aeiou"
# c = 0
# for ch in name:
#     if ch in v:
#         m.append(ch)  # assim imprime em forma de lista "append()"

# print(m)
# print(set(m))   # com o set só imprime as ocorrencia sem repetições

#****** For com break ou continue

# for i in range(5):  #Com o break o loop para quando o i chega ao 3
#     if i ==3:
#         break
#     else:
#         print (i, end = " ")

# for i in range(5):  #Com o continue o loop continua quando o i é 3 ele continua 
#     if i ==3        # o loop mas não imprime o 3 e vai ao if sem imprimir
#         continue
#     else:
#         print (i, end = " ")

#******

# i = 1 
# while i <= 3:
#     print (i)
#     i += 1



    