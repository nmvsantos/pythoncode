# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 08:36:36 2020

@author: Nuno Santos
"""

# a =[5,7,12]
#print(dir(a))       # dá a lista de funções que podemos usar com este tipo de dados
#print(a.pop())      # Exemplo de como usar, primeiro o nome da lista "." e a funçao
#print(a.index(7))   #Index dá em que posição na lista está o valor
#print(a[1])         # dá o valor em que se encontra na posição 1
# a[1] =8              #Altera o valor na posição 1 para o valor 8, troca o 7 pelo 8
# print(a)

#lista só string
# s = "Sara"
# print(s[1])
# s[1] = 'd'          #Este comando não funciona porque uma string não pode ser editavel.
# print(s)

# a = [1,2]
# b = [2,1]
# print(a==b)         #Mosta que as duas lista não são iguais assim as list são ordered

# Slide   para imprimir o conteudo de uma lista
# friends = ["Sara" , "Nuno", "Luis"]
# for f in friends:
#     print(f)

# for i in range(len(friends)):
#     print(friends[i])

#Exercicio slide22-25
# my_list = [1,2,23,4,"word"]     #Solução do prof
# for i in [0,1,2,3,4]:
#     if i ==len(my_list)-1:
#         break
#     print(my_list[i] , my_list[i+1])

# my_list = [1,2,23,4,"word"]    #Minha solução
# for i in range(len(my_list)):
#     if i < len(my_list)-1:
#         print(my_list[i] , my_list[i+1])
#     else:
#         break

# my_list = [1,2,23,4,"word"]     #solução do prof
# for i in range(len(my_list) -1):
#     print(my_list[i] , my_list[i+1])


# List with slicing
# a = [7,5,30,2,6,25]
# print(a[1:4])   #Imprime os numeros posicionados de 1 a 4
# print(a[:3])    #Imprime os numeros os 3 primeiros
# print(a[3:])    #Imprime os numeros os 3 ultimos
# print(a[3:0:-1])   #imprime a apartir da posição 3("2") e anda 1 casa para trás
# print(a[::-1])  # Dá a lista ao contrário
# print(a[0:5:2]) # Dá os valores mas de duas em duas casas
# print(a[6:0:-2]) #Dá os valores da direita para a esquerda mas de 2 em 2 casas
# print(a[:0:-2])  #Dá os valores da direita para a esquerda mas de 2 em 2 casas

# a[3:5] = [14,15]    #isto altera os valores na posição 3 e 5 para 14 e 15 respecti
# print(a)

# b = [4,7]
# c = b*2 #imprime a lista 2 vezes
# print(c)

#lista dentro de uma lista Slide 33
# a = [3, [109,27], 4,15]
# print(a[1])
# print(a[1][1]) #Como é uma lista, imprime o 2 elemento da lista
# print(a[1][0])
# print(len(a))   #Nota a 2ª lista é considerada um elemento

#Exercise 2
# a = [7,5,30,2,6,25]
# b = max(a)
# c = a.index(b)
# print(b) # Dá o maximo
# print(c) #Dá a posição na lista

# s = 0
# for i in [7,5,30,2,6,25]:
#     s += i
# print(s) #soma todos os valores através do loop

# a = [7,5,30,2,6,25] #solução do prof
# m = a[0]
# for i in a:
#     if i > m:
#         m=i
# print(m)

#Slide 38
#a = [1,3,6,5,3]
# print(a.count(3)) # Conta quantas vezes aparece o 3

# b = a.insert(2,13) # insere o o numero 13 na posição 2 insert(index,obj)
# print(b)

# a = [1,2,6,5,2]
# a.remove(2) # remove só o primeiro 2 para remover todos tem que ser através de um loop
# print(a)

#Slide 39
# x = [10,15,12,8]
# a = x.pop(2)  #remove o numero na posição 2 e coloca à parte para usar
# print(x)
# print(a)

#Slide 40
# z = [5,9,3]
# del z[1] #Elimina o valor na 2º posição
# print(z)

#Slide 41
# z = [0,1,2,3,4,5,6]
# del z[2:4] #Elimina mais do que 1 elemento e nas posições 2 e 3
# print(z)

#Slide 42
# z = [1,2,3]
# z.reverse() # Reverte a sequencia dos valores na lista
# b = z.reverse() #Este comando não funciona nao de ve usar
# print (b)
# print(z)

# a = [2,4,3,5,1]
# a.sort() # ordena os valores de menor para maior
# print(a)

#Slide 43
# x = [1,2,3]
# x.extend([5]) #Acrescenta o valor "5" no fim da lista
# print(x)

# y = [4,5]
# x.extend(y) #Acrescenta os valores de uma lista a outra (da lista y à x)
# print(x)

# Slide 44
# x = [1,2,3]
# x.append(4) # Acrescenta o valor directamente na lista como integer
# print(x)
# y = [4,5]
# x.append(y) # Acrescenta uma lista dentro da outra mas como lista e como 1 elemento
# print(x)

# x = [1,2,3]
# x.append([6]) # acrescenta o valor 6 como uma lista dentro da lista
# print(x)

#Slide 45  # Definir uma lista usando um loop
# a = []
# for i in range(4):
#     a.append(i)
# print(a)

#Slide 46
# x = [1,2,3]
# x.clear() # Limpa todos os valores da lista
# print(x)

# x = [1,2,3]
# z = x.copy() # Copia os valores de uma lista para outra
# print(z)

#Slide 47
# a = [1,2,3]
# b = a.copy()    
# c = a
# d = a[:]

# a[0] = 11

#Slide 48 
# x=2          # Quando são do tipo integer são independentes 
# y=x          # Quando são do tipo integer são independentes 
# y +=1
# print(x)
# print(y)

# x = []
# y = x
# y.append(5)
# print(x)
# print(y)

#Extra
# a = round(4.2874298,2) # A função arrendo um float em x casas décimais
# print(a)

# a = "$$$$$##jo$$$$"
# b = list(a) #converte a string para lista
# c = "".join(b)

#Exercise3
# a = [1,2]
# b = [1,4,5]
# c = []
# for i in a:
#         for j in b:
#             if i!=j:
#                 c.append((i,j))
# print(c)

#NaN  not a number
# a = [2.6, float("NaN"), 4.8, 6.9 ,float("NaN")]
# b = []
# import math 
# for i in a :
#     if not math.isnan(i):
#         b.append(i)
# print(b)



    














