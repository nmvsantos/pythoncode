# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 14:45:35 2020

@author: Nuno Santos
"""

### Slide 15
# a = [5,7,12]
# print(a[0])
# print(a[1])
# print(a[2])
# print(type(a))
# print(len(a))

### Slide 18
# a = [5,7,12]
# print(a.index[7]) #Dá em que poseção está o valor 7

### Slide 21
# friends = ["Hamed", "Josi", "Stefan"]
# for f in friends:
#     print(f)
# #or
# for i in range(len(friends)):
#     print (friends[i])

### Slide 22 Exercise
# mylist = [1,2,23,4,"word"]
# a = len(mylist)
# for  i in range(a) :
#     if i < len(mylist) - 1:
#         print(mylist[i] , mylist[i+1])

### Slide 27 - 30
# a = [7,5,30,2,6,25]
# print(a[1:4])
# print(a[:4])
# print(a[4:])
# print(a[3:0])
# print(a[3:0:-1])
# print(a[::-1]) # reverte toda a lista
# print(a[0:7:2])# imprime de 2 em 2 casas
# a[3:5] = [14,15] # troca os valores
# print(a)

### Slide 31
# a = [4,7]
# b = a*2
# print(b)

# a = [1,2]
# b = ["a", "b" , "c"]
# c = a+b #Junta as duas lista
# print(c)

### Slide 33 Lista dentro de uma lista
# a = [3, [109,27], 4 ,15]
# print(a[0])
# print(a[1])
# print(a[1][1]) # imprime o segundo elementodo elemento na posição 1

### Slide 35 Exercise
# a = [7,5,30,2,6,25]
# b = max(a)
# c = a.index(b)
# print("The index is" ,c)

# s = 0
# for i in a:
#     s +=i
# print(s)

### Slide 38
# a = [1,3,6,5,3]
# print(a.count(3)) #Apresenta quantas vezes aparece o numero 3 na lista
# a.insert(2,13) # o 2 é a posição onde queremos colocar, o 13 o numero a colocar
# print(a)

### Slide 39 e 40
# a = [1,2,6,5,2]
# a.remove(2)
# print(a) # nota mt importante só remove 1 digito e não todos

# x =[10, 15, 12 ,8]
# a = x.pop(1) # A função pop retirar o valor da lista e guarda numa variavel
# print(x)
# print(a)

# a = [10, 15, 12 ,8]
# del a[1] # Com o delete não conseguimos guardar o numero
# print(a)

# a = [10, 15, 12 ,8]
# del a[1:3] # Elimina mais do que 1 numero da lista
# print(a)

### Slide 42
# a = [1,2,3]
# a.reverse() # reverte a orde
# print(a)

# a = [2,4,3,5,1]
# a.sort() # ordena os numero do menor para maior
# print(a)

### Slide 43 and 44
# x = [1,2,3]
# x.extend([5]) # acrescenta o digito 5 na lista
# print(x)

# x = [1,2,3]
# y = [4,5]
# x.extend(y) #Assim coloca uma lista dentro de outra
# print(x)

# x = [1,2,3]
# x.append(5) # o mesmo que extend
# print(x)

# x = [1,2,3]
# y = [4,5]
# x.append(y) # Coloco a lista y dentro da lista x mas como lista
# print(x)

### Slide 46
# a = [1,2,3]
# a.clear() # Limpa os valores da lista
# print(a)

# a = [1,2,3]
# c=a            # Aqui são dependente, se mudar um muda no outro
# b = a.copy()   # Aqui sao indepen, se mudar um não muda no outro
# print(b)
# print(c)
# a[1] = 20 # para mostra o que está escrito em cima
# print(b)
# print(c)

### Slide 49-51 M-operations
# a = [i for i in range(4)]
# print(a)

# a = [i*2 for i in range(4)]
# print(a)

# a = [1,-2,5,-56,8]
# b = [abs(i) for i in a]
# print(b)

# a = ["$ali", "sara$"]
# b = [i.strip("$") for i in a] # .strip remove o caracter que estiver na lista
# print (b)

# a = [11,8,14,20,2]
# b = [i for i in a if i <10]
# print(b)

### Slide 53 Exercise 3
# a = [1,2]
# b = [1,4,5]
# c = []
# for i in a:
#     for j in b:
#         if i !=j:
#             c.append((i,j))
# print(c)

### Slide 55 NaN exercise  Formula para tirar os not a number
# a = [2.6, float("NaN"), 4.8, 6.9, float("NaN")]
# b=[]
# import math
# for i in a:
#     if not math.isnan(i):
#         b.append(i)
# print(b)









