# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 15:07:52 2020

@author: Nuno Santos
"""
#Homework
m = [[1,2,3],[4,5,6,],[7,8,9]]
# m1 = m[0]
# m2 = m[1]
# m3 = m[2]


# #Quest 1

# print(m[0])


# #Quest 2

# print([m[0][0],m[1][0],m[2][0]]) 

# #or
# c = []
# for i in range(len(m)):
#     c.append(m[i][0]) 
# print(c)


# #Quest 3 
 
# print([m[0][0],m[1][1],m[2][2]]) 
# # or
# c = []
# for i in range(len(m)):
#     c.append(m[i][i]) 
# print(c)


# #Quest 4 
 
# print([m[0][2],m[1][1],m[2][0]])   
# #***or
# c = []
# for i in range(len(m)):
#         j = len(m)-1-i
#         c.append(m[i][j]) 
# print(c)
# #***or
# j=len(m)-1
# for i in range(len(m)):
#     print(m[i][j-i], end = " ")

# #Quest 5

# for i in range(len(m)):
#     if i < len(m):
#         c = sum(m[i])
#         print(c , end=" ",)
# print()

# #Quest 6

# a = len(m)
# for i in range(len(m)):
#     if i < len(m):
#         c=[]
#         while len(c) <= i+len(c) and len(c) != a:
#             if len(c) == 0:
#                 k = len(c)
#                 j = i
#                 c.append(m[k][j]) 
#             else:
#                 z = len(c)
#                 j = i
#                 c.append(m[z][j]) 
#             continue
#         print(sum(c), end=" ")





##Extra
##Transpose
# for i in range(len(m)):
#     j = len(m)
#     print(m[j-3][i], m[j-2][i], m[j-1][i])


# c = []  #Função para dividir várias listas numa só
# for i in range(len(m)):
#     if i < len(m):
#         c.append(m[i][len(m)-3]) 
#         c.append(m[i][len(m)-2])
#         c.append(m[i][len(m)-1])
# print(c)

