# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 16:50:11 2020

@author: Nuno Santos
"""

### Slide17
# t = ("English", "History", "Maths")
# print(t[0])
# print(t[1:3])

### Slide 18
# t = ("English", "History", "Maths")
# for i in t:
#     print(f"I like to read {i}")

### Slide 20
# t = (1,9,2)
# print(sum(t))
# print(max(t))
# print(min(t))
# print(t.count(9))

### Slide 21
# t = (1,9,2)
# print(t*2)
# print(t+t+t)
# print((3,6) + (9,))
# print((1,2) + (9,6))
# print(tuple(reversed(t)))

### Slide 25 Exercise 1
# t = (4,6)
# a = list(t)
# a.extend([9]) # or a.append(9)
# t = tuple(a)
# print(t)

### Slide 29 Exercise 2
# a = "$ $ Python$#Course  $"
# a = a.strip("$, "", #")
# print(a)
# t = list(a)
# z = t.copy()
# for i in t:
#     if i == "#" :
#         z.remove(i)
#     if i == "$":
#         z.remove(i)
# n = z.index("C")
# z.insert(n, " ")
# a = "".join(z)
# print(a)   


### Slide 33 Use remove 
# t = (4,7,2,9,8)
# x = list(t)    # First convert to list
# x.remove(2)     # use remove function
# t = tuple(x)    # convert to tuple again
# print(t)

###Slide 34 Unpack
# t = (4,6)
# a,b = t
# print(a)
# print(b)

### Slide 35-36 ZIP  # Junta os 2 primeirosdigitos de cada tuples numa lista mas
# a = (1,2)          # dentro da lista sao tuple
# b = (3,4)
# c = zip(a,b)
# x = list(c)
# print(x)
# print(type(x))

# z = ((1,3),(2,4)) # Na mesma tuple queremos queremos juntar os 2 primeiros digitos
# u = zip(*z)
# print(list(u))

# a = (1,2, "A")          
# b = (3,4, 8)
# c = zip(a,b)
# x = list (c)
# print(x)   #junta os digitos conforme a ordem que estão no tuple
# print(list(zip(*x)))  # junta os 2 tuples (a e b) numa unica lista 

# a = [11,22,33]
# b = zip(a, range(2))
# print(list(b))

###Slide 37 Exerci 3
# a = [1,2,"A"]
# b = ("Python" , 161.8,0,5)
# c = {10,12,14,16,18,20} #Como é do tipo set não é ordered precisamos de converter
# #print(list(zip(a,b,c)))

# c = list(c)

# i = 0  # Isto para ajudar a resolver o exercico acima
# z1 = [(a[0], b[0], c[0]) ,(a[1], b[1], c[1]), (a[2], b[2], c[2])]
# z2 = [(a[i], b[i], c[i]) ,(a[i+1], b[i+1], c[i+1]), (a[i+2], b[i+2], c[i+2])]

# a = [1,2,"A"]
# b = ("Python" , 161.8,0,5)
# c = {10,12,14,16,18,20} #Como é do tipo set não é ordered
# c = list(c)
# m = min(len(a),len(b),len(c) ) # para saber o minimo entre a,b,c

# e = []
# for i in range(m):
#     f = (a[i], b[i], c[i])
#     e.append(f)
# print(e) 

###Slide 40
# num = [8,2,(9,3),4,(1,6,7),34]
# c = 0
# for i in num:    # Este for é para contar quantos não são tuples
#     if isinstance(i, tuple):
#         continue
#     c += 1   # Aqui está a contar os que não são tupple
# print (c)
# print(len(num) - c)  # A diferença entre os que o numero de elemento e o ñ tuple

### Slide 42
# num = [8,2,(9,3),4,(1,6,7),34]
# c = 0
# for i in num:
#     if type(i) == tuple:
#         c+=1
# print(c)

### Slide 44
# m = [(1,2,3), (4,5,6)]
# b = [i[:-1] + (9,) for i in m]
# print(b)

## My solution for 
# m = [(1,2,3), (4,5,6)]
# c = []
# for i in m:
#     i = list(i)
#     z = len(i) - 1
#     del i[z]
#     i.insert(z, 9)
#     d = tuple(i)
#     c.append(d)
    
# print(c)








