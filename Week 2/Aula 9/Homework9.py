# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:28:08 2020

@author: Nuno Santos
"""

#Homework Aula9

#Exercise 1
# m = [(1,2,3), (4,5,6)]
# l = len(m)
# #Resultado [(1,2,9), (4,5,9)]

# b = []
# for i in m:
#     b = i[:-1]
#     a = i[:-1] +(9,)
#     b.append(a)
# print(b)   

#My solution
# m = [(1,2,3), (4,5,6)]
# c = []
# for i in m:
#     i = list(i)
#     z = len(i) - 1
#     del i[z]
#     i.insert(z, 9)
#     d = tuple(i)
#     c.append(d)
    
# print(c)
    

#Exercise 2
# a = (1,3)
# a =  enumerate(a) # Colocar enumerate na variavel
# print(list(a))
# for i , j  in a:
#         print("i is :", i)
#         print("j is :" , j)

### Might be the solution
a = (1,3)      # Ir ao slide 19 da aula 10
c = [a]
b = dict(c)

d={}
for i, j in b.items():
     print("i is :", i)
     print("j is :" , j)


### Prof solution
my_obj = [[1,2],[3,4]]
for i, j in my_obj:
     print("i is :", i)
     print("j is :" , j)

#Exercise 3
# a = [(1,3),(2,4),("A",8)]
# b,c,d = a

# m = min(len(b),len(c),len(d))
# e = []
# for i in range(m):
#     f = (b[i]),(c[i]), (d[i])
#     e.append(f)
# print(e)   

