# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 08:31:38 2020

@author: Nuno Santos
"""
#Notas da aula 9

# t = ( 1,)
# t[0] = 2 # mostra que não podemos alterar os valores do tipo tuple

# a = (1,2,3)

# for i in a :  # Aqui o i assume o valor do a[0]
#     i = a[0]
#     i = a[1]
#    ...
##if i in a :  # Aqui o i vai comparar com o valor a[0]
##or
##if (i == a[0] or i == a[1]) :
    # i == a[0]
    # i == a[1]
  
    
#Exercise1 Slide 24
# t= (4,6)
# c = list(t)  
# c.append(9) # ou podes usar c.extend([9])
# t = tuple(c)

#Exercise2
"""
#Como resolver atraves de questões
# Qual é o meu input? A sting and unchangeable
# Como remover # $ e espaços?
# Existe algum metodo para o fazer? Sim através do strip() mas valores direi ou esque
# Converter em lista
...
"""
# t = "$ $ Python$#Course  $"
# t = t.strip("# $")  #remove todos os valores da esquerda ou da direita que contenham aqueles valores
# a = t.replace("#", " ")
# b = a.replace("$" , "")
# t = b

#Prof solution
# t = "$ $ Python$#Course  $"
# a = t.strip("# $")  #remove todos os valores da esquerda ou da direita que contenham aqueles valores
# a = list(a)
# b = a.copy()
# for i in a: # para remover o "#"e "$"
#     if i == "#" or i == "$": # podemos escrever i in ["#","$"]
#         b.remove(i)
# b.insert(b.index("C"), " ") # para colocar um espaço entre as duas palavras
#                             #Usamos o insert e obrig o index para saber onde coloc
# d = "".join(b)      # Para converter de lista em string
# print(d)

# só usando loop para remover
# t = "$ $ Python$#Course  $"
# a = list(t)
# b = a.copy()
# for i in a:
#     if i == "#" or i == "$" or i ==" ":
#         b.remove(i) 
# b.insert(b.index("C"), " ") # para colocar um espaço entre as duas palavras
#                             #Usamos o insert e obrig o index para saber onde coloc
# d = "".join(b)      # Para converter de lista em string
# print(d)

#Slide 35
# t = (4,8)
# a,b = t
# print(a)
# print(b)

# d = [[1,2], [3,4]]
# a,b = d
# print(a)
# print(b)

# #Slide 36
# a =(1,2)
# b = (3,4)
# c = zip(a,b)
# x = list(c)
# print(x)
# print(x[0])

#Exercise 3
# a = [1,2,"A"]
# b = ("Python" , 161.8,0,5)
# c = {10,12,14,16,18,20} #Como é do tipo set não é ordered
# print(list(zip(a,b,c)))
#c = list(c)


# i = 0  # Isto para ajudar a resolver o exercico acima
# z1 = [(a[0], b[0], c[0]) ,(a[1], b[1], c[1]), (a[2], b[2], c[2]) ]
# z1 = [(a[i], b[i], c[i]) ,(a[i+1], b[i+1], c[i+1]), (a[i+2], b[i+2], c[i+2]) ]

# a = [1,2,"A"]
# b = ("Python" , 161.8,0,5)
# c = {10,12,14,16,18,20} #Como é do tipo set não é ordered
# c = list(c)
# m = min(len(a),len(b),len(c) ) # para saber o minimo entre a,b,c

# e = []
# for i in range(m):
#     f = (a[i], b[i], c[i])
#     e.append(f)
# print(e) 

# #Exercice 4
# num = [8,2,(9,3) ,4,(1,6,7) ,34]
# c = 0
# for i in num:
#     if type(i) == tuple:
#         c +=1
# print("There are" , c, "tuples in num")





    
    
    
    
    
    
    
    
    