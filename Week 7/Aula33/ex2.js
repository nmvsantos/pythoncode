var value = 0

var valueDisplay = document.getElementById("value-display");
var addBt = document.getElementById("addBtn");
var subtractBt = document.getElementById("subtractBtn");

addBt.addEventListener("click", function() {
    value ++;
    valueDisplay.innerHTML = value.toString();
});

subtractBt.addEventListener("click", function() {
    value --;
    valueDisplay.innerHTML = value.toString();
});