//Revisão das aulas anteriores
var i = 0;
do {
   console.log(i); 
} while (i<0) {
    console.log(i);
}

for( var i=0; i<5 ; i++){ // i++ é o mesmo que i+1
console.log(i);
}

var a = ["abc", "cde"];

for (i in a) {
    console.log(i);
}

var object = new Object();
var object = {
    abc:"alpha";
}
object.cenas = "cenas";

function myobject() {
    this.abc = "alpha"; //this adiciona os dados à classe/objecto
}

for (var attribute in object) {
    console.log(object[attribute]);
}

var object = new myobject();

//Array
class MyArray {
    var 1 =Nuno;
    var 2 =Santos;
    var 3 =Ansião;
}

var array1 = ["3"];
array1.push("7"); // é como se fosse o append do python
// NOTA usar o playcode.io para ver o programa a correr

