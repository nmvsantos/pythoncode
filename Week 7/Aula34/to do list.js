//NOTA só abre no Chrome o ficheiro
var idCount;

//Elements
var todoInput = document.querySelector(".todo-input"); //é preciso por ponto no todo//
var todoButton = document.querySelector(".todo-btn");
var todoList = document.querySelector(".todo-list");
var filterDropdown = document.querySelector(".filter-todo");

//event listeners
todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteCheck);
filterDropdown.addEventListener("change", filterTodo);

//functions
function addTodo(event) {
  event.preventDefault(); // isto é para evitar que a pagina recarregue a cada click

  const todoValue = todoInput.value;
  let todoCompleteValue = false;

  let todoJson = {
    value: todoValue,
    complete: todoCompleteValue,
    id: `todo-${idCount}`,
  };
  save(todoJson);
  idCount++;

  //criar o Div
  const div = document.createElement("div");
  div.classList.add("todo");
  div.id = todoJson.id;
  //var divClass = document.createAttribute("class")
  //divClass.value = "todo"
  //div.setAttribute(divClass)

  //criar li
  const todoItem = document.createElement("li");
  todoItem.classList.add("todo-item");
  todoItem.innerHTML = todoInput.value;
  div.appendChild(todoItem);

  //Criar botão de check
  const completeBtn = document.createElement("button");
  completeBtn.classList.add("complete-btn");
  completeBtn.innerHTML = '<i class="fas fa-check"></i>';
  div.appendChild(completeBtn);

  //Criar botão de trash
  const trashBtn = document.createElement("button");
  trashBtn.classList.add("trash-btn");
  trashBtn.innerHTML = '<i class="fas fa-trash"></i>';
  div.appendChild(trashBtn);

  //Adicionar à lista de todo
  todoList.appendChild(div);

  todoInput.value =
    ""; /*isto é para apagar o que escrevemos quando faz refresh*/
}

function deleteCheck(event) {
  //Obter o botão da lista que foi pressionado
  var pressedButton = event.target;

  if (pressedButton.classList[0] == "trash-btn") {
    const parent = pressedButton.parentElement;
    parent.classList.add("fall");
    parent.addEventListener("transitionend", function () {
      parent.remove();
      removeItemWithId(parent.id);
    });
  }
  if (pressedButton.classList[0] == "complete-btn") {
    const parent = pressedButton.parentElement;
    parent.classList.toggle("complete");
    updateTodoWithId(parent.id);
  }
}

function filterTodo(event) {
  const todos = todoList.querySelectorAll(".todo");
  todos.forEach(function (todo) {
    /*for each faz o mesmo que for of*/ switch (event.target.value) {
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("complete")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
      case "uncompleted":
        if (!todo.classList.contains("complete")) {
          /* ! tem o significado de diferente*/
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
    }
  });
}

function updateTodoWithId(id) {
  //1. obter o elemento do localstorage com o id
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  const item = todoItems.filter(function (todoItem) {
    return todoItem.id == id;
  })[0];
  console.log(item);
  //2. Alterar o valor do elemento encontrado
  item.complete = !item.complete;
  //3. Guardar alterações
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function removeItemWithId(id) {
  //1. Buscar a lista de elementos
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  //2. remover elementos com id
  todoItems = todoItems.filter(function (todoItem) {
    return todoItem.id != id;
  });
  //3. guardar alterações
  localStorage - setItem("todo", JSON.stringify(todoItems));
}

function loadItems() {
  if (localStorage.getItem("todo") != null) {
    let todoItems = JSON.parse(localStorage.getItem("todo"));
    todoItems.map(function (todoItem) {
      buildItemWithViewHolder(todoItem);
    });
  }
}

function buildItemWithViewHolder(viewHolder) {
  const div = document.createElement("div");
  div.classList.add("todo");
  div.id = viewHolder;
  if (viewHolder.complete) div.classList.add("complete");
  //var divClass = document.createAttribute("class")
  //divClass.value = "todo"
  //div.setAttribute(divClass)

  //criar li
  const todoItem = document.createElement("li");
  todoItem.classList.add("todo-item");
  todoItem.innerHTML = viewHolder.value;
  div.appendChild(todoItem);

  //Criar botão de check
  const completeBtn = document.createElement("button");
  completeBtn.classList.add("complete-btn");
  completeBtn.innerHTML = '<i class="fas fa-check"></i>';
  div.appendChild(completeBtn);

  //Criar botão de trash
  const trashBtn = document.createElement("button");
  trashBtn.classList.add("trash-btn");
  trashBtn.innerHTML = '<i class="fas fa-trash"></i>';
  div.appendChild(trashBtn);

  //Adicionar à lista de todo
  todoList.appendChild(div);
}

function save(todo) {
  let todoItems;
  if (localStorage.getItem("todo") == null) {
    todoItems = [];
  } else {
    todoItems = JSON.parse(localStorage.getItem("todo"));
  }

  todoItems.push(todo); /*push é o append do python*/
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function loadItems() {
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  if (todoItems != null) {
    todoItems.map(buildItem);
  }
}

function buildItem(valueHolder) {
  const div = document.createElement("div");
  div.classList.add("todo");
  if (valueHolder.complete) div.classList.add("complete");
  div.id;
}

window.onpageshow = function () {
  if (localStorage.getItem("idCount") == null) {
    idCount = 0;
  } else {
    idCount = JSON.parse(localStorage.getItem("idCount"));
  }
  loadItems();
};

window.onpagehide = function () {
  localStorage.setItem("idCount", JSON.stringify(idCount));
};
