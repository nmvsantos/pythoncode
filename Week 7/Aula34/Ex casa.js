if (new Date().getHours() < 18) {
  document.getElementById("demo").innerHTML = "Good day!";
} else {
  document.getElementById("demo").innerHTML = "Good evening!";
}

var cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];
var text = "";
var i;
for (i = 0; i < cars.length; i++) {
  text += cars[i] + "<br>";
}
document.getElementById("demo1").innerHTML = text;

var cars = ["BMW", "Volvo", "Mini"];
var x;
for (x of cars) {
  document.write(x + "<br >");
}

var text = "";
var i = 0;

do {
  text += "<br>The number is " + i;
  i++;
} while (i < 10);

document.getElementById("demo3").innerHTML = text;
