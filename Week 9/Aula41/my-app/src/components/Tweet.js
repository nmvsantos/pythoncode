import { useState} from "react"
import s from '../styles/Tweet.module.css'

const Tweet = ({tweet, tweets, setTweets}) => {

const onRemoveHandler = (event) =>{
    const newTweets = tweets.filter((tweetItem) =>{
        return tweetItem.id !== tweet.id;
    });
    setTweets(newTweets);
}

const [like,setLike] = useState(false);

const onLikeHandler = (event) =>{
    //Buscar o indice do tweet
    setLike(!like);
};

    return (
        <div className={s.tweet} id={tweet.id}>
            <h3>{tweet.text}</h3>
            <p>
                <i className={`fas fa-heart ${like ? '':'hidden'}`} ></i>
            </p>
            <button onClick = {onRemoveHandler}>Remove</button>
            <button onClick={onLikeHandler}>Like</button>
        </div>
        
    );
}
export default Tweet;