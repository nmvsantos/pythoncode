import Tweet from "./Tweet"

const TweetList = ({tweets, setTweets}) => {
return(
    <div className="tweet-list">
        {
            tweets.map((tweet) =>(<Tweet tweet={tweet} tweets={tweets} setTweets = {setTweets}/>)
            )
        }
    </div>
)

  //  const styles = {color:props.color}
  //  return(<h1 style={styles}>Tweets</h1>)
}

export default TweetList; /*nome da função*/