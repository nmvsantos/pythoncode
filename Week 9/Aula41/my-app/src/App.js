import { useState, useRef, useEffect} from "react" // 1º importar o useState
import TweetList from "./components/TweetList"
import { uuid } from 'uuidv4';


function App() {

const inputRef = useRef(null); //guarda os valores introduzidos pelo user

const[abc,setAbc] = useState("Submit");

const [tweets,setTweets] = useState([]); // 2º Definir o estado com [] que contem a variavel e a função

//useEffect(() =>{
  //console.log("Useefct")
 // inputRef.current.value="";
//},[tweets]);

  const submitClickHandler = (event) => {
    event.preventDefault();
    setAbc("Submited");
    console.log(inputRef.current.value);
    const tweetObject = {
      id: uuid(),
      text: inputRef.current.value,
      liked: false
    }
    inputRef.current.value="";
    const newTweets = [...tweets, tweetObject];
    setTweets(newTweets);
  }

const appStyles = {
  color: "white",
  backgroundColor: "aqua"
}


  return (<div className="App" style={appStyles}>
    <form >
      <input type="text" placeholder="Write your tweet..." ref= {inputRef}/>
      <button onClick= {submitClickHandler} type="submit">{abc}</button>
    </form>
    <TweetList color="blue" tweets={tweets} setTweets = {setTweets}/>
  </div>
  );
}

export default App;
