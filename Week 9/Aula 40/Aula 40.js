var myVanillaVariable = 3;

for (let i = 0; i < myVanillaVariable; i++) {
  i++;
}

console.log(myVanillaVariable);
console.log(i);

const myConst = 3;

/*
function myFun(argument) {
  //argumento
}

//é igual

const myFun = (argument) => {
  //argumento1
  //argumento2
};
*/
