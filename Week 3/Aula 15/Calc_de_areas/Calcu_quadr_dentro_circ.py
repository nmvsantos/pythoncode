# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 10:35:47 2020

@author: Nuno Santos
"""

import sys
sys.path.append("C:\\Users\\Nuno Santos\\Desktop\\pythoncode\\Aula 15\\areas")


import areaquad
from math import sqrt

def maior_quad(valor):  
    areaquad.opera_quadrado(valor)

a = int(input("Escreva um valor")) # Para obter o raio
c = sqrt(2)
b = a*c # Para calcular o valor do lado correspondente ao raio (r*raiz quad de 2)
maior_quad(b)


