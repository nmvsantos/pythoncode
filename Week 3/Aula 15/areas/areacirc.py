# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 10:31:16 2020

@author: Nuno Santos
"""

from math import pi

def opera_circulos(raio):
    print("perimetro do circulo", 2*pi*raio)
    print("Area do circulo",pi*raio**2)
