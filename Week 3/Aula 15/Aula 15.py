# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 08:40:37 2020

@author: Nuno Santos
"""

### Aula 15
##Exer 4 da ficha 4

# from statistics import mean

# def medias(ficheiro):
#     try:
#         with open (ficheiro) as file_reader:
#             for line in file_reader:
#                 line = line.strip()
#                 split_line = line.split()
#                 for i in range(len(split_line)):
#                     split_line[i] = float(split_line[i])
#                 print(round(mean(split_line)))
#     except:
#         print("Someting wrong")                      
            
# medias("Exercicio_4_ficha_4.txt")

# def ficheiro_falha():
#     user_input = input("indique ficheiro")
#     try:
#         medias(user_input)
#     except:
#         print("erro")


## Ler ficheiros de CSV
#import csv

# with open("eggs.csv", newline = "") as csvfile:
#         spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
#         for row in spamreader:
#             print(', '.join(row))
            
## ou

# with open("eggs.csv", newline = "") as csvfile:
#         spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
#         print(spamreader.__next__())
        
### Exercicio 1 do CSV

# import csv
# def le_grafico(ficheiro_csv):
#     grafico = []
#     with open(ficheiro_csv) as csvfile:
#         csv_reader = csv.reader(csvfile, delimiter = ";")
#         linha1 = csv_reader.__next__()  # dá os valores de uma lina do csv
#         print(linha1)
#         linha2 = csv_reader.__next__()
#         print(linha2)
#         for i in range(len(linha1)):
#             item_linha1_com_ponto = linha1[i].replace(",",".")
#             item_linha2_com_ponto = linha2[i].replace(",",".")
#             grafico.append((float(item_linha1_com_ponto),float(item_linha2_com_ponto)))
#         return grafico
        
# print(le_grafico("graficos.csv"))

###############************Classes e objectos**************################

## Para definir uma classe
## Classe pode ter variaveis ou funções
class House:
    area = 120 # são consideradas as variaveis da class
    window_size = (2,3)
    
    def func(self): # Para a definir função tem que ter sempre o "self"
        pass # pass indica que há codigo em falta e salta

    def __init__(self, cor_das_paredes, cor_do_telhado): 
    #def __init__(self, cor_das_paredes=0xffffff, cor_do_telhado=0x0000ff) #atribui automaticamente a cor
        self.cor_das_paredes = cor_das_paredes
        self.cor_do_telhado = cor_do_telhado
       ## dá para customizar o inicializador
       ## O __init__ Assim vai buscar os atributos do House duplicando-os
       ## acrescentando os novos definidos no __init__, personalizando o
       ## o layout do objecto ou os atributos do objecto dos novos
       

#print(House.area) # Se quiser ler uma variavel de uma classe
#print(House.func()) # Se quiser aceder à função da classe
                    # vai dar erro porque tem que ter o self preenchido
"""
Variaveis e funções dentro de uma classe são chamados de atibutos
"""    
### como criar objectos
##int() #cria um objecto da classe inteiro
##float() #cria um objecto da classe float

## Para criar um objecto do tipo Myclass
#House() ## Cria um objecto dentro da classe House, ou seja, cria um objecto
        ## já com os dados "area = 120" + "window_size" = (2,3)

concrete_house = House(0xffffff,0x0000ff) #0x indica que o nº é hexadecimal
                            #0xffffff é cor branca
                            #0x0000ff é cor azul
concrete_house.area = 450 # Aqui o concrete house assume area de 450 e window
                        # (2,3)
# print(concrete_house.area) 
# print(concrete_house.window_size)                    
# print(concrete_house.cor_das_paredes)
# print(concrete_house.cor_do_telhado)
"""
O concrete_house ficou com o valor de 450, cor_das_paredes 0xffffff e
cor_do_telhado 0x0000ff
"""

another_house = House(0xffffff,0x0000ff) #como o __init__ não está pré definido
                                        #tem que ter semprevalor
another_house.cor_das_paredes
another_house.cor_do_telhado
another_house.portas = 2
print(another_house.portas)
print("Quantas portas " + str(another_house.portas))
print("Qual area " + str(another_house.area))
#Com a função del(another_house.area) elimina o objecto e implica nos outros

### Para uma classe herdar de uma outra
class Pais:
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        
    def trabalhar(self):
        print("vamos trabalhar")
        
class eu(Pais): #eu(Pais) é para herdar valores de Pais
    def __init__(self, nome, idade, hobby):
        super().__init__(nome, idade) # o super está a referencia a classe Pais
        self.hobby = hobby
    
    def trabalhar(self):
        super().trabalhar()
        print("trabalhar mais")
        
me = eu("Nuno", 24, "jogar à bola")
# print("O meu nome é " + me.nome)
# print("A minha idade é " + str(me.idade)) # colcoar str porque é int
# print("O meu hobby é " + str(me.hobby))
me.trabalhar() # Isto imprime a parte da def. trabalhar

class estrutura:
    pass

estru = estrutura()
estru.x = 2















