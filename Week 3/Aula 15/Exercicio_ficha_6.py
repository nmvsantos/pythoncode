# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 15:44:47 2020

@author: Nuno Santos
"""

### Exerci 1a e 1b
# from math import pi

# def opera_circulos(raio):
#     print("perimetro do circulo", 2*pi*raio)
#     print("Area do circulo",pi*raio**2)
#     # return peri_cir
#     # return area_cir
    
# def opera_quadrado(lado):
#     print("perimetro do quadrado", lado + lado + lado + lado)
#     print("Area do quadrado" , lado * lado)
    
# opera_circulos(5)
# opera_quadrado(5)

### Exerci 1c

# from math import pi

# def opera_circulos(raio):
#     print("perimetro do circulo", 2*pi*raio)
#     print("Area do circulo",pi*raio**2)
#     # return peri_cir
#     # return area_cir
    
# def opera_quadrado(lado):
#     print("perimetro do quadrado", lado + lado + lado + lado)
#     print("Area do quadrado" , lado * lado)
    
# def areas(valor):
#     opera_circulos(valor)
#     opera_quadrado(valor)

# areas(int(input("Escreva um valor")))    


### Exerci 1d

from math import pi
from math import sqrt

def opera_circulos(raio):
    print("perimetro do circulo", 2*pi*raio)
    print("Area do circulo",pi*raio**2)
    # return peri_cir
    # return area_cir
    
def opera_quadrado(lado):
    print("perimetro do quadrado", lado + lado + lado + lado)
    print("Area do quadrado" , lado * lado)
    
def maior_quad(valor):  
    opera_quadrado(valor)
    
a = int(input("Escreva um valor")) # Para obter o raio
b = sqrt(a) # Para calcular o valor do lado correspondente ao raio
maior_quad(b)

