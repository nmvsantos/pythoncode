# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 11:58:51 2020

@author: Nuno Santos
"""
###Exercico 1 a
# class pessoa:
#     def __init__(self, nome, idade, peso, altura):
#         self.nome = nome
#         self.idade = idade
#         self.peso = peso
#         self.altura = altura

# p1 = pessoa("Nuno", 32 ,80 , 1.75)   
# print("O meu nome " + p1.nome) 

###Exerci 1 b
# class ppl:
#     def __init__(self, nome, idade, peso, altura):
#         self.nome = nome
#         self.idade = idade
#         self.peso = peso
#         self.altura = altura
        
#     def envelhecer(self):       
#         pass
    
#     def engorda(self):
#         pass
    
#     def emagrecer(self):
#         pass        
    
#     def crescer(self, altura):
#         if self.idade < 21:
#             print(altura + 0.5)
            
# p2 = ppl("Ana",20,77, 1.8)
# p2.crescer(1.8)
# #print(p2.altura)


### Exercicio 2
from datetime import datetime

class macaco:
    
    def __init__ (self, nome):
        self.nome = nome
        self.bucho = []
        
    def comer(self, alimento):
        self.alimento = alimento
        print("O que o", self.nome,"está a comer é ", self.alimento)
        
    def verbucho (self , bucho):        
        self.bucho.append(bucho)
        print("O", self.nome, "tem no seu bucho," , self.bucho)
    def digerir(self):
        h = datetime.now()
        print("O", self.nome, "acaba a sua digestão acaba às", h.hour + 1 , h.minute)
        print()
        
m1 = macaco("Zé")
m1.comer("Banana")
m1.verbucho("Banana")
m1.comer("Pêra")
m1.verbucho("Pêra")
m1.comer("Abacate")
m1.verbucho("Abacate")

m1.digerir()

m2 = macaco("Tó")
m2.comer("Maça")
m2.verbucho("Maça")
m2.comer("Morango")
m2.verbucho("Morango")
m2.comer("Laranja")
m2.verbucho("Laranja")

m2.digerir()















