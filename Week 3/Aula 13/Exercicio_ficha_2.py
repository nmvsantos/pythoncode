# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 18:01:59 2020

@author: Nuno Santos
"""

### Exercicio1
# def imprimeDivisaoInteira(x, y):
#     if y == 0:
#         print("Divisao por zero")
#     else:
#         print(x//y)
        

# ## a.
# ## Se o valor y for 0 vai imprimir a mensagem "Divisao por zero”
# ## se y diferente de 0 faz a divisão inteira entre os dois elementos

# ## b.

# imprimeDivisaoInteira(4, 2) # 2 # Não precisa do print aqui por está na função
# imprimeDivisaoInteira(2, 4)  # 0
# imprimeDivisaoInteira(3, 0)  #Divisão por zero
# help(imprimeDivisaoInteira)
# imprimeDivisaoInteira() # Dá erro porque falta os valores


### Exercicio 2
# def potencia(a, b):
#     return a**b

## a.
# a = 2  # Atribui o valor 2 a "a"
# b = 3  # Atribui o valor 3 a "b"
# potencia(b, a) # Erro  porque não está definido o valor para as variaveis
# potencia(a, b)  # Erro  porque não está definido o valor para as variaveis
# print(potencia(b, a)) # Erro  porque não está definido o valor para as variaveis
# print(potencia(a, b)) # Erro  porque não está definido o valor para as variaveis
# print(potencia(2,0))  # Executa a função e gera o valor 1
# print(potencia(2)) # Falta um valor para o argumento b para a função funcionar


## b.
# def potenciaP(a):
#     return a**a
# print(potenciaP(4))

## c.
# def potencia(a, b):
#     return a**b

# def potenciaP(a):
#     return a**a

# def potencia2(a, b=None) : ## Para ter a segunda elemento como opctional tem \"b=None"
#     if b != None:       
#         return potencia(a,b)
#     else  :
#         return potenciaP(a)

# print(potencia2(2))
# print(potencia2(2,1))

### Exercicio 3
# a = 4
# def printFuncao():
#     a = 17
#     print("Dentro da funcao: ", a)
# printFuncao()
# print("Fora da funcao: ", a)

## a.
## imprime Dentro da funcao:  17       Fora da funcao:  4

## b.
## Variavel global é aquela que está acessivel a todas as funções
## Variavel local é aquela que só está acessivel dentro da função onde foi
## definida

### Exercicio 4
# def somaDivisores(num):
    
#    """
# Soma de divisores de um numero dado
# Requires:
# num seja int e num > 0
# Ensures: um int correspondente à soma dos divisores
# de num que sejam maiores que 1 e menores que num

#    """

### a.
## O tuilizador deve inserir um unico número maior que zero e inteiro e a função
## retornar um inteiro relativo à soma dos divisores


### b.
# def somaDivisores(num):
#     for i in range(1, num//2+1):
#         if num % i == 0: 
#             yield i
#     yield num

# num = int(input("Escreve um nº inteiro e maior que 0 "))
# print(list(somaDivisores(num)))

#### NOTA Return sends a specified value back to its caller whereas 
#### NOTA Yield can produce a sequence of values.

## Resp. Retorna os divisores do numero inserido pelo utilizador


### Exercicio 5
# def somaDivisores(num):
#         for i in range(1, num//2+1):
#             if num % i == 0: 
#                 yield i
#         yield num


# num = int(input("Escreve um nº inteiro e maior que 0 "))
# while num >0:
#     print(list(somaDivisores(num)))
#     num = int(input("Escreve um nº inteiro e maior que 0 "))

### Exercicio 6        

# from datetime import date
# dia_atual = date.today().day
# mes_atual = date.today().month
# ano_atual = date.today().year

# def pede_informação():
#     ano = int(input("Introduza o ano de nascimento: "))
#     mes = int(input("Introduza o mes de nascimento: "))
#     dia = int(input("Introduza o dia de nascimento: "))
#     nome = input("Introduza o Pai/Mae:")
#     return ano, mes, dia, nome


# def calcul_idade ():
#     if mes > mes_atual or \
#     (mes == mes_atual and dia > dia_atual):
#         print(nome, "tem", ano_atual - ano - 1, "ano(s)")
#     else:
#         print(nome, "tem", ano_atual - ano, "ano(s)")

# ano, mes, dia, nome = pede_informação()
# calcul_idade()


# ## Exercicio 7
# def maximo(a,b):
#     """
#     Devolve o maior valor entre dois valores

#     Parameters
#     ----------
#     a : Int
#         Primeiro valor
#     b : Int
#         Segundo valor

#     Returns
#     -------
#     O maior valor entre os dois valores dados

#     """
#     c = max(a,b)
#     return c

# print(maximo(2,3))

### Exercicio 7.2
# def maximo(a,b):
#     c = max(a,b)
#     return c
# def minimo(a,b):
#    return b if a == maximo(a,b) else  a

# print(minimo(2,3))
    
### Exercicio 8
# def elimina_unid(a):
#     """
#     Elimina i valor das unidades se o numero tiver mais do que 2 digitos

#     Parameters
#     ----------
#     a : Int
#         Insira um numero inteiro

#     Returns
#     -------
#     Retorna 0 se o valor inserido tiver um unico digitoou retorna os restantes
#     numeros excepto o das unidades

#     """
   
#     a = str(a)
#     b = len(a)
#     if  b == 1:
#        return 0
#     else:       
#        b = a[0:-1]
#        return b
   
# print(elimina_unid(1))

### Exercicio 9

# def add_zero(a):
#     """
#     Função adiciona o digito 0

#     Parameters
#     ----------
#     a : Int
#         Insira um valor inteiro

#     Returns
#     -------
#     b : Inteiro
#         Devolve o valor inserido mais o digito 0 no fim

#     """
    
#     a = str(a)
#     b = a + str(0)
#     return b
# print(add_zero(8))

### Exercicio 10
# def somaDivisores(num):
#     for i in range(1, num//2+1):
#         if num % i == 0: 
#             yield i
#     yield num

# num = int(input("Escreve um nº inteiro e maior que 0 "))
# print(list(somaDivisores(num)))

### Exercicio 11
# def primo(a):
#     if a%2 ==1:
#         print("Numero primo")
#     elif a ==2:
#         print("Numero primo")
#     else:
#         print("Numero não primo")

# primo(6)

### Exercicio 12                                   #Não está bem
# def primo(a):
#     c=1  #1 porque não está a assumir o 2 como primo
#     if a%2 ==1:
#         print("Numero primo")  
#         print("2") # para imprimir o numero dois como primo
#         for i in range(2,a+1):
#             if i%2 ==1:
#                 print(i) # imprime os numeros que são primos
#                 c = c+1
#         print("Existem",c,"numeros primos")
#     elif a ==2:
#         print("Numero primo")
#     else:
#         print("Numero não primo")

# primo(23)



















