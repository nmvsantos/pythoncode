# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 17:16:51 2020

@author: Nuno Santos
"""

### Exercicio 1
## a.
# a = list(map(lambda x:x+1, range(1,4)))
# print(a)
### No range (1,4) o x vai assumir 1,2 e 3 e depois aplica o x = x+1

## b.
# b = list(map(lambda x:x>0, [3,-5,-2,0]))
# print(b)
### Neste caso o map + lambda está a analisar se a condição x>0 é True or False

## c.
# c = list(filter(lambda x:x>5, range(1,7)))
# ###e = list(map(lambda x:x>5, range(1,7))) #extra para avaliar
# print(c)
# ### Aqui dá o valor onde se verifica a condição

# ## d.
# d = list(filter(lambda x:x%2==0, range(1,11)))
# print(d)
### Aqui dá o valor onde se verifica a condição

"""
Quando temos map + lambda com um condição x>0 ou x=0 , etc retorna True or False
Se tiver um operação retorna o valor da operação
Se tivermos filter +lambda com uma condição x>0 ou x=0 , etc retorna o numero
onde se verifica a condição.

"""

### Exercicio 2
# from functools import reduce

# ## a.
# a = reduce(lambda y, z: y* 3+z, range(1,5))
# print(a)

"""
A função gera os seguintes resultados
y - z
1 - 2
5(1*3+2) - 3
18 (5*3+3) - 4
58 (18*3+4) z não há porque o intervalo acaba no 4

"""

## b.
# b = reduce (lambda x, y: x** 2+y, range(2,6))
# print(b)

"""
O que faz a função na alinea b
x - y
2 - 3 
7(2^2+3) - 4 
53(7^2+4) - 5 
2815(53^2+5) y não há porque o intervalo acaba no 5

Só altera o valor da variável x conforme a operação
o y só adquire valores do range
"""
