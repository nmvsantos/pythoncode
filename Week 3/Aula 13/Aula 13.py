# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 08:39:31 2020

@author: Nuno Santos
"""

### Aula 13 Funções

##Exemplo de uma função
# def multiplica_numero(numero, valor =2):
#     return numero*valor

# print(multiplica_numero(2))
# print(multiplica_numero(2,3))

#*****

# a = [["a",2], ["b",25], ["c", 0]]
# a.sort()
# print(a)


# a = [["a",2],["a",0], ["b",25], ["c", 0]]
# a.sort() #Aqui já ordena porque há 2"a" e vai buscar por ordem numerica
# print(a)

# def compare_items(item):
#     return len(item[0])

# a = [["a",2],["asdsa",0], ["abmopasfn",25], ["csdfnçsd", 0]]
# a.sort(key=compare_items)
# print(a)

#**** Exemplo no notion no ficheiro funções

# def print_num(start,end):
#     if start == end:
#         return
#     else:
#         print(start)
#         print_num(start+1,end)
        
# print_num(0,11) # Muita atenção tem que escrever a função com os valores

# a= "c"
# def triplica(string):
#     string = string*3
#     return string

# print(triplica(a))

# a= "c"  
# def triplica(string):
#     string = string*3
#             #Sem o return transofrma-se em procedimento retornado "None"

# print(triplica(a))

#****
# def range(start,end, step=1):
#     i = start
#     while i < end:
#         yield i  # O yield é para parar a execução e guardar o histórico
#         i += step
        
# a = range(0,10,2)
# for i in a:
#     print(i)
# print(a)


#*********** Aplicar MAP


# def aplica_função_a_elementos(lista, função):
#     for i in range(len(lista)):
#         lista[i] = função(lista[i])
#     return lista

# def add_two(x):
#     return x+2

# print(aplica_função_a_elementos([2,5,1,8], add_two))
# #""""" O Map quando temos 2 função 

# lista = [2,5,1,8]
# a = map(add_two , lista)
# print(a) # Para corrigir o erro temos que colocar list
# a = list(map(add_two , lista))
# print(a)

# def multiply(a,b):
#     return a*b

# a = list(map(multiply, (1,2,3),(2,3,4))) # o "a" é (1,2,3) o "b" (2,3,4)

# print(a)

#***************** Aplicar Filter
# a = [1,22,33,2]
# def lt_d(x):
#     return x<16

# b = list(filter(lt_d,a)) # Aqui só retorna valores em que a condição for True
#                           #E elimina os valores onde a condição der False
#                           #Condição é x<16
# print(b)


#****************Aplicar sorted
# a = ("abc", "ahss", "ais")

# b = list(sorted(a, key=len)) # O key = len tem que ser sempre com uma função
# print(b)

# a = ("abc", "ahss", "ais")
# b = list(sorted(a, key=len, reverse=True)) # Com reverso
# print(b)

#*** Para criar um sum para string
# a =["abc" , "def", "ghi"]
# def my_sum(lista):
#     accumulator = type(lista[0])() 
#     for i in lista:
#         accumulator += i
#     return accumulator

# sumed_a = my_sum(a)
# print(sumed_a)



# lista_lista = [[1,2],[3,4],[5,6]]
# sumed_lista_listas = my_sum(lista_lista)
# print(sumed_lista_listas)
# #### Ou através de uma função do Python o reduce
# def add(a,b):
#     return a+b

# from functools import reduce
# sumed_lista_listas = reduce(add, lista_lista)
# print(sumed_lista_listas)



# fruits = ["banana", "apple", "peach"]
# sumed_fruits = reduce(add, fruits, "I love ")
# print(sumed_fruits)
# sumed_fruits = reduce(lambda x,y: x+y, fruits) # Lambda indica que estamos
#                     #definir uma função sem nome e temporária
# print(sumed_fruits)


#### Extra como criar uma lista rapidamente
# a = [x+2 for x in range(2)]

# b = [sub_item for i in [[1,2],[3,4]] for sub_item in i]

# ## A linha acima corresponde à debaixo

# z=[[1,2],[3,4]]
# c = []
# for i in z:
#     for sub_item in i:
#         c.append(sub_item)
# print(c)    


######### Exercicios 1

def imprimeDivisãoInteira(x,y): ## Para puxar a parte verde """ + enter
    """
    Divide um inteiro por outro

    Parameters
    ----------
    x : int
        dividendo.
    y : int
        divisor.

    Returns
    -------
    Int: resultado da divisão de x por y

    """
    if y==0:
        print("Divisão por zero")
    else:
        print(x//2)

imprimeDivisãoInteira(4,2) # Não precisa do print aqui por está na função

# ### Exercicio 3
# # Quest1
# def potencia(a,b):
#     return a**b
# # print(potencia(2,3))
# # print(potencia(2,0))

# #Quest2
# def potenciaP(a):
#     return a**a
# #print(potenciaP(4))

# #Quest3
# def potencia2(a, b=None) : ## Para ter a segunda elemento como opctional tem \"b=None"
#     if b != None:       
#         return potencia(a,b)
#     else  :
#         return potenciaP(a)

# print(potencia2(2,0))


###***Exercicio 6
# a = 4
# def printFunção():
#     a = 17
#     print("dentro da função:", a)
    
# printFunção()
# print("dentro da função:", a)

# a = 4
# def printFunção():
#     global a # Com o global o a assume o valor 17
#     a = 17
#     print("dentro da função:", a)
    
# printFunção()
# print("dentro da função:", a)



# def fu1():
#     a = 3
#     print("non-local", a)
#     def fu2():
#         a = 4
#         print("non-local", a)
# fu2(2)

###**** Exercicio 10
# ano_atual = 2005
# mes_atual = 12
# dia_atual = 25

# anomae = int(input("ano nasc"))
# mesmae = int(input("mes nasc"))
# diamae = int(input("dia nasc"))

# anopai = int(input("ano nasc"))
# mespai = int(input("mes nasc"))
# diapai = int(input("dia nasc"))

# if mespai > mes_atual or \
#     (mesmae== mes_atual and diapai>dia_atual):
#         print("Mae tem", ano_atual-anopai -1,"anos")
# else:
#     print("mae tem", ano_atual - anopai, "anos")

# if mesmae > mes_atual or \
#     (mesmae== mes_atual and diamae>dia_atual):
#         print("Mae tem", ano_atual-anomae -1,"anos")
# else:
#     print("mae tem", ano_atual - anomae, "anos")

# def idade













