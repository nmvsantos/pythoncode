# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 14:10:12 2020

@author: Nuno Santos
"""

### Slide 16
### Set é not ordered assim, não podemos imprimir o primeiro e rest valores
# f ={"apple", "orange","banana"}
# print(f)
# print(len(f))
# ###print(f[0]) Como não é ordered não funciona
# m = set("a")
# print(m)
# ###n = set("a","b") ## Não funcio porque só aceita um elemento s
# o = set(("a","b")) # Então temos que colocar desta maneira com o ()
# print(o)

### Slide 17  Funções add e update
# name ={"Nuno", "Miguel","Vicente"}
# name.add("Santos")  # Só dá para adicionar um unico valor
# print(name)

# name ={"Nuno", "Miguel","Vicente"}
# name.add(("Santos", "Jorge"))
# print(name)  ## Coloca um tuple dentro do set como elemen temos que usar o .update


# name ={"Nuno", "Miguel","Vicente"}
# name.update(["Santos", "Jorge"]) ## Aqui podemos, então colocar + do que 1 valor
# print(name) 

# name ={"Nuno", "Miguel","Vicente"}
# name.update("Santos")  ## Não funcio porque coloca letra a letra
# print(name)

### Slide 18 Funções remove, discard, copy, clear and copy
# name ={"Nuno", "Miguel","Vicente", "Santos"}
# name.remove("Santos")
# name.discard("Santos")

# name ={"Nuno", "Miguel","Vicente", "Santos"}
# x =name.pop()  # elimina um valor qualquer porque ñ é ordered
# print(x)
# print(name)

# name ={"Nuno", "Miguel","Vicente", "Santos"}
# c = name.copy() # São independente, se remover num sitio não remove do outro
# v = name # São dependente, remove de um lado e do outro
# x =name.pop()
# print(c)
# print(v)

# name ={"Nuno", "Miguel","Vicente", "Santos"}
# name.clear()  # Limpa os valores do set
# print(name)

### Slide 21  Uniões e intersecções
# x = {5,10,15}
# y = {10,15,20}
# print(x.union(y)) ## Junta tudo e elimina os repetidos
# print(x|y)

# x = {5,10,15}
# y = {10,15,20}
# print(x.intersection(y)) ## Junta tudo e só ficam os repetidos
# print(x&y)

# x = {5,10,15}
# y = {10,15,20}
# x.update(y)
# print(x) # Tem a mesma acção do union

###Slide 22 diferença ou menos
# a = {1,2,3,4,5}
# b = {2,4,7}
# print(a-b) #elimina da lista a o numero que estiverem rep na lista b
# print(b-a) # elimina na lista b os numero que estiverem rep na lista a

# r= a.difference(b)
# print(r)  ## Utilizando a função a.difference(b) temos o mesmo que a-b

# a = {1,2,3}
# b = {2,3,4}
# print(a.symmetric_difference(b)) # Elimina os valores repetidos
# print(a.union(b)-a.intersection(b)) # Faz o mesmo que acima

# a = {1,2,3}
# b = {2,3,4}
# r = a.difference_update(b)
# print(r)
# print(a) # Retira do set a o valores que repetem com a lista b
# print(b) # O set b não mexe


###Exercise 1
# a = {1,2,3,4,6,9,10}
# b = {1,3,4,9,14,13,15}
# c = {1,2,3,6,9,11,12,14,15}

# ##primeira imagem
# print(c-a.intersection(b,c))
# print(c-a.intersection(b).intersection(c))
# print(c.difference(a&b&c))

# #Segunda imagem
# print(a.intersection(b) - a.intersection(b,c))
# print(a.intersection(b) - c)   
# print((a&b)-c) 
  
# #Terceira imagem
# print(a.union(b) - c)
# print((a|b) - c)

### Slide 26 Como actualizar um set
# s = "Nuno"
# l = [13,25]
# t = (7,8)
# d = {"um":1, "dois":2}
# x = {23,32}
# x.update(s,l,t,d)
# print(x) #divide tudo em elemento singulares, o stringé dividido em letras
#         # No caso do dicionário, os keys mantém o nome completo

### Slide 27 e 28 função isdisjointe subset
# x = {"n","u"}
# y = {"n","u","o"}
# print(x.isdisjoint(y))  # Quando é False, que dizer que HÁ intersecção

# x = {1,2}
# y = {3,7,8}
# print(x.isdisjoint(y))  # Quando é True, que dizer que NÃO HÁ intersecção

# x = {1,2,4}
# y = {1,2,3,4,5}
# print(x.issubset(y)) # True quer dizer que x está dentro de y
# print(y.issubset(x)) # False quer dizer que y não está dentro de x


### Slide 31 Exercise 2
# a = {"a","y","c","o", "z"}
# b = "Python Course"
# b_set = set(b)  # Para converter string em set
# print(a.intersection(b_set))

### Homework 1
d1 = {"a" : 1, "b" : 3, "c" :2}
d2 = {"a" : 2, "b" : 3, "c" :1}

d11 = d1.items()
d111 = set(d11)
print(d111)
d22 = d2.items()
d222 = set(d22)
print(d222)
print(d111.intersection(d222))






