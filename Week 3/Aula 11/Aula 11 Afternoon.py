# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:03:07 2020

@author: Nuno Santos
"""

### Para converter em dicionário precise de 2 lista de dados
# print(dict([(1,2),(3,4)]))  ## Neste caso (1,2) e (3,4)

### Revisão de loops
# for title in range(3,20,4):
#      print("First loop:", title)

# for title in [3,7,11,15,19]:
#     print("Second loop:" , title)
    
# for title in [3,7,11,15,19]:
#     title +=2
#     print("Third loop:" , title)
    
# for title in [3,7,11,15,19]:
#     if title > 7 : # imprime só acima do 7
#         title +=2
#         print("fourth loop:" , title)

# for title in [3,7,11,15,19]:
#     if title in [7,15] : # imprime só estes
#         title +=2
#         print("fifth loop:" , title)

# for title in [3,7,11,15,19]:
#     if title not in [7,15, 10] : # imprime tudo excepto estes
#         #title +=2
#         print("fifth loop:" , title)

# a,b = 2,3  # O valor a recebe o valor 2 e o b recebe o valor 3
# print(a)
# print(b)

### Tuple é melhor para guardar dados porque é immutable
### Para remover os duplicados de uma lista, converte para set() e depois para list
### dicionário é o melhor para contar o numero de ocorrencias
### Para calculos com numero o melhor é array

import numpy
a = [1,3.14]
numpy.array(a)
print(numpy.array(a))

m = [[1,2,3],[4,5,6,],[7,8,9]]

m2 = numpy.array(m)
m2.T
print(m2)




























