# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 09:57:48 2020

@author: Nuno Santos
"""

"""
Aula 11 part 2 sobre Set

"""

### Para definir um set vazio usamos set() não existe outra maneira
# a = set()

# ### Set é not ordered assim, não podemos imprimir o primeiro e rest valores
# f ={"apple", "orange","banana"}
# print(f)
# ###print(f[0]) Como não é ordered não funciona
# m = set("a")
# print(m)
# ###n = set("a","b") ## Não funcio porque só aceita um elemento s
# o = set(("a","b")) # Então temos que colocar desta maneira
# print(o)


### Slide 17 
# f ={"apple", "orange","banana"}
# f.add("cherry")  # Só dá para adicionar um unico valor
# print(f)

# f ={"apple", "orange","banana"}
# f.add(("cherry", "mango"))
# print(f)  ## Coloca um tuple dentro do set como elemen temos que usar o .update


# f ={"apple", "orange","banana"}
# f.update(["mango", "grapes"]) ## Aqui podemos, então colocar + do que 1 valor
# print(f) 

# f ={"apple", "orange","banana"}
# f.update("mango")  ## Não funcio porque colocar letra a letra
# print(f)

### Slide 18
# vowels = {"a", "e", "i", "o", "u"}
# vowels.remove("a") # Se o valor não tiver na lista dá erro
# print(vowels)

# vowels = {"a", "e", "i", "o", "u"}
# vowels.discard("a") # Se o valor não tiver na lista não dá erro
# print(vowels)

# vowels = {"a", "e", "i", "o", "u"}
# v2 = vowels
# v3 = vowels.cope

# vowels = {"a", "e", "i", "o", "u"}
# x = vowels.pop() # elimina um valor qualquer porque ñ é ordered
# print(x)
# print(vowels)

# vowels = {"a", "e", "i", "o", "u"}
# vowels.clear()
# print(vowels)

### Slide 20
# x ={1,2,3}
# y ={2,3,4}
# print(x.union(y)) # junta os dois e remove os duplicados
# print(x|y)

# x ={1,2,3}
# y ={2,3,4}
# print(x.intersection(y))  # Só mostra os numeros que estão em duplicado
# print(x&y)  

# x ={1,2,3}
# y ={2,3,4}
# x.update(y)
# print(x)  #junta os dois e remove os duplicados o mesmo que union mas altera 
            # o valor do x
        
### Slide 21
# a ={1,2,3,4,5}
# b ={2,4, 7}
# print(a-b)  #retira da lista a os valores iguais que estão na lista b
# print(b-a)  #retira da lista b os valores iguais que estão na lista a

# r = a.difference(b)
# print(r)
# print(a)
# print(b)

# x ={1,2,3}
# y ={2,3,4}    # Estes 4 prints fazem o mesmo, retirar os valores iguais nas 2 listas
# print(x.symmetric_difference(y))
# print(x ^y)
# print(x.union(y) - x.intersection(y))
# print(x.union(y) - y.intersection(x))

# a ={1,2,3,4,5}  # retirar os valores iguais nas 2 listas
# b ={2,4, 7}
# r = a.difference_update(b)
# print(r)
# print(a)
# print(b)

###Slide 23 Exercise 1
# a = {1,2,3,4,6,9,10}
# b = {1,3,4,9,14,13,15}
# c = {1,2,3,6,9,11,12,14,15}

# ##primeira imagem
# print(c-a.intersection(b,c))
# print(c-a.intersection(b).intersection(c))
# print(c.difference(a&b&c))

##Segunda imagem
# print(a.intersection(b) - a.intersection(b,c))
# print(a.intersection(b) - c)   
# print((a&b)-c) 
  
##Terceira imagem
# print(a.union(b) - c)
# print((a|b) - c)

## Slide 26
# x = {1,2}
# y = {1,2,3}
# print(x.isdisjoint(y))  # Quando é False, que dizer que HÁ intersecção

# x = {1,2}
# y = {3,7,8}
# print(x.isdisjoint(y))  # Quando é True, que dizer que NÃO HÁ intersecção

## Slide 27
# x = {1,2}
# y = {1,2,3,4,5}
# print(x.issubset(y)) # True quer dizer que x está dentro de y
# print(y.issubset(x)) # False quer dizer que y não está dentro de x

###Slide 29 Exercise 2
# a = {"a","y","c","o", "z"}
# b = "Python Course"
# d = set()
# for i in b:
#     for j in a:
#         if j==i:
#             d.update(i)
# print(d)

# #só com interse

# b_set = set(b)
# print(a.intersection(b_set))

###Slide 33 Homework
# d1 = {"a" : 1, "b" : 3, "c" :2}
# d2 = {"a" : 2, "b" : 3, "c" :1}

# d = {}
# for k , v in d1.items():
#     for a,b in d2.items():
#         if v==b and k==a:            
#             d.setdefault(k,b)
# print(d)

#****************
d1 = {"a" : 1, "b" : 3, "c" :2}
d2 = {"a" : 2, "b" : 3, "c" :1}

d11 = d1.items()
d111 = set(d11)
print(d111)
d22 = d2.items()
d222 = set(d22)
print(d222)
print(d111.intersection(d222))
