# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 08:49:02 2020

@author: Nuno Santos
"""

### Slide 51 Exercise 6
# s = "Python Course"
# z = ["o", "r"]
# d = {}
# for i in s:
#     if i in z:
#         d[i] = d.get(i,0) + 1
#     else:
#         continue
# print(d)

# ###or

# s = "Python Course"
# z = ["o", "r"]
# d = {}
# for i in s:
#     if i in z:
#         d[i] = d.setdefault(i,0) + 1
#     else:
#         continue
# print(d)

###Slide 53 Exercise 7
# d = {"x" : 3, "y" : 2, "z" : 1, "y" : 4, "z" : 2}  
# print(d)
# for k,v in d.items:
#     if k not in r.keys():
#         r[k] = v
# print(r)
#### Answer Porque o dicionário não pode aceitar mais do que uma vez a mesma key

### Slide 57 Exercise 8
# student = [{"id": 123, "name" : "sophia", "s" : True}, 
#            {"id": 378, "name" : "William", "s" : False}, 
#            {"id": 934, "name" : "Sara", "s" : True}]

# ##a = student[0]["s"] ## Para mostrar o valor
# c = 0
# for i in student:
#     c += int(i["s"]) ## Fizermos o i["s"] acessamos ao valor True ou ao 3º valor
# print(c)

### Outra forma de resolver

student = [{"id": 123, "name" : "sophia", "s" : True}, 
            {"id": 378, "name" : "William", "s" : False}, 
            {"id": 934, "name" : "Sara", "s" : True}]

print(sum(i["s"]for i in student))  

### Slide 61
# d = {"F":0 , "B" :0}

# import random
# for i in range(17):
#     d[random.choice(list(d.keys()))] += 1
# print(d)





























