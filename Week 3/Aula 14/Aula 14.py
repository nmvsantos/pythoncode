# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 08:35:01 2020

@author: Nuno Santos
"""

### Exerc6 ficha 2
# DIA_ATUAL = 2
# MES_ATUAL = 11
# ANO_ATUAL = 2015

# print("Dados do Pai")
# anoPai = int(input("Introduza o ano de nascimento: "))
# mesPai = int(input("Introduza o mes de nascimento: "))
# diaPai = int(input("Introduza o dia de nascimento: "))
# print("Dados da Mae")
# anoMae = int(input("Introduza o ano de nascimento: "))
# mesMae = int(input("Introduza o mes de nascimento: "))
# diaMae = int(input("Introduza o dia de nascimento: "))
    
# if mesPai > MES_ATUAL or \
#     (mesPai == MES_ATUAL and diaPai > DIA_ATUAL):
#     print("Pai tem", ANO_ATUAL - anoPai - 1, "ano(s)")
# else:
#     print("Pai tem", ANO_ATUAL - anoPai, "ano(s)")
    
# if mesMae > MES_ATUAL or \
#     (mesMae == MES_ATUAL and diaMae > DIA_ATUAL):
#     print("Mae tem", ANO_ATUAL - anoMae - 1, "ano(s)")
# else:
#     print("Mae tem", ANO_ATUAL - anoMae, "ano(s)")
    

# def pede_informação():
#     ano = int(input("Introduza o ano de nascimento: "))
#     mes = int(input("Introduza o mes de nascimento: "))
#     dia = int(input("Introduza o dia de nascimento: "))

# def calcul_idade (nome, dia, mês, ano)
    
    
### Exercicio7 ficha 2

# def o_maior(a,b):
#     """
#     Devolve o valor maior entre 2 inteiros

#     Parameters
#     ----------
#     a : Inteiro
#         Inserir um número inteiro
#     b : Inteiro
#         Inserir um número inteiro

#     Returns
#     -------
#     Retorna o maior valor inteiro entre os dois valores.

#     """
#     if a > b:
#         print(a)
#     else:
#         print(b)

# o_maior(4,2)
# ## ou
# def maximo(a,b):
#     return a if a>b else b

# print(maximo(2,16))
## ou
# def maximum(a,b):
#     return max(a,b)

# print(maximum(2,2))

## segunda parte do 7
# def maximum(a,b):
#     return max(a,b)

# def minimum (a,b):
#     return b if a == maximum(a,b) else a

# print(minimum(3,3))

### Exercicio8 ficha 2

# def elimina_unid(a):
#     a = str(a)
#     b= len(a)    
#     if b == 1:
#         return 0
#     else:
#         a = a[0:-1]        
#     return a
    
# print(elimina_unid(57))

##or
# def retira(numero):
#     str_numero = str(numero)
#     if len(str_numero)==1:
#         return 0
#     else:
#         return int(str_numero[0:-1])
# print(retira(537))


# ### Exercicio9 ficha 2

# def add_zero(a):    
#     c = str(a) + str(0)  
#     return c

# print(add_zero(53))


### Ficheiros
## Caso o ficheiro não esteja na mesma pasta temos que colocar
##C:\Users\Nuno Santos\Desktop\pythoncode\Aula 14

# f = open("blofile.txt") # Aqui temos que ter o ficheiro na mesma pasta
# print(f)
## <_io.TextIOWrapper name='blofile.txt' mode='r' encoding='cp1252'>
## contem o nome do ficheiro; o modo se é read, write or create; odefault é "r"

# d = open("blofile.txt", "r") # Aqui é em modo leitura 
# print(d)
# ## Só permite fazer a leitura do ficheiro não permite escrever
# ## 

# d = open("blofile.txt", "w") # Aqui é em modo escrita 
# print(d)
# ## Abre o ficheiro e escreve no ficheiro MAS vais apagar a informação anterior
# ## Se o ficheiro não existir ele vai criar o ficheiro e escreve lá dentro

# e = open("blofile.txt", "a") # Aqui é apende, escreve no ficheiro mas não apaga
# print(e)                     ## nada do que lá estava.
 

# g = open("blofile.txt", "x") # Cria um ficheiro e dá erro se o ficheiro existir
# print(g)

### Para abrir um ficheiro e ler o conteudo
#f = open("blofile.txt") # Aqui temos que ter o ficheiro na mesma pasta
# print(f.read())

# read_results = f.read()
# print(read_results)

# f = open("blofile.txt")
# read_results1 = f.read(4) # Só lê os 3 primeiros caracteres
# print(read_results1)

### Para ler uma linha NOTA só lê a linha nº1
# f = open("blofile.txt")
# read_results = f.readline() # Só imprime a linha nº1
# #print(read_results)
# print(f.readline())   
# print(f.readline())

"""
Se colocarmos várias vezes "print(f.readline())" podemos ler várias linhas por
exemplo se quiser a linha 3 temos que colocar 2 vezes
"""

# for line in f:  # Faz o mesmo do que está em cima
#     print(line)

### Para fechar o ficheiro
# f.close()

## faz o mesmo que f.close
# with open("blogfile.txt") as file_reader:
#     for line in file_reader:  
#         print(line)

### Para escrever alguma coisa no ficheiro com o apend
# f = open("blofile.txt", "a")
# f.write("\nNow the file has more content!")
# f.close()

# f = open("blofile.txt", "r")
# print(f.read())


### Para escrever alguma coisa no ficheiro com o w
# f = open("blofile.txt", "w")
# f.write("\nNow the file has more content!")
# f.close()

# f = open("blofile.txt", "r")
# print(f.read())
# ### Aqui apagou toda a informação e colocou só o f.write

### Esta função dá o caminho absoluto do ficheiro
# import os
# print(os.path.abspath("blofile.txt")) #Esta dá o caminho absoluto do file
# print(os.path.basename("./blofile.txt")) # Dá o nome do ficheiro
## o ./ vai à directoria actual

## Remover um ficheiro
## os.remove("blofile.txt)")


## Esta função lê linhas para uma lista de strings
# lines = []
# lines_after_writing = []

# with open("blofile.txt") as file_reader:
#     lines = file_reader.readlines()
#     print(lines)

# with open("blofile.txt", "w") as file_writer:
#     file_writer.writelines(lines)
#     print(lines)

# with open("blofile.txt") as file_reader:
#     lines_after_writing = file_reader.readlines()
#     print(lines_after_writing==lines)


## Se numa linha troca os t por p

# import copy
# lines = []
# lines_for_writing = []
# lines_after_writing = []

# with open("blofile.txt") as file_reader:
#     lines = file_reader.readlines()
#     lines_for_writing = copy.deepcopy(lines)
#     for  line in lines:
#         line.replace("s","p")
#     print(lines)

# with open("blofile.txt", "w") as file_writer:
#     file_writer.writelines(lines)
#     print(lines)

# with open("blofile.txt") as file_reader:
#     lines_after_writing = file_reader.readlines()
#     print(lines_after_writing==lines)

## Se quisermos editar a mensagem do erro
# try:
#     int("abc")
# except ValueError:
#     print("The string must contain numbers not letter")
# except:
#     print("Alguma coisa que correu mal")
# finally:
#     print("thanks")
### Se escrever o "abc" print o except se colocar outra coisa print o finally


# try:
#     a = int("12")
#     a/0
# except ValueError:
#     print("The string must contain numbers not letter")
# except:
#     print("Alguma coisa que correu mal")
# finally:
#     print("thanks")

# with open("blofile.txt") as file_reader:
#     try:
#         file_reader.readlines()
#         #do something
#     except expression as identifier:
#         identifier
#         #Handle exception

# try:
#     # do somethin
# except: expression as identifier:
#     #handle exception
# finally:
#     f.close


   
    