# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 17:07:47 2020

@author: Nuno Santos
"""

### Exercicio 1
## No primeiro código abria o ficheiro em modo escrita, apagava o conteudo e 
## escrevia no ficheiro o numero de 0 a 99

##No segundo código abre a variavel indata fica com o valor que estiver no
##"infile.txt"

## No terceiro codigo imprime duas vezes a informação presente no "infile.txt"

### Exercicio 2 

# with open("infile.txt") as file_reader:
#     for line in file_reader:  
#         print(line)

### Exercicio 3 
# c=0
# with open("infile.txt") as file_reader:
#     for line in file_reader:  
#         c = c+1
#         print("linha numero", c,line)

### Exercicio 4
# from statistics import mean

# def medias(ficheiro):
#     try:
#         with open (ficheiro) as file_reader:
#             for line in file_reader:
#                 line = line.strip()
#                 split_line = line.split()
#                 for i in range(len(split_line)):
#                     split_line[i] = float(split_line[i])
#                 print(round(mean(split_line)))
#     except:
#         print("Someting wrong")                      
            
# medias("Exercicio_4_ficha_4.txt")
   

### Exercicio 5
# from statistics import mean

# def medias(ficheiro):
#     try:
#         with open (ficheiro) as file_reader:
#             for line in file_reader:
#                 line = line.strip()
#                 split_line = line.split()
#                 for i in range(len(split_line)):
#                     split_line[i] = float(split_line[i])
#                 print(round(mean(split_line)))
#     except:
#         print("Erro de I/O")      

# def ficheiro_falha():
#     user_input = input("indique ficheiro")
#     try:
#         medias(user_input)
#     except:
#         print("não foi possivel abrir")                
            
# medias("Exercicio_4_ficha_4.txt")

### Exercicio 6


# def linha_para_elemento(ficheiro):
#     with open("quimicos.txt") as file_reader:
#         keys=["nome", "atómico", "densidade"]
#         a = keys[0]
#         b = keys[1]
#         c = keys[2]    
#         d1 = []
#         d2 = []
#         d3 = []
#         d4 = {}
#         d5 = {}
#         d6 = {}
#         d_final = {}
#         for line in file_reader:
#             split_line = line.split()
#             # for i in range(len(split_line)):
#             #     split_line[i]
#             for z in range(0,len(split_line),3):
#                 d1.append(split_line[z])
#                 d4.setdefault(a,d1)
#             for x in range(1,len(split_line),3):
#                 d2.append(split_line[x])
#                 d5.setdefault(b,d2)
#             for y in range(2,len(split_line),3):
#                 d3.append(split_line[y])
#                 d6.setdefault(c,d3)
#     d4.update(d5)
#     d4.update(d6)
#     d_final = d4.copy()
#     print(d_final)
#     # print(d5)
#     # print(d6)
  

# linha_para_elemento("Quimicos.txt")     


### Exercicio1  CSV

# import csv
# def le_grafico(ficheiro_csv):
#     grafico = []
#     with open(ficheiro_csv) as csvfile:
#         csv_reader = csv.reader(csvfile, delimiter = ";")
#         linha1 = csv_reader.__next__()
#         linha2 = csv_reader.__next__()
#         for i in range(len(linha1)):
#             item_linha1_com_ponto = linha1[i].replace(",",".")
#             item_linha2_com_ponto = linha2[i].replace(",",".")
#             grafico.append((float(item_linha1_com_ponto),float(item_linha2_com_ponto)))
#         return grafico
        
# print(le_grafico("graficos.csv"))


### Exercicio 2 CSV
# import csv

# def graficocsvlinha(ficheiro):
#     with open("ficheiro2.csv") as csv_file:
#         csv_reader = csv.reader(csv_file,delimiter = ",")
#         linha1 = csv_reader.__next__()
#         print("Abcissa" , linha1)
#         linha2 = csv_reader.__next__()
#         print("ordenadas",linha2)

# graficocsvlinha("ficheiro2.csv")

### Exercicio 3
# import csv

# def graficocsvlinha(ficheiro):
#     with open("ficheiro2.csv") as csv_file:
#         csv_reader = csv.reader(csv_file,delimiter = ",")
#         linha1 = csv_reader.__next__()
#         print("Abcissa" , linha1)
#         linha2 = csv_reader.__next__()
#         print("ordenadas",linha2)
#         l = list(zip(linha1,linha2))
#         print("Abcissa + ordenada dá:",l)
            

# graficocsvlinha("ficheiro2.csv")



















