# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 08:46:15 2020

@author: Nuno Santos
"""

# import copy
# a = ["abc", "def", "ghi"]
# b = copy.copy(a)
# b[2] = "c"

###Exercicio  1 programa que leia a temperatura em grau fahrenheir e que a converta
### em Celsius
### Função Celsius = (F-32)/1.8

# a = float(input("Qual a temperatura em Fah?"))
# b = (a-32)//1.8
# print("A temperatura em Celsius é" , b)

###Exercicio 2  programa que peça o ano de nascimento ao utilizador e que 
### retorne a idade no fim do ano

# a = int(input("Qual é o ano de nascimento?"))
# b = 2020-a
# print("No final de ano terá", b)

###Exercicio 3 Leia uma hora em horas, minutos e segundo e que traduza para
### para segundo

# a = int(input("Quantas as hora?"))
# b = int(input("Quantos minuto"))
# c = int(input("Quantos segundos?"))

# a1 = a*3600
# b1 = b*60

# tot = a1+b1+c
# print(tot, "segundos")

### Resolução mais correcta

# h =input("Quantas as hora?")
# h_1 = h.split(":")
# segundo_total = int(h_1[0])*3600 + int(h_1[1])*60 + int(h_1[2])
# print(segundo_total, "segundos")

### Exercicio 4  Se no inicio a a variavel x tiver 1
# x =1
# if x == 1 :
#      x = x + 1
#      if x == 1:   ## Este nunca é executado
#             x = x + 1
#      else :
#             x = x-1
# else:
#     x = x-1
# print(x)

### Exercicio 5  Prog que recebe um número inteiro e que o converta em romana

# a = int(input("Escreve um numero"))
# n =(1000,900,500,400,100,90,50,40,10,9,5,4,1)
# rom = ("M", "CM", "D" ,"CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I")
# nn = tuple(zip(n,rom))


# d = {1:"I", 4:"IV", 5:"V", 9:"IX", 10:"X", 40:"XL", 50:"L", 90:"XC", 100:"C",
#      400:"CD", 500:"D", 900:"CM", 1000: "M" }
# r = ""
# for i in n:
#     q = a // i
#     if q != 0:
#         for _ in range(q): ### Se colocar o _ é para ignorar a variável
#             r += d[i]
#     a = a % i 
# print(r)        

###Outra solução do exercicio 5
# a = int(input("Escreve um numero"))
# n =(1000,900,500,400,100,90,50,40,10,9,5,4,1)
# rom = ("M", "CM", "D" ,"CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I")
# nn = tuple(zip(n,rom))
# r = [] 
# for decimal, letra_romana in nn:
#     count= a//decimal
#     r.append(letra_romana*count)
#     a -= decimal*count
# print("".join(r), end=" ")

### Exercicio 6  1 programa que leia um ano e que escreva o século a que pertence
###
# a = int(input("Escreve um ano"))
# t = a//100
# r = a%100
# print(t if r ==0 else t+1)

### Exercicio 7  Programa recebe o ano,mês e dia em separado e um número de 
### dias x e devolva uma nova data x dias mais tarde
ano = int(input("Escreve um ano"))
mes = int(input("Escreve um mês"))
dia = int(input("Escreve um dia"))
x = int(input("Quantos dias à frente"))

meses = {1:31, 2:28, 3:31 , 4:30, 5 :31, 6:30 , 7:31 , 8:31, 9:30, 10:31, 11:30,
         12:31}



if mes == 12:
    
    dias_do_mes = meses[mes]
    if dia + x > dias_do_mes:
            mes =1
            dia = x - (dias_do_mes-dia)
            ano = ano + 1
    else:
            dia = dia + x
else:
    if ano %4 == 0 and not(ano%400 ==0 and ano%100 ==0):
        dias_do_mes = 0
        if mes ==2:
            dias_do_mes = meses[mes] + 1
        else:
            dias_do_mes = meses[mes]
        if dia + x > dias_do_mes:
            mes = mes + 1
            dia = x - (dias_do_mes-dia)
        else:
            dia = dia + x
    else:
        dias_do_mes = meses[mes]
        if dia + x > dias_do_mes:
            
            mes = 1
            dia = x - (dias_do_mes-dia)
            ano = ano +1
        else:
            dia = dia +x
print(f"{ano}-{mes}-{dia}")










