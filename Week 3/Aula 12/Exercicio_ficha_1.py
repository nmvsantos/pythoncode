# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 14:46:15 2020

@author: Nuno Santos
"""

###Exercicio  1 programa que leia a temperatura em grau fahrenheir e que a converta
### em Celsius
# a = int(input("Temperatura em Fah"))
# celsius = round((a-32)/1.8)  # Arredondar usa o "round
# print(celsius , "celsius")

###Exercicio 2  programa que peça o ano de nascimento ao utilizador e que 
### retorne a idade no fim do ano

# from datetime import datetime
# a = int(input("Ano de nascimento"))
# b = datetime.now().year
# c = b-a
# print(c)

# ##Exercicio 3 Leia uma hora em horas, minutos e segundo e que traduza para
# ## para segundo
# a = input("Escreva um hora separada por :")
# b = a.split(":")
# x = int(b[0])*3600
# y = int(b[1])*60
# tot = x + y + int(b[2])
# print(tot)   
    
### Exercicio 4

# a = int(input("Escreva um numero em segundos"))
# h = a//3600
# m = (a-(h*3600))//60
# s = a%60
# print(h,"horas", m , "minutos", s, "segundos")

### Exercicio 5
# x = 0
# if x == 1:
#     x = x + 1
#     if x == 1:  ### c)
#         x = x + 1
#     else:
#         x = x - 1
# else:
#     x = x - 1
### a) Terá o valor 1
### b) Teria que ter o valor de 0
### c) Assinalado no programa


###Exercicio 6  Prog que recebe um número inteiro e que o converta em romana

# a = int(input("Escreve um numero inteiro"))
# n =(1000,900,500,400,100,90,50,40,10,9,5,4,1)
# rom = ("M", "CM", "D" ,"CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I")
# nn = tuple(zip(n,rom))


# d = {1:"I", 4:"IV", 5:"V", 9:"IX", 10:"X", 40:"XL", 50:"L", 90:"XC", 100:"C",
#       400:"CD", 500:"D", 900:"CM", 1000: "M" }
# r = ""
# for i in n:
#     q = a // i
#     if q != 0:
#         for _ in range(q): ### Se colocar o _ é para ignorar a variável
#             r += d[i]
#     a = a % i 
# print(r) 

### Exercicio 7  1 programa que leia um ano e que escreva o século a que pertence
###
# a = int(input("Escreve um ano"))
# t = a//100
# r = a%100
# if r ==0:
#     t
# else:
#     t=t+1
# print(t)   

### Exercicio 8  Programa recebe o ano,mês e dia em separado e um número de 
### dias x e devolva uma nova data x dias mais tarde
# ano = int(input("ano: "))
# mes = int(input("mês: "))
# dia = int(input("dia: "))
# dias_add = int(input("que dia será daqui a este número de dias? "))

# meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

# e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
# dias_do_mes = meses_dias[mes] + e_ano_bisexto # you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

# while dia + dias_add > dias_do_mes:
#     ano += (mes + 1) > 12 # é adicionado um se o proximo mês for menor que 12 (Dezembro)
#     mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 # se o mês for maior que doze incrementamos o mês o obtemos o resto da divisão (ex: mês = 13 então mês % 12 = 1)
#     dias_add -= dias_do_mes - dia # removemos os restantes dias que faltam no mês dos dias adicionados
#     dia = 0 # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
#     dias_do_mes = meses_dias[mes] + e_ano_bisexto # atualizamos os dias do mês

# dia += dias_add # adicionamos os restantes dias do mês

# print(f"{ano}-{mes}-{dia}")

### Exercicio 9
# x = 1
# y = 1
# while x == 1 and y < 5:
#     y = y + 2
##Rest. falta "=" no "x=1" e o ":" no fim do while


### Exercicio 10
# n = int(input("Escreve um número inteiro: "))
# print("Tabuada do", n, ":")
# i = 1
# while i <= 10:
#     print(n, "x", i, "=", n * i)
#     i = i + 1
## Resp. no fim do while deveria ter "i = i + 1" em vez de "i+1"
## Resp. O n terá o valor colocado por user o i terá o valor de 11 para parar

### Exercicio 11
# dividendo = int(input("Dividendo: "))
# divisor = int(input("Divisor: "))
# resto = dividendo
# quociente = 0
# while resto >= divisor:
#     resto = resto - divisor
#     quociente = quociente + 1
#     print("O quociente é", quociente, "e o resto é", resto)

### a)
### Resp- pede 2 valores ao user, depois iguala a variavel resto à variavel \
### dividendo, no loop enquanto o resto for maior ou igual divisor faz as \
### operações resto - divisor e ao quociente adiciona 1 unid (que começa em 0)
### Feita estas 2 opera imprime a frase. Retorna ao loop se a condição
### resto >= divisor for verdadeira

### b)


### c)

# dividendo = int(input("Dividendo: "))
# divisor = int(input("Divisor: "))
# resto = dividendo
# quociente = 0
# if dividendo < divisor:
#     print("dividendo menor que o divisor")
#     dividendo = int(input("Dividendo: "))
#     divisor = int(input("Divisor: "))
# else:
#     while resto >= divisor:
#         resto = resto - divisor
#         quociente = quociente + 1
#         print("O quociente é", quociente, "e o resto é", resto)

### d)
# dividendo = int(input("Dividendo: "))
# divisor = int(input("Divisor: "))
# resto = dividendo
# quociente = 0
# while dividendo <0 or divisor <0:
#     print("dividendo ou divisor menores que 0")
#     dividendo = int(input("Dividendo: "))
#     divisor = int(input("Divisor: "))
    
# while dividendo < divisor:
#         print("dividendo menor que o divisor")
#         dividendo = int(input("Dividendo: "))
#         divisor = int(input("Divisor: "))
    
# while resto >= divisor:
#             resto = resto - divisor
#             quociente = quociente + 1
# print("O quociente é", quociente, "e o resto é", resto)

### Exercicio 12                                             #Não cosegui
# n = int(input("Escreve um número inteiro: "))
# current_n = 1
# while current_n <= n:
#     i = current_n
#     f = 1
#     while i >= 1:
#         f = f * i
#         i = i - 1
#         print("Factorial de " + str(current_n) + ": " + str(f))
#         current_n = current_n + 1

### Exercicio 13
# a = float(input("Escreva um numero décimal "))
# n = round(a)
# print("Parte inteira do numero é ", n)

### Exercicio 14
# k = int(input("Escreva um numero inteiro"))
# w = input("Escreva uma palavra")

# i = 0
# while k>0:
#     i = i+1
#     k = k-1
#     print("linha nº", i , (w + " " )*i)

### Exercicio 15
# k = int(input("Escreva um numero inteiro"))
# i = 0
# while k>=0:  
#     print(3**i)
#     i = i+1
#     k = k-1


###Exercicio 16                                               #Não consegui
k = int(input("Escreva um numero inteiro maior que 2 "))
soma = 0
for val in range(1,k):
        if k % val == 0:
            soma += +1
if soma == k:
    print(val)


###Exercicio 17
# k = int(input("Escreva um numero inteiro maior que 10 "))
# c=0
# for i in range(10,k):
#     num_s = str(i)
#     num_list = list(num_s)
#     if num_list == num_list[::-1]:
#         capi = "".join(num_list)
#         c+=1
#         print(capi)
# print("total de capicuas", c)

### Extra, inverte os elementos numa lista
# a = ["2","1"]
# print(a[::-1])






