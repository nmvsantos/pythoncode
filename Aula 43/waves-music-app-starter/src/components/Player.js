import{FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPlay, faPause, faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons"
import {useRef, useState} from "react"

const Player = ({currentSong, setCurrentSong, songs, setSongs, isPlaying, setIsPlaying, audioRef}) =>{

    
    
    const animateTrackRef = useRef(null);
    const [songInfo, setSongInfo] = useState({
        current :0,
        duration :0,
    }) // 1 criar estado song info
    const animationPercentage = Math.round((songInfo.current/ songInfo.duration) *100);

//2.1 Adicionar handlers nas setinhas
//2.2 parar a musica atual que estiver a tocar
//2.3 obter o indice da musica atual 
//2.4 obter o indice da musica seguinte ou anterior



const onAudioPlayHandler = (event) =>{
    if (isPlaying) {
        audioRef.current.pause();
        setIsPlaying(false);
    } else {
        audioRef.current.play();
        setIsPlaying(true);
    }
}

const onTimeUpdateHandler = (event) => { // 2 criar um evento handler
    const current = event.target.currentTime;
    const duration = event.target.duration;

setSongInfo({
    current: current,
    duration: duration
});

}

const onTimeChangeHandler = (event) =>{
    audioRef.current.currentTime = event.target.value
    setSongInfo({...songInfo, current: event.target.value}) //os ... é só para copiar as propriedades, ou seja, copia independente não se alteram
}


const skipTrackHandler = async (forwards) => {
    const songsLength = songs.length;
    const currentSongIndex = songs.findIndex((song) => {
        return song.id === currentSong.id
    })
    let nextIndex;
    if (forwards){
        if (currentSongIndex === songsLength - 1) {
             nextIndex = 0;
        } else {nextIndex = currentSongIndex +1
        }
        //nextIndex = (currentSongIndex+1) % songsLength
    } else{
        if (currentSongIndex === 0) {
            nextIndex = songsLength -1;
       } else {
           nextIndex = currentSongIndex -1
       }  
       //nextIndex = (songsLength + (currentSongIndex - 1)) % songsLength
    }
    await setCurrentSong(songs[nextIndex]); //com o async e await cada vez que muda a musica e começa logo a tocar
    notifyActiveLibraryHandler(songs[nextIndex])
    setIsPlaying(true);
    audioRef.current.play();

}

const onEndedHandler = async (event) => { //Isto para por a tocar a proxima musica quando acaba a atual
    //buscar a proxima musica
    const songsCount = songs.length
    const currentSongIndex = songs.findIndex((song) => {
        return song.id === currentSong.id;
    })
    const nextIndex = (currentSongIndex+1) % songsCount;

    //tocar a proxima musica
    await setCurrentSong(songs[nextIndex]);
    notifyActiveLibraryHandler(songs[nextIndex])
    setIsPlaying(true);
    audioRef.current.play();
}

const notifyActiveLibraryHandler = (nextPrev) => {
    const newSongs = songs.map((song) =>{
        if (song.id === nextPrev.id) {
            return {
                ...song,
                active: true
            }
        } else {            
            return {
            ...song,
            active: false
            }
        }
    });
    setSongs(newSongs);
}

const getTime =(time) =>{
return(
    Math.floor(time/60) + ":" + ("0" + Math.floor(time%60)).slice(-2) //slice para converter em segundo
)
}


    return (
        <div className="Player">
            <div className="time-control">
                <p>{getTime(songInfo.current)}</p>
                <div className="track" style={{
                    backgroundImage:`linear-gradient(to right, ${currentSong.color[0]}, ${currentSong.color[1]})`
                }}>
                    <input type="range" min={0} max={songInfo.duration}  value={songInfo.current} onChange={onTimeChangeHandler}/>
                    <div className="animate-track" ref = {animateTrackRef} style = {{
                        transform: `translateX(${animationPercentage - 0.01*animationPercentage}%)`
                    }}>
                        <div className="animate-track-thumb" ></div>
                    </div>
                    
                </div>
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="play-control">
                <FontAwesomeIcon icon={faAngleLeft} size = "2x" onClick = {() => skipTrackHandler(false)}/>
                <FontAwesomeIcon icon={isPlaying ? faPause :  faPlay} size = "2x" onClick={onAudioPlayHandler}/>
                <FontAwesomeIcon icon={faAngleRight} size = "2x" onClick = {() => skipTrackHandler(true)}/>
                <audio src={currentSong.audio} ref = {audioRef} onTimeUpdate={onTimeUpdateHandler} 
                onLoadedMetadata={onTimeUpdateHandler} onEnded={onEndedHandler}  ></audio>
            </div>
        </div>
        
    )
}
export default Player