$(document).ready(function () {
  var squareIn = true;
  $("#switch").click(function () {
    if (squareIn) {
      $("#square").fadeOut(2000);
    } else {
      $("#square").fadeIn("slow");
    }
    squareIn = !squareIn;
  });
});
