# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 08:24:55 2020

@author: Nuno Santos
"""

### Aula 5

# def volume_sphere() :
#     pi = 3.1415
#     radius = float(input("Enter radius:"))
#     volume = 4/3*pi*(radius**3)
#     print(f"{volume:.3f} U")

# volume_sphere()

# ou com entrada de dados

# import math
# def volume_sphere(radius_1, power, const) :
#     volume = const*math.pi*(radius_1**power)
#     print(f"{volume:.3f} U")
    
# power = 3
# const = 1    
# radius_1 = float(input("Enter radius:"))
# volume_sphere(radius_1, power , const)

# ou com entrada de dados e com retorno do valor

# import math
# def volume_sphere(radius_1, power, const) :
#     volume = const*math.pi*(radius_1**power)
#     return volume
    
# power = 3
# const = 4/3    
# radius_1 = float(input("Enter radius:"))
# volume = volume_sphere(radius_1, power , const)
# print(f"{volume:.3f} U")

#**** slide 11

# def food(vegetable):
#     if vegetable == "tomato" :
#         print ("I bought tomato")
#     elif vegetable == "orange" :
#         print ("I bought other vegetable")
#     else:
#         print("I bought other vegetable")
# food("tomato")



#******** Slide 14 quando a função retorna o valor em duas operações

# def addtwo(a,b):
#     added = a + b
#     subtracao = a - b
#     return added , subtracao
# x = addtwo(3,5)
# print(x)

#**** Slide 14 se quiser só um valor de uma lista

# def addtwo(a,b):
#     added = a + b
#     subtracao = a - b
#     return added , subtracao
# x = addtwo(3,5)
# print(x[0])
# print(x[1])

# slide 15

# def food() :
#     eggs = "food local"
#     print (eggs)
# def more_food ():
#     eggs = "more_food local"    
#     print(eggs)
#     food()
#     print(eggs)
    
# eggs = "global" # Isto quer dizer que o eggs vai primeiro o more_food
# more_food()     # porque na parte fora do def está em primeiro lugar e é obrigatório ter a entreda da função
# print(eggs)     # As funções têm que estar em primeiro

#Slide 16
# a = 21 
# b = 10 
# c = 0

# c = a + b
# c += a # c+a
# c *= a # c*a
# c /=a  # c/a float

# c=2
# c %= a #c % a 
# c **= a #c**a
# c //= a # c/a integer

#Slide 20 Exercise 1

# n = int(input("write a number"))
# while n > 0 :
#     if n % 2 == 0:
#         print( "Even number" , n)
#         n = n - 1
#     else :
#         print ("odd number" , n)
#         n = n - 1
        
#Slide 21 Exercise 2

def mean(v) :
    total = sum(v) / len(v)
    print(total)

v = [2,61,-1,0,88,55,3,121,25,75]
mean(v)
# soma = sum(v)
# counta = len(v)
# print(soma)
# print(counta)

    
    
    
        
        














