# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 16:02:37 2020

@author: Nuno Santos
"""

## Slide 7 Como definir funções 
# def thing():
#     print('Hello')
#     print('Fun')
#                     #  Para a fazer primeiro temos que escrever a função
# thing()             # depois escrevemos a ordem de execução
# print('Zip')
# thing()


## Slide 11  #Imprimir a função com um valor dado para executá-la
# def food(vegetable):
#     if vegetable == "tomato" :
#         print ("I bought tomato")
#     elif vegetable == "orange" :
#         print ("I bought other vegetable")
#     else:
#         print("I bought other vegetable")
# food("tomato")

## Slide 12 Funcões com retorno
# def greet():
#     return "Hello"

# print(greet(), "Rodrigo")
# print(greet(), "João")

## Slide 14  # No return ele guarda os valores para usar
# def addtwo(a,b):
#     added = a + b
#     subtracao = a - b
#     return added , subtracao
# x = addtwo(3,5)
# print(x)

# def addtwo(a,b):  # Se quiser só imprimir o primeiro valor
#     added = a + b
#     subtracao = a - b
#     return added , subtracao
# x = addtwo(3,5)
# print(x[0])
# print(x[1])

## Slide 15
# def food() :
#     eggs = "food local"
#     print (eggs)
# def more_food ():
#     eggs = "more_food local"    
#     print(eggs)
#     food()
#     print(eggs)
    
# eggs = "global" # Isto quer dizer que o eggs vai primeiro o more_food
# more_food()     # porque na parte fora do def está em primeiro lugar e é obrigatório ter a entreda da função
# print(eggs)     # As funções têm que estar em primeiro

## Slide 16 diferente operações
# a = 21 
# b = 10 
# c = 0

# c = a + b
# c += a # c+a
# c *= a # c*a
# c /=a  # c/a float

# c=2
# c %= a #c % a 
# c **= a #c**a
# c //= a # c/a integer

##Slide 20 Exercise 1

# n = int(input("write a number"))
# while n > 0 :
#     if n % 2 == 0:
#         print( "Even number" , n)
#         n = n - 1
#     else :
#         print ("odd number" , n)
#         n = n - 1
        
##Slide 21 Exercise 2

# def mean(v) :
#     total = sum(v) / len(v)
#     print(total)

# v = [2,61,-1,0,88,55,3,121,25,75]
# mean(v)
# soma = sum(v)
# counta = len(v)
# print(soma)
# print(counta)

























