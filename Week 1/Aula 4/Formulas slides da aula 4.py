# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 15:43:17 2020

@author: Nuno Santos
"""

##Slide5 Loop

# for i in [5,4,3,2,1,0]:
#     print (i)
# print("Boom!")

##Slide6  Loop para strings
# friends = ["Ana", "Filipe" , "joão", "Miguel"]
# for friend in friends:
#     print("Congrats on your new job", friend)
#print("done!")

## Slide 8 loop in a range
# print('Before')
# for i in range(5):
#     print(i)
# print('After')

## Different ranges
# print("Before")
# for i in range(2,6) :
#     print (i)
# print("after")
    
# print("Before")
# for i in range(15,0,-5) :
#     print (i)
# print("after")

## Exercise 1
# a = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
# for i in a :
#         if i.isupper() :
#             print(i , end="")


##Exercise 2
# for i in range(0,50,4) :
#     print (i)

##Slide 15
# largest_so_far = -1
# print('Before', largest_so_far)
# for the_num in [9, 41, 12, 3, 74, 15] :
#    if the_num > largest_so_far :
#       largest_so_far = the_num
#    print(largest_so_far, the_num)

# print('After', largest_so_far)

##Slide 16

# loop_interaction = 0
# print("Before", loop_interaction)
# for thing in [9,41,12,3,74,15] :
#     loop_interaction = loop_interaction + 1
#     print(loop_interaction , thing)
# print("After", loop_interaction)

##Slide 17 Loop com soma

# sum = 0
# print("Before", sum)
# for thing in [9, 41, 12, 3, 74, 15] :
#     sum = sum + thing
#     print(sum, thing)
# print("After", sum)

## Slide 18 Loop com soma e contar e a função média

# count = 0
# sum = 0
# print('Before', count, sum)
# for value in [9, 41, 12, 3, 74, 15] :
#     count = count + 1
#     sum = sum + value
#     print(count, sum, value)
# average_value = sum / count
# print('After', count, sum, average_value)


## Slide 19
# print ("before")
# for value in [9,41,12,3,74,15] :
#     if value >74:
#         print ("Large number", value)
#     print("middle")
# print("After")
      
## Slide 20  Loop com boolean
# found = False
# print('Before', found)
# for value in [9, 41, 12, 3, 74, 15] : 
#     if value == 3 :
#         found = True
#     print(found, value)
# print('After', found)

# ## Slide 20 Loop para tirar o True no loop seguinte
# found = False
# print('Before', found)
# for value in [9, 41, 12, 3, 74, 15] : 
#     if value == 3 :
#         found = True
#         print(found, value)
#     elif found == True and value ==3:
#         found = True
#         print(found, value)
#     elif found == True and value != 3:
#         found = False
#         print(found, value)
#     elif found == False and value != 3 :
#         print(found, value)
# print('After', found)

## Slide 21 Conditional com is ou is not
# smallest = None
# print('Before')
# for value in [9, 41, 12, 3, 74, 15] :
#     if smallest is None : 
#         smallest = value
#     elif value < smallest : 
#         smallest = value
#     print(smallest, value)
# print('After', smallest)

## Slide 22 Conditional com is ou is not
# smallest = None
# print('Before')
# for value in [3, 41, 12, 9, 74, 15] :
#     if smallest is None : 
#         smallest = value
#     elif value < smallest : 
#         smallest = value
#     print(smallest, value)

# print('After', smallest)
