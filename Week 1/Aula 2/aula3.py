# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 09:03:04 2020

@author: utilizador
"""

#Exercise 3
# pi = 3.1415
# ra = 5

# volume = 4/3*pi*(ra**3)
# print(f"{volume:.3f} U")

# #Exercicio 3 update novas formas
# pi = 3.1415
# ra = 5
# print(f"{4/3*pi*(ra**3):.3f} U")

# #***

# pi = 3.1415
# radius = float(input("Enter radius:"))
# volume = 4/3*pi*(radius**3)
# print(f"{volume:.3f} U")

# #***

pi = 3.1415
radius = float(input("Enter radius:"))
const = 4/3
power =3
volume = const*pi*(radius**power)
print(f"{volume:.3f} U")

#*** Hw pi from library
import math
radius = float(input("Enter radius:"))
const = 4/3
power =3
volume = const * math.pi * (radius**power)
print(f"{volume:.3f} U")
