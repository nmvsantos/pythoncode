# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 17:16:54 2020

@author: Nuno Santos
"""


#Exercise 1
print ("Enter a number:")
a = int( input())

if a < 10:
        print ("Please, guess higher")
elif a > 10:
       print("Please, guess lower")
else :
       print("Congrats, you found the answer!")  
        
#Exercise 2
age = int( input("Please enter your age"))
minimum_age = 18
retire_age = 67

if age >= minimum_age and age < retire_age:
        print ("Have a good day at work")
elif age < minimum_age:
       print("You are too young to work, come back to school")
else :
       print("You have worked enough, Let’s Travel now") 