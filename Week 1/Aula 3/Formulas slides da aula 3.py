# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 15:12:37 2020

@author: Nuno Santos
"""
"""
Exercicios/ código da aula 2


"""

## Slide 12 diferentes print results
# print(4==5)
# print(6>7)
# print(15<100)
# print('hello' == 'hello')
# print('hello' == 'Hello')
# print('dog' != 'cat')
# print( True == True)
# print(True != False)
# print( 42 == 42.0)
# print( 42 == "42")
# print('apple' == "Apple") 
# print('apple' > "Apple") 
# print('A unicode is', ord("A") ," ,a unicode is" , ord("a")) # ord dá o numero da letra 

##Slide 14 Como funiona o a função if
# minimum_age = 18
# age = 19
# if age > minimum_age:
#     print("Congrats, you can get your license")
# else:
#     print("Please, come back in {0} years" .format(minimum_age - age)) 

## Slide 15  if com em escada
# name = "Nuno"
# password = "cacau"
# if name == "Josi":
#    print('Hello Josi')
# if password == 'swordfish':
#    print('Access granted.')
# else:
#    print('Wrong password.')

##Slide 16 com if e elif e else
# minimum_age = 18
# user_age = 650
# if user_age < minimum_age:
#     print("Please, come back in {0} years" .format(minimum_age - user_age))
# elif user_age >= 95 :
#     print("Probably, you can not drive anymore. Please, make an appointment with your doctor.")
# else :
#     print("Congrats, you can get your license.")

##Exercise 1
# print ("Enter a number:")
# a = int( input())

# if a < 10:
#         print ("Please, guess higher")
# elif a > 10:
#        print("Please, guess lower")
# else :
#        print("Congrats, you found the answer!")  

##Slide 20 operadores de comparadores
# print((4 < 5) and (5 < 6)) # T and T = T
# print((4 < 5) and (9 < 6)) # T and F = F
# print((1 == 2) or (2 == 2)) # F or T = T
# print((2 + 2 == 4) and not (2 + 2 == 5) and (2 * 2 == 2 + 2)) # T an T and T = T

##Exercise 2
# age = int( input("Please enter your age"))
# minimum_age = 18
# retire_age = 67

# if age >= minimum_age and age < retire_age:
#         print ("Have a good day at work")
# elif age < minimum_age:
#        print("You are too young to work, come back to school")
# else :
#        print("You have worked enough, Let’s Travel now") 
       
##Slide 24 Condicional com mais do que 1 critério
# day = "Saturday"
# temperature = 30
# raining = False

# if day =="Saturday" and temperature > 20 and not raining :
#     print("Go out ")
# else:
#     print ("Better finishing python programming exercises")

##Slide 25  Loops -> While
# n = 5
# while n > 0 :
#     print(n)
#     n = n - 1
# print("Boom!!")
# print(n)

##Slide 26
# n = 5    #O loop não pára 
# while n > 0 :
#     print("Time")
#     print("ticking ")
# print("Stopped")
##Correcto loop
# n = 5    
# while n > 0 :
#     print("Time")
#     print("ticking ")
#     n = n-1 # é preciso colocar um contador de n para parar
# print("Stopped")

##Slide 27
# n = 0
# while n > 0 :
#     print("Time")
#     print("ticking ")
# print("Stopped")  # O loop não funciona por o n=0 e o loop só começa com o valor acima de 0

##Slide 28  #Loop e a função break assim que atinja o valor
# while True:
#     line = input('> ')
#     if line == 'done' :
#         break
#     print(line)
# print('Done!')

##Slide 31 Loop with continue and break
while True:
    line = input('> ')
    if line[0] == '#' :
        continue
    if line == 'done' :
        break
    print(line)
print('Done!')





























