# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 15:52:25 2020

@author: Nuno Santos
"""

#Exercise 1

# guess_number = 5
# user_number = int(input("Guess a number"))
# if user_number < guess_number :
#     print("Please, guess higher")
# elif user_number > guess_number :
#     print("Please, guess lower")
# else :
#     print("Congrats, you found the answer")
    
#Exercise 1 with loop


while True :
    guess_number = int(input("Guess a number"))
    if guess_number == 5 :
        break
    print("Please, try again")  
print("Congrats, you found the answer")

