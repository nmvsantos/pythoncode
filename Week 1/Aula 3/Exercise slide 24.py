# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 15:32:38 2020

@author: Nuno Santos
"""

day = "Saturday"
temperature = 30
raining = False

if day == "Saturday" and temperature > 20 and not raining :
    print("Go out")
else:
    print ("Better finishing python programming exercises")
