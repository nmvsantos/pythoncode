# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 09:36:00 2020

@author: Nuno Santos
"""
numero = 5

#print(4==5)
#print(6>7)
#print(15<100)
#print('hello' == 'hello')
#print('hello' == 'Hello')
#print('dog' != 'cat')
#print( True == True)
#print(True != False)
#print( 42 == 42.0)
#print( 42 == "42")
#print('apple' == "Apple") 
#print('apple' > "Apple") 
#print("A unicode is", ord("A") ," ,a unicode is" , ord("a")) # dá o valor que está atribuido à letra


#minimum_age = 18
#age =10
#if age > minimum_age:
#    print("Congrats, you can get your license")
#else :
#    print("Please, come back in {0} years" .format(minimum_age - age)) 


# minimum_age = 18
# user_age = 120
# if user_age < minimum_age :
#     print("Please, come back in {0} years" .format(minimum_age - user_age))
# elif user_age == 150 :
#     print("Probably, you can not drive anymore. Please, make an appointment with your doctor.")
# else :
#     print("Congrats, you can get your license.")  
    
# #Exercise 1
# guess_number = 5
# user_number = int(input("Guess a number"))
# if user_number < guess_number :
#     print("Please, guess higher")
# elif user_number > guess_number :
#     print("Please, guess lower")
# else :
#     print("Congrats, you foud the answer")
 
#****

# print((4 < 5) and (5 < 6))
# print((4 < 5) and (9 < 6))
# print((1 == 2) or (2 == 2))

# print((2 + 2 == 4) and not (2 + 2 == 5))
# print((2 + 2 == 4) and not (2 + 2 == 5) and (2 * 2 == 2 + 2))

#****

#Exercise 2

# age = int( input("Please enter your age"))
# minimum_age = 18
# retire_age = 67

# if age >= minimum_age and age < retire_age:
#         print ("Have a good day at work")
# elif age < minimum_age:
#         print("You are too young to work, come back to school")
# else :
#         print("You have worked enough, Let’s Travel now") 
       
#***

#n = 4
        
# while n > 0 :
#     print(n)
#     n = n - 1
# print("Boom!!")

#****
# while True:
#     line = input('> ')
#     if line == 'done' :
#         break
#     print(line)
# print('Done!')

#****
while True:
    line = input('> ')
    if line[0] == '#' : #quando começamos a frase com # ele não a escreve e vai pedir de imediato a nova frase
        continue        #isso só acontece porque temos o continue
    if line == 'done' :
        break
    print(line)
print('Done!')




    
